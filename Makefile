DITTO_CONFIG ?= Makefile.config
-include $(DITTO_CONFIG)

.PHONY:: local remote
all: local remote

BUILD=build

# we define a ToolChain by prefixing a couple of variables, prefixed by toolchain name
# <TC>_NICK		the (unique) name of toolchain (will be used in generated module names)
# <TC>_CC		C compiler
# <TC>_CXX		C++ compiler
# <TC>_CFLAGS		C compiler's flags
# <TC>_CXXFLAGS		C++ compiler's flags
# <TC>_LINK		linker (can be complex if using winegcc)
# <TC>_LINK_FLAGS	winegcc's flags
# <TC>_WINE	        wine object directory (libs, crt0...)

# GCC MINGW common flags
GCC_MINGW_CFLAGS=-I$(WINE_SRC)/include -I$(WINE_SRC)/include/msvcrt \
-D__WINESRC__ -D_UCRT=0 -D__WINE_PE_BUILD \
-Wall -fno-strict-aliasing -Wdeclaration-after-statement -Wempty-body -Wignored-qualifiers -Winit-self \
-Wno-packed-not-aligned -Wshift-overflow=2 -Wstrict-prototypes -Wtype-limits \
-Wunused-but-set-parameter -Wvla -Wwrite-strings -Wpointer-arith -Wlogical-op -Wabsolute-value

GCC_MINGW_CXXFLAGS=-I$(WINE_SRC)/include -I$(WINE_SRC)/include/msvcrt -std=c++20 \
-D__WINESRC__ -D_UCRT=0 -D__WINE_PE_BUILD \
-Wall -fno-strict-aliasing -Wempty-body -Wignored-qualifiers -Winit-self \
-Wno-packed-not-aligned -Wshift-overflow=2 -Wtype-limits \
-Wunused-but-set-parameter -Wvla -Wwrite-strings -Wpointer-arith -Wlogical-op

# GCC_MINGW 64bit tool chain
GCC_MINGW64_TARGET=x86_64-w64-mingw32
GCC_MINGW64_NICK=$(GCC_MINGW64_TARGET)--gcc
GCC_MINGW64_WINE=$(WINEWOW64)
GCC_MINGW64_CC=$(GCC_MINGW64_TARGET)-gcc
GCC_MINGW64_CXX=$(GCC_MINGW64_TARGET)-g++
GCC_MINGW64_CFLAGS=$(GCC_MINGW_CFLAGS) -I$(GCC_MINGW64_WINE)/include -Wformat-overflow -Wnonnull -mcx16
GCC_MINGW64_CXXFLAGS=$(GCC_MINGW_CXXFLAGS) -I$(GCC_MINGW64_WINE)/include -Wformat-overflow -Wnonnull -mcx16
GCC_MINGW64_WINETOOLS=$(WINEWOW64)/tools
GCC_MINGW64_LINK=$(GCC_MINGW64_WINETOOLS)/winegcc/winegcc --wine-objdir $(GCC_MINGW64_WINE) --winebuild $(GCC_MINGW64_WINETOOLS)/winebuild/winebuild
GCC_MINGW64_LINK_FLAGS=-mno-cygwin -b $(GCC_MINGW64_TARGET) $(WINEGCC_LIB_SUFFIX)
GCC_MINGW64_RC=$(GCC_MINGW64_WINETOOLS)/wrc/wrc

# GCC_MINGW 32bit tool chain
GCC_MINGW32_TARGET=i686-w64-mingw32
GCC_MINGW32_NICK=$(GCC_MINGW32_TARGET)--gcc
GCC_MINGW32_WINE=$(WINEWOW32)
GCC_MINGW32_CC=$(GCC_MINGW32_TARGET)-gcc
GCC_MINGW32_CXX=$(GCC_MINGW32_TARGET)-g++
GCC_MINGW32_CFLAGS=$(GCC_MINGW_CFLAGS) -I$(GCC_MINGW32_WINE)/include -fno-omit-frame-pointer
GCC_MINGW32_CXXFLAGS=$(GCC_MINGW_CXXFLAGS) -I$(GCC_MINGW32_WINE)/include -fno-omit-frame-pointer
GCC_MINGW32_WINETOOLS=$(WINEWOW64)/tools
GCC_MINGW32_LINK=$(GCC_MINGW32_WINETOOLS)/winegcc/winegcc --wine-objdir $(GCC_MINGW32_WINE) --winebuild $(GCC_MINGW32_WINETOOLS)/winebuild/winebuild
GCC_MINGW32_LINK_FLAGS=-mno-cygwin -b $(GCC_MINGW32_TARGET) $(WINEGCC_LIB_SUFFIX)
GCC_MINGW32_RC=$(GCC_MINGW64_WINETOOLS)/wrc/wrc

# One can generate:
# - "simple" module: only compiled once for a toolchain, and likely not on all the tools chains
#   push_module_simple() will generate the relevant Mekefile bits
#   (no decoration based on toolchain will be performed for generating its name, to it's up to
#    the caller to handle uniqueness regarding compilation from several toolchains)
# - "multiple" module: it's compiled with most of the toolchains, and likely several times
#   with different compiler options.
#   push_module() will generate the relevant Mekefile bits
#   (the module name is decorated with toolchain's nickname)

DIR_DITTO=src/ditto
SRC_DITTO=main.c dbginfo.c coherence.c helper.c explore.c
OBJ_DITTO=$(SRC_DITTO:%.c=%.o)

LINK_FLAGS_DITTO=-console
LIBS_DITTO=kernel32 ucrtbase dbghelp

# needed to force first generation of $(BUILD)/mark.h
ditto:: $(BUILD)/mark.h

$(BUILD)/mark.h: Makefile scripts/extract.pl
	@cd $(DIR_DEBUGGEE) && perl ../../scripts/extract.pl *.[ch] *.cc *.hh > ../../$@

DIR_DEBUGGEE=src/ditto
SRC_DEBUGGEE=debuggee.c debuggee_cc.cc
OBJ_DEBUGGEE=debuggee.o debuggee_cc.o

LINK_FLAGS_DEBUGGEE=-shared ../../$(DIR_DITTO)/debuggee.spec
LIBS_DEBUGGEE=winecrt0 kernel32 ucrtbase

DIR_WDTP=src/wdtp
SRC_WDTP=wdtp.c wdtp_expr.c wdtp_xpoint.c
OBJ_WDTP=$(SRC_WDTP:%.c=%.o)

LIBS_WDTP=winecrt0 kernel32 ucrtbase

wdtp:: $(BUILD)/wdtp_markers

$(BUILD)/wdtp_markers: $(SRC_WDTP:%=$(DIR_WDTP)/%) scripts/wdtp.pl
	-@perl scripts/wdtp.pl markers $(SRC_WDTP:%=$(DIR_WDTP)/%) > $@

.SUFFIXES: .wdtp .args

$(BUILD)/%.wdtp: $(DIR_WDTP)/%.wdtp
	-@perl scripts/wdtp.pl input $< $(BUILD)/wdtp_markers > $@

$(BUILD)/%.args: $(DIR_WDTP)/%.wdtp
	-@perl scripts/wdtp.pl args $< > $@

WDTP_TESTS=expr type xpoint

wdtp:: $(WDTP_TESTS:%=$(BUILD)/%.wdtp)  $(WDTP_TESTS:%=$(BUILD)/%.args)

DIR_CRASHME=src/crashme
SRC_CRASHME=crashme.c
RC_CRASHME=crashme.rc
OBJ_CRASHME=$(SRC_CRASHME:%.c=%.o) $(RC_CRASHME:%.rc=%.res)

LINK_FLAGS_CRASHME=-console
LIBS_CRASHME=winecrt0 kernel32 ucrtbase user32 dbghelp

# 1=modulename 2=sourcedir 3=objects 4=toolchain 5=compilerflags 7=linkflags
define generate_target_makefile
	@-mkdir -p $(BUILD)/$1
	@rm -f $(BUILD)/$1/Makefile
	@echo "VPATH=../../$2"                                                  >> $(BUILD)/$1/Makefile
	@echo "include ../../scripts/submakefile.mk"                            >> $(BUILD)/$1/Makefile
	@echo ".SUFFIXES: .rc .res"                                             >> $(BUILD)/$1/Makefile
	@echo "all: $1"                                                         >> $(BUILD)/$1/Makefile
	@echo "	@:"                                                             >> $(BUILD)/$1/Makefile
	@echo '$1: $3'                                                          >> $(BUILD)/$1/Makefile
	@echo '	$$(call colorecho,$($(4)_NICK) link,$$@)'                       >> $(BUILD)/$1/Makefile
	@echo '	$$(Q)$($(4)_LINK) -o $1 $($(4)_LINK_FLAGS) $3 $6'               >> $(BUILD)/$1/Makefile
	@echo '.c.o:'                                                           >> $(BUILD)/$1/Makefile
	@echo '	$$(call colorecho,$($(4)_NICK) CC,$$(notdir $$<))'              >> $(BUILD)/$1/Makefile
	@echo '	$$(Q)$($(4)_CC) -c $$< $($(4)_CFLAGS) -MMD $5'                  >> $(BUILD)/$1/Makefile
	@echo '.cc.o:'                                                          >> $(BUILD)/$1/Makefile
	@echo '	$$(call colorecho,$($(4)_NICK) CXX,$$(notdir $$<))'             >> $(BUILD)/$1/Makefile
	@echo '	$$(Q)$($(4)_CXX) -c $$< $($(4)_CXXFLAGS) -MMD $5'               >> $(BUILD)/$1/Makefile
	@echo '-include *.d'                                                    >> $(BUILD)/$1/Makefile
	@echo '.rc.res:'                                                        >> $(BUILD)/$1/Makefile
	@echo '	$$(call colorecho,$($(4)_NICK) RC,$$(notdir $$<))'              >> $(BUILD)/$1/Makefile
	@echo '	$$(Q)$($(4)_RC) $$< -o $$@ -I../../$2 -I$(WINE_SRC)/include -I$(WINE_SRC)/include/msvcrt' >> $(BUILD)/$1/Makefile
endef

# 1=family 2=partial-modulename 3=toolchain 4=extracompileflags 5=extrawinegccflags
# FIXME uppercasing could be a helper somewhere
# _FAM, _MOD will be global, don't call recursively
# FIXME couldn't make a single macro
define push_module_simple
	$(eval _FAM := $(shell echo '$1' | tr '[:lower:]' '[:upper:]'))
	$(eval _MOD := $1$2)
	$(call generate_target_makefile,$(_MOD),$(DIR_$(_FAM)),$(OBJ_$(_FAM)),$3,$4,$5 $(LINK_FLAGS_$(_FAM)) $(LIBS_$(_FAM):%=-l%))

	@echo ".PHONY:: $1 $(_MOD)"                                             >> $(BUILD)/Makefile
	@echo "local:: $1"                                                      >> $(BUILD)/Makefile
	@echo "$1:: $(_MOD)"                                                    >> $(BUILD)/Makefile
	@echo "$(_MOD):"                                                        >> $(BUILD)/Makefile
	@echo '	@LANG=C $$(MAKE) -C $(BUILD)/$(_MOD)'                           >> $(BUILD)/Makefile
endef
define push_module
	$(call push_module_simple,$1,--$($(3)_NICK)$2,$3,$4,$5)
endef

.PHONY:: test test32 test64 teststrictwow clean distclean

ALL_MAKEFILE=$(BUILD)/Makefile

comma:= ,

$(BUILD)/Makefile: Makefile
	$(call colorecho,generate makefiles,)
	@-rm -f $(BUILD)/Makefile
# generic tools
	$(call push_module_simple,ditto,64.exe,GCC_MINGW64,-g -gdwarf-4 -O2)
	$(call push_module_simple,ditto,32.exe,GCC_MINGW32,-g -gdwarf-4 -O2)
# gcc/mingw with dwarf debug info
	$(call push_module,debuggee,--dwarf4-O1.dll,GCC_MINGW64,-g -gdwarf-4 -gstrict-dwarf -O1)
	$(call push_module,debuggee,--dwarf4-O2.dll,GCC_MINGW64,-g -gdwarf-4 -gstrict-dwarf -O2)
	$(call push_module,debuggee,--dwarf2-O1.dll,GCC_MINGW64,-g -gdwarf-2 -gstrict-dwarf -O1)
	$(call push_module,debuggee,--dwarf2-O2.dll,GCC_MINGW64,-g -gdwarf-2 -gstrict-dwarf -O2)
	$(call push_module,debuggee,--dwarf4-O1.dll,GCC_MINGW32,-g -gdwarf-4 -gstrict-dwarf -O1)
	$(call push_module,debuggee,--dwarf4-O2.dll,GCC_MINGW32,-g -gdwarf-4 -gstrict-dwarf -O2)
	$(call push_module,debuggee,--dwarf2-O1.dll,GCC_MINGW32,-g -gdwarf-2 -gstrict-dwarf -O1)
	$(call push_module,debuggee,--dwarf2-O2.dll,GCC_MINGW32,-g -gdwarf-2 -gstrict-dwarf -O2)
ifdef WINDOWS_BRIDGE
	@echo "$(BUILD)/.windows_compile_stamp: $(WIN_FILES)"                   >> $(BUILD)/Makefile
	@echo "	@$(WINDOWS_BRIDGE) compile $(WIN_FILES)"                        >> $(BUILD)/Makefile
	@echo "	@touch $(BUILD)/.windows_compile_stamp"                         >> $(BUILD)/Makefile
	@echo ".PHONY:: windows-test"                                           >> $(BUILD)/Makefile
	@echo "windows-test: $(BUILD)/.windows_compile_stamp"                   >> $(BUILD)/Makefile
	@echo "	@$(WINDOWS_BRIDGE) test scripts/run.bat"                        >> $(BUILD)/Makefile
	@echo ".PHONY:: windows-log"                                            >> $(BUILD)/Makefile
	@echo "windows-log: $(BUILD)/.windows_compile_stamp"                    >> $(BUILD)/Makefile
	@echo "	@$(WINDOWS_BRIDGE) log scripts/run.bat log"                     >> $(BUILD)/Makefile
	@echo ".PHONY:: windows-dump"                                           >> $(BUILD)/Makefile
	@echo "windows-dump: $(BUILD)/.windows_compile_stamp"                   >> $(BUILD)/Makefile
	@echo "	@$(WINDOWS_BRIDGE) log scripts/run.bat dump"                    >> $(BUILD)/Makefile
endif
# wdtp (winedbg test program): target generation
	$(call push_module,wdtp,--dwarf4-O0.exe,GCC_MINGW64,-g -gdwarf-4 -gstrict-dwarf -O0)
#	$(call push_module,wdtp,--dwarf4-O1.exe,GCC_MINGW64,-g -gdwarf-4 -gstrict-dwarf -O1)
#	$(call push_module,wdtp,--dwarf4-O2.exe,GCC_MINGW64,-g -gdwarf-4 -gstrict-dwarf -O2)
#	$(call push_module,wdtp,--dwarf2-O0.exe,GCC_MINGW64,-g -gdwarf-2 -gstrict-dwarf -O0)
#	$(call push_module,wdtp,--dwarf2-O1.exe,GCC_MINGW64,-g -gdwarf-2 -gstrict-dwarf -O1)
#	$(call push_module,wdtp,--dwarf2-O2.exe,GCC_MINGW64,-g -gdwarf-2 -gstrict-dwarf -O2)
	$(call push_module,wdtp,--dwarf4-O0.exe,GCC_MINGW32,-g -gdwarf-4 -gstrict-dwarf -O0)
#	$(call push_module,wdtp,--dwarf4-O1.exe,GCC_MINGW32,-g -gdwarf-4 -gstrict-dwarf -O1)
#	$(call push_module,wdtp,--dwarf4-O2.exe,GCC_MINGW32,-g -gdwarf-4 -gstrict-dwarf -O2)
#	$(call push_module,wdtp,--dwarf2-O0.exe,GCC_MINGW32,-g -gdwarf-2 -gstrict-dwarf -O0)
#	$(call push_module,wdtp,--dwarf2-O1.exe,GCC_MINGW32,-g -gdwarf-2 -gstrict-dwarf -O1)
#	$(call push_module,wdtp,--dwarf2-O2.exe,GCC_MINGW32,-g -gdwarf-2 -gstrict-dwarf -O2)
# crashme
	$(call push_module_simple,crashme,64.exe,GCC_MINGW64,-g -gdwarf-4 -O2)
	$(call push_module_simple,crashme,32.exe,GCC_MINGW32,-g -gdwarf-4 -O2)

ifdef WINDOWS_BRIDGE

WIN_BATCH=scripts/build.bat scripts/run.bat
WIN_FILES=$(SRC_DEBUGGEE:%=$(DIR_DEBUGGEE)/%) $(DIR_DEBUGGEE)/debuggee_cc.hh \
          $(SRC_DITTO:%=$(DIR_DITTO)/%) $(DIR_DITTO)/test.h $(DIR_DITTO)/ditto.h $(BUILD)/mark.h \
          $(SRC_WDTP:%=$(DIR_WDTP)/%) $(DIR_WDTP)/wdtp.h \
	  $(SRC_CRASHME:%=$(DIR_CRASHME)/%) $(RC_CRASHME:%=$(DIR_CRASHME)/%) $(DIR_CRASHME)/ids.h \
          $(WIN_BATCH) "$(BUILD)/*/Makefile" $(BUILD)/Makefile.win

MSVC_CFLAGS=/W4 -I "$$(MSVC_VSBUILD)\DIA SDK\include"
MSVC_CXXFLAGS=/W4 -I "$$(MSVC_VSBUILD)\DIA SDK\include" /std:c++20
MSVC32_NICK=i686-windows--msvc
MSVC32_CFLAGS=$(MSVC_CFLAGS)
MSVC32_CXXFLAGS=$(MSVC_CXXFLAGS)
MSVC64_NICK=x86_64-windows--msvc
MSVC64_CFLAGS=$(MSVC_CFLAGS)
MSVC64_CXXFLAGS=$(MSVC_CXXFLAGS)

MSVC_LINK_FLAGS_DEBUGGEE=/DLL /DEBUG /EXPORT:stack_walk_thread kernel32.lib
MSVC_LINK_FLAGS_DITTO=/SUBSYSTEM:CONSOLE /DEBUG msvcrt.lib kernel32.lib dbghelp.lib
MSVC_LINK_FLAGS_WDTP=/SUBSYSTEM:CONSOLE /DEBUG
MSVC_LINK_FLAGS_CRASHME=/SUBSYSTEM:CONSOLE /DEBUG kernel32.lib user32.lib dbghelp.lib

# 1=modulename 2=subobject(uppercase of module) 3=toolchain 4=additional compiler's options
# no .PHONY target on NMAKE?
define push_win_module_core
	@-mkdir -p $(BUILD)/$1
	@rm -f $(BUILD)/$1/Makefile
	@echo "all: $1"                                                         >> $(BUILD)/$1/Makefile
	@echo '{../../$(DIR_$2)/}.c.o:'                                         >> $(BUILD)/$1/Makefile
	@echo '	@$$(CC) /c $($3_CFLAGS) $4 /Fo$$@ $$<'                          >> $(BUILD)/$1/Makefile
	@echo '{../../$(DIR_$2)/}.cc.o:'                                        >> $(BUILD)/$1/Makefile
	@echo '	@$$(CC) /c $($3_CXXFLAGS) $4 /Fo$$@ $$<'                        >> $(BUILD)/$1/Makefile
	@echo '$1: $(OBJ_$2)'                                                   >> $(BUILD)/$1/Makefile
	@echo '	@$$(CC) $$** /Fe$1 /link $(MSVC_LINK_FLAGS_$2)'                 >> $(BUILD)/$1/Makefile

	@echo "$3::"                                                            >> $(BUILD)/Makefile.win
	@echo '	@cd $(BUILD)/$1 && $$(MAKE) & cd .. & cd ..'           		>> $(BUILD)/Makefile.win
	@echo "clean::"                                                         >> $(BUILD)/Makefile.win
	@echo '	@-rmdir /s /q $(BUILD)\$1'                                      >> $(BUILD)/Makefile.win
endef

# 1=family 2=partial-modulename 3=toolchain 4=extracompileflags
define push_win_module
	$(call push_win_module_core,$1--$($(3)_NICK)$2,$(shell echo '$1' | tr '[:lower:]' '[:upper:]'),$3,$4)
endef

ALL_MAKEFILE+=$(BUILD)/Makefile.win

$(BUILD)/Makefile.win: $(BUILD)/Makefile
# FIXME: improve parallelisation for reducing time
	$(call colorecho,generate Windows makefiles,)
	@-rm -f $(BUILD)/Makefile.win
	$(call push_win_module,ditto,.exe,MSVC64,/O2 /MD /Zi)
	$(call push_win_module,ditto,.exe,MSVC32,/O2 /MD /Zi)
	$(call push_win_module,debuggee,--pdb-O1.dll,MSVC64,/O1 /MD /Zi)
	$(call push_win_module,debuggee,--pdb-O2.dll,MSVC64,/O2 /MD /Zi)
	$(call push_win_module,debuggee,--pdb-O1.dll,MSVC32,/O1 /MD /Zi)
	$(call push_win_module,debuggee,--pdb-O2.dll,MSVC32,/O2 /MD /Zi)
	$(call push_win_module,wdtp,--pdb-O0.exe,MSVC64,/O0 /MD /Zi)
#	$(call push_win_module,wdtp,--pdb-O1.exe,MSVC64,/O1 /MD /Zi)
#	$(call push_win_module,wdtp,--pdb-O2.exe,MSVC64,/O2 /MD /Zi)
	$(call push_win_module,wdtp,--pdb-O0.exe,MSVC32,/O0 /MD /Zi)
#	$(call push_win_module,wdtp,--pdb-O1.exe,MSVC32,/O1 /MD /Zi)
#	$(call push_win_module,wdtp,--pdb-O2.exe,MSVC32,/O2 /MD /Zi)
	$(call push_win_module,crashme,.exe,MSVC64,/O2 /MD /Zi)
	$(call push_win_module,crashme,.exe,MSVC32,/O2 /MD /Zi)

remote: local $(BUILD)/Makefile.win $(BUILD)/.windows_compile_stamp

else
remote:
endif

-include $(BUILD)/Makefile
-include scripts/submakefile.mk
-include $(BUILD)/Makefile.test

$(BUILD)/Makefile.test: $(ALL_MAKEFILE) scripts/gentest.pl Makefile.config
	$(call colorecho,generate test configuration,)
	-$(Q)perl scripts/gentest.pl ditto --32=$(WINE32) --64=$(WINE64) --wow32=$(WINEWOW32) --wow64=$(WINEWOW64) --newwow=$(WINENEWWOW) > $@
	-$(Q)perl scripts/gentest.pl wdtp --wow32=$(WINEWOW32) --wow64=$(WINEWOW64) >> $@

# don't erase dll/pdb generated on windows
clean:
	rm -f $(BUILD)/mark.h $(BUILD)/Makefile* $(BUILD)/*/Makefile $(BUILD)/.windows_compile_stamp $(BUILD)/wdtp_markers

distclean: clean
	rm -f *.orig *.rej *~ */*.orig */*.rej */*~ */*/*.orig */*/*.rej */*/*~
	rm -rf $(BUILD)
