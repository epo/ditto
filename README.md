This is Wine DITTO (Wine Debug Info Test TOol), a non-regression test
for Wine's debugging information implementation.

# Requirements

In order to use DITTO, you need:
- a valid Wine development setup
- potentially different Wine compilation targets for managing 32bit,
  64bit, wow32/64 setups
- a Un*x cross compiler for generating PE modules (and debug
  information).
  The currently supported compilers are:
  + MingW (gcc)
- for PDB debug information testing, a PDB capable compiler.
  The currently supported compilers are:
  + MSVC (requires Windows system and a proper MS Visual Studio setup,
    preferably with x86 and x64 tool chains installed)
- all compilers must support 32bit and 64bit module generation.
- various other tools (make, perl...)

DITTO can trigger the generation of modules with a Windows compiler,
but in order to support the various possible configurations, it's only
make of a stub that you have to fill in according to your settings.

You could also share the ditto directory between:
- a Un*x system
- a Window system with SDK and MSVC installed.
and trigger manually the compilations on the Windows system.

The end result is to be able to test the DLLs compiled on the Windows
system when running the DITTO tools with Wine on the Un*x system.

# Configuration

Configuration is made from Makefile.config that you need to edit
yourself. The variables to be set are:

| Variable       | Mandatory | Explanation                                         |
| ---            | ---       | ---                                                 |
| WINE_SRC       | Yes       | The top directory where Wine's source can be found. |
| WINEWOW64      | Yes       | The directory where Wine's compilation as a 64bit target, configured with Wow enabled, can be found. |
| WINEWOW32      | Yes       | The directory where Wine's compilation as a 32bit target, configured with Wow enabled, can be found. |
| WINENEWWOW     | No        | The directory where Wine's compilation as multi-arch wow can be found. |
| WINE64         | No        | The directory where Wine's compilation as a pure 64bit target can be found. |
| WINE32         | No        | The directory where Wine's compilation as a pure 32bit target can be found. |
| WINDOWS_BRIDGE | No        | The filename of a script to control commands on a windows target (like compilation, or running commands). |

All Unix compilations will be done against WINE_SRC, WINEWOW32 and
WINEWOW64 directories.
Tests will be run on WINEWOW32 and WINEWOW64, and optionally on
WINE32, WINE64 and WINENEWWOW when they are defined.

Note:
- MingW's default headers are not sufficient to compile DITTO (hence
  the requirement for WINE_SRC)
- no support for Wine prefix is done (it assumes that running Wine
  from one of those Wine build directories will correctly set its Wine
  prefix).
- no support exists for an installed Wine setup (outside of build
  trees).
- when WINDOWS_BRIDGE is defined, it's run as a way to populate build/
  directory with the output of windows operation (compilation, getting
  logs...). Since it highly depends on local configuration, it's up to
  you to write it.

# Components
DITTO comes with various components:
- ditto: a tool to test retrieving debug information out of different
  debuggees,
- wdtp (WineDbg test program): a tool to test winedbg behavior against
  a set of commands run with a set of debuggees,
- crashme: a simple application to generate exceptions of various
  sorts.

To avoid confusion DITTO (in uppercase) refers to the whole package
and the various tools, while ditto (in lowercase) refers to test tool.

# How it works (ditto)

ditto is made of several components:
- The **tester** is made of several test units (comparable to what
  exists in Wine tests/ subdirectories).
- The **debuggee**: it's a module holding debug information.
- The **tester** will load the **debuggee** and then perform (using
  DbgHelp APIs) tests on this **debuggee**

In order to cover a lot of test cases:
- the **tester** is compiled as ditto32.exe for a 32bit PE, and
  ditto64.exe as a 64bit PE.
- the **debuggee** is compiled in different flavours, depending on
  image format, compiler, debug information, compiler's options.
- The **debuggee** is held in a DLL, named according to this pattern:
      debuggee--**triplet**--**compiler**--**flags**.dll
  where
  + **triplet**  is the generic Linux triplet for cross
                 compilation. It normally encompasses the
                 processor/machine and the file format of the module.
  + **compiler** the compiler used to generate the DLL.<br>MingW (GCC)
                 and MSVC are available.
  + **flags**    options passed to the compiler.<br>mainly
                 debug format, optimization levels O1, O2..., but
                 could also be pgo options...
- the tests from **tester** are meant to be run:
  + on various systems: either on Wine setup or in Windows.
  + combining various bitness:
    * 32bit **tester** and 32bit **debuggee**
    * 64bit **tester** and 64bit **debuggee**
    * 64bit **tester** and 32bit **debuggee**
  + testing also in-process vs out-process usage of DbgHelp
    * in-process: means the **tester** loads the **debuggee** in its
      address space and performs tests on it.
    * out-process: means the **tester** spawns another instance of
      itself. The parent instance will run the tests, whilst the child
      instance will load the **debuggee**.
    * in-process mode requires the same bitness on **tester** and
      **debuggee**

## Compilation phase

The compilation phase shall generate all ditto*.exe and debuggee*.dll
files, corresponding to the configuration. Each one of them will be
generated inside a dedicated directory inside the build/ subdirectory.

### Manual (WINDOWS_BRIDGE not configured)

Compilation must be first done on Un*x system with 'make'.
Then compilation must be run under Windows with 'build.bat'.

The first compilation on Un*x will generate some additional files that
are required by the Window compilation.

So either:
- The DITTO tree is shared between the Un*x and Windows setup:
  + on Un*x, run 'make'
  + on Windows, run 'script\build.bat'
- if you only have copy capabilities between the two setups, your compilation
  phase must look like:
  + on Un*x, run 'make'
  + copy DITTO tree from Un*x to Windows
  + on Windows, run 'script\build.bat'
  + copy ditto/build subdirectories from Windows back to Un*x
    (either handle overwrite of files, or only copy
    ditto/build/debuggee*msvc* subdirectories.

### If WINDOWS_BRIDGE is configured

The Windows part of the compilation will automatically be run, using the
same depedancies as on Unix.

## Test phase

Once the compilation phase is complete, the test phase can begin.

Under Un*x, 'make test' will run all possible test combinations.

### Manual (WINDOWS_BRIDGE not configured)

You can also run the tests in the Windows environment. Note that only
the files generated for Windows (from the build.bat command) will be
tested. This test ensures that DITTO behaves correctly under native
DbgHelp. Use the run.bat command in the Windows environnement to run
the tests.

### If WINDOWS_BRIDGE is configured

'make windows-test' will trigger tests under Windows.

Note: windows-test isn't included in test target, as the generic way
of writing tests is like:
- create a test
  + write tests
  + ensure they pass under Windows (make windows-test)
  + insert todo to let 'make test' pass
- improve builtin dbghelp
  + fix dbghelp to remove the todo tests above

## Make targets

You can have access to finer targets (when needed)

- on Un*x
  + ditto:          recompiles all programs used to run test
                    (ditto*.exe) 
  + debuggee:       recompile all debuggee flavours
  + test-ditto:     test all possible configurations
                    (debuggee-*.dll) on all configured Wine systems
  + test-<machine>: test all possible configurations for given machine
                    (x86_64, i686)
  + test-<format>:  test all possible configurations for debuggee
                    in given debug format (pdb, dwarf, dwarf2, dwarf4)
  ° help:           prints all the subtargets from makefile (there are
                    more than what's listed above)
- on Windows (no Makefile, just standalone commands)
  + build.bat:      recompile all Windows components.
                    (ditto*-msvc.exe and debuggee-*-msvc-pdb-*.dll)
  + run.bat:        test all possible configurations under Windows
                    (debuggee-*-msvc-pdb-*.dll) on Windows System
- if WINDOWS_BRIDGE is fully configured:
  + debuggee:       will also trigger the compilation under Windows
  + windows-test:   will run the tests under Windows

## Which use cases are covered

```
|          |          | System  | Wine  | Wine  | Wine  | Wine  | Wine   | Wine   | Win-  | Win-  |
|          |          |         | 32bit | 64bit | Wow   | Wow   | NewWow | NewWow | dows  | dows  |
|----------+----------+---------+-------+-------+-------+-------+--------+--------+-------+-------|
|          |          | tester  | PE32  | PE64  | PE32  | PE64  | PE32   | PE64   | PE32  | PE64  |
|----------+----------+---------+-------+-------+-------+-------+--------+--------+-------+-------|
| Debuggee | Compiler | Debug   |       |       |       |       |        |        |       |       |
|          |          | Format  |       |       |       |       |        |        |       |       |
|----------+----------+---------+-------+-------+-------+-------+--------+--------+-------+-------|
| In-      | MingW    | Dwarf-2 |  wow  |  wow  | O1,O2 | O1,O2 | O1,O2* | O1,O2* | ~~D~~ | ~~D~~ |
| process  |          | Dwarf-4 |  wow  |  wow  | O1,O2 | O1,O2 | O1,O2* | O1,O2* | ~~D~~ | ~~D~~ |
|          | MSVC     | PDB     |  wow  |  wow  | O1,O2 | O1,O2 | O1,O2* | O1,O2* | O1,O2 | O1,O2 |
|----------+----------+---------+-------+-------+-------+-------+--------+--------+-------+-------|
| Out-     | MingW    | Dwarf-2 |  wow  | ~~~~~ | O1,O2 | O1,O2 | O1,O2* | O1,O2* | ~~D~~ | ~~D~~ |
| process  |          | Dwarf-4 |  wow  | ~~~~~ | O1,O2 | O1,O2 | O1,O2* | O1,O2* | ~~D~~ | ~~D~~ |
| PE32     | MSVC     | PDB     |  wow  | ~~~~~ | O1,O2 | O1,02 | O1,O2* | O1,O2* | O1,O2 | O1,O2 |
|----------+----------+---------+-------+-------+-------+-------+--------+--------+-------+-------|
| Out-     | MingW    | Dwarf-2 | ~~~~~ |  wow  | ~~~~~ | O1,O2 | O1,O2* | O1,O2* | ~~~~~ | ~~D~~ |
| process  |          | Dwarf-4 | ~~~~~ |  wow  | ~~~~~ | O1,O2 | O1,O2* | O1,O2* | ~~~~~ | ~~D~~ |
| PE64     | MSVC     | PDB     | ~~~~~ |  wow  | ~~~~~ | O1,O2 | O1,O2* | O1,O2* | ~~~~~ | O1,O2 |
```

| Symbol | Meaning                                       | Tested                     |
|--------|-----------------------------------------------|----------------------------|
| ~~~~~  | 32/64 bit mismatch in process.                | Never (not doable)         |
| \~\~D\~\~  | Dwarf* is not implemented in Windows' DbgHelp | Never                      |
| wow    | There's a very small difference compared to the same test run on Wine wow configuration | Not by default<br>Available as test-paranoid if either WINE32 or WINE64 is defined |
| O1,O2  | The **debuggee** flavors compiled with options O1 and O2 are successfully tested (no failures, potential TODO) | By default |
| *      | Some tests have been disabled (based on StackWalk*), because of failure. | By default |

When running a test, a quadruplet is printed:
- first element in system (Wine environment) (first row above)
- second is whether **tester** is 32 vs 64bit (ditto32 / ditto64)
- third item is whether the **debuggee** is run in-process (hence with
  same bitness as **tester**), or out-process (which can be 32 or 64
  bit (ditto32 / ditto64) (first column above)
- fourth item is how the debuggee has been compiled (compiler, debug
  format). This is the full flavor name.

## Updating / adding more tests to ditto

When doing so, you must first ensure that running script\build.bat
under Windows compiles without warnings, and that script\run.bat
doesn't report any error.
(or make windows-test if WINDOWS_BRIDGE is configured).

Then, after ensuring that the generated files on Windows are available
under Un*x (see above), you can run the tests under Un*x.

Note that this can lead to also updating Wine's dbghelp implementation
for correctness of the tests results.

As of today, no support is defined for providing synchronization
between the two Wine source tree and the ditto source tree (even
though this shall be done at some point).

# Other debugging facilities

ditto*.exe can also be used to display debugging information from an
external module (and its debugging bits).

All the commands are run the same way:
ditto [command] [options] module

See ditto --help for further details.

FIXME: be more detailed + make targets

If WINDOWS_BRIDGE is fully defined,
+ 'make windows-log' will generate various logs about native dbghelp
  exploration of debuggee*.dll
+ 'make windows-dump will generate a complete dump of native dbghelp
  internal information of debuggee*.dll
You can look at scripts/windows_bridge.sh.in as a skeletton for such
an implementation.

# wdtp

This tools allows to test WineDbg behavior. In a very similar way than
the ditto tool, it generates several **debuggees** (named according to
the same scheme as above as
  wdtp--**triplet**--**compiler**--**flags**.exe

Then several tests are embodied in the **debuggee**.exe (in a similar
way as Wine's tests directories). To each test is paired:
- a set of commands to be executed by WineDbg,
- a template of the expected results (print outs by WineDbg),

Running the tests means for every debuggee:
- run winedbg with the command file against the debuggee,
- compare the output against the template, and lists the discrepancies
  if any.

The internals (naming scheme for debuggees, use of the Windows bridge)
are the same as the ones described in ditto tool.

Use 'make test_wdtp' to trigger the wdtp tests. Note that 'make test'
will not run the wdtp tests, but only the ditto ones.

# crashme

This is a application that can generate exceptions, with some level
of configuration:
- either from command line
- or configured in a graphical tool.

The options are:
- type of exception (breakpoint, segmentation fault, raised
  exception),
- done within (or not) an exception handler.

# Distribution of DITTO

## Why isn't there any binary packages for using DITTO?

In order to distribute binaries compiled by MSVC, there's a need for a
commercial license of MSVC (AFAICT).

## Why isn't DITTO integrated into Wine?

There are a couple of reasons, that boil down to DITTO requirements
not fitting well inside Wine non-regression requirements:
- all compilations regarding tests/ directories and winetest generate
  stripped modules for size reasons; DITTO requires non stripped
  versions,
- since DITTO rebuilds the **debuggee** before testing, this
  introduces some fragility in the process, as compilers can be
  updated, and hence generate different output.
- **debuggee** is partly made of C++ in order to test debug information
  for C++ source code.
- it would be better to integrate the compiled **debuggee** as static
  data, but that raises also the license question (see above).
- that would also raise a size question. Current **debuggee**:s size
  (including the .pdb files) is roughly 2/3 of winetest.exe size.
  So winetest.exe would grow of 66% in order to integrate DITTO.

Before this is ironed out, DITTO uses a test framework which is very
close to Wine's. That would ease integration into Wine if this ever
happens.

# Future work

Some ideas:
- System view
  - support installed Wine (instead of build trees)
  - WINDOWS_BRIGE is kinda slow (no parallelisation, no per tool build)
- Modules:
  - support more module formats:
    + ELF
  - support more compilers
    + clang (both for dwarf* and pdb)
    + ability to support several versions of same compiler
  - support more debug formats:
    + dwarf5
    + stabs?
  - support more options:
    + PGO on Windows
- Language support
  - C++ debug information
    + at least class handling (inheritance, virtual inheritance, methods...)
  - Public symbol handling
  - ...
- other stuff:
  - save & reload of minidump files
