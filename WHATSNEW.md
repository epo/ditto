# Version 0.10 (requires at least Wine 9.3)

## ditto

### Added tests

+ static (file and function scope), and global variables.

### Debug format specifics

- no longer forcing MSVC compiler version.
- adaptations to struggle against compilers' optimization.

### Tools

- dump following hierarchy links is now implemented.
- logs for various dump tools can now be generated automatically.

## Environment

- added ability to trigger compilation of MSVC modules
  (just a stub that needs to be locally implemented)
- added ability to test a new WOW Wine setup.
- restructured files layout code so that we can easily
  add new modules (winedbg test is on its way), and
  compilers's settings (clang support is on its way).
- debuggee modules naming (across compilers) has evolved for more
  flexibility. Format is now:
  <module>--<host triplet>--<compiler>--<options>
  where <module> is debuggee, <host triplet> is the generic
  Linux triplet format for cross compilation, <compiler> is the
  compiler name (including potentially its version number) and
  <options> a unique string for distinguishing across various
  compilation options.
  This mainly allows to support more machines, and more file formats
  (like .so builds).
- ability to add new modules

## wdtp

Integrated a new tool to test Wine's winedbg.
Tests cover:
- expression (read / write memory, C-like expression interpreter...)
- breakpoint & watch point
- types


## crashme

Integrated a new tool to generate crashes.
