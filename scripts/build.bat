@if /I "%1"=="clean" goto :clean
@if /I "%1"=="__compile32" goto :__compile32
@if /I "%1"=="__compile64" goto :__compile64
@if /I "%1"=="__pgocompile64" goto :__pgocompile64

@rem lookup for VS installation
@if exist "C:\Program Files\Microsoft Visual Studio\2022\Community\" (
@SET "MSVC_VSBUILD=C:\Program Files\Microsoft Visual Studio\2022\Community\"
) else if exist "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\" (
@SET "MSVC_VSBUILD=C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools"
) else @ECHO "Couldn't locate Visual Studio installation"

@SET "VCVARS=%MSVC_VSBUILD%\VC\Auxiliary\Build\vcvarsall.bat"

@if /I "%1"=="pgo" goto :pgo

@cmd.exe /c "%0 __compile64"
@cmd.exe /c "%0 __compile32"

@goto :EOF

:__compile64
@call "%VCVARS%" x64 %VCVARSOPT%
NMAKE.EXE /F build\Makefile.win MSVC64

@goto :EOF

:__compile32
@call "%VCVARS%" x64_x86 %VCVARSOPT%
NMAKE.EXE /F build\Makefile.win MSVC32

@goto :EOF

:clean
rmdir /s /q build\debuggee-pe64-msvc-pdb-O1.dll
rmdir /s /q build\debuggee-pe64-msvc-pdb-O2.dll
rmdir /s /q build\debuggee-pe32-msvc-pdb-O1.dll
rmdir /s /q build\debuggee-pe32-msvc-pdb-O2.dll
rmdir /s /q build\debuggee-pe64-msvc-pdb-pgoO1.dll
rmdir /s /q build\debuggee-pe64-msvc-pdb-pgoO2.dll
rmdir /s /q build\ditto32-msvc.exe
rmdir /s /q build\ditto64-msvc.exe

@goto :EOF

:__pgocompile64
call "%VCVARS%" x64 %VCVARSOPT%

@SET "CFLAGS=/Zi /W4 -I "%MSVC_VSBUILD%\DIA SDK\include""
@SET "LINKFLAGS=/SUBSYSTEM:CONSOLE /EXPORT:stack_walk_thread Kernel32.lib"

if not exist build\debuggee-pe64-msvc-pdb-pgoO1.dll\ mkdir build\debuggee-pe64-msvc-pdb-pgoO1.dll
cd build\debuggee-pe64-msvc-pdb-pgoO1.dll
CL.EXE %CFLAGS% /O1 /GL ..\..\debuggee.c /c
CL.EXE %CFLAGS% /std:c++20 /O1 /GL ..\..\debuggee_cc.cc /c
LINK.EXE debuggee.obj debuggee_cc.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-pgoO1.dll %LINKFLAGS% /LTCG /GENPROFILE
cd ..\..

if not exist build\debuggee-pe64-msvc-pdb-pgoO2.dll\ mkdir build\debuggee-pe64-msvc-pdb-pgoO2.dll
cd build\debuggee-pe64-msvc-pdb-pgoO2.dll
CL.EXE %CFLAGS% /O2 /GL ..\..\debuggee.c /c
CL.EXE %CFLAGS% /std:c++20 /O2 /GL ..\..\debuggee_cc.cc /c
LINK.EXE debuggee.obj debuggee_cc.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-pgoO2.dll %LINKFLAGS% /LTCG /GENPROFILE
cd ..\..

call scripts/run.bat pgo64

cd build\debuggee-pe64-msvc-pdb-pgoO1.dll
LINK.EXE debuggee.obj debuggee_cc.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-pgoO1.dll %LINKFLAGS% /LTCG /USEPROFILE
cd ..\..

cd build\debuggee-pe64-msvc-pdb-pgoO2.dll
LINK.EXE debuggee.obj debuggee_cc.obj /DLL /DEBUG /OUT:debuggee-pe64-msvc-pdb-pgoO2.dll %LINKFLAGS% /LTCG /USEPROFILE
cd ..\..

@goto :EOF

:pgo
@rem pgo isn't supported on 32bit targets
@cmd.exe /c "%0 __pgocompile64"
@goto :EOF

:EOF
