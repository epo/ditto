print "/* Automatically generated file: do not modify */\n";
print "\n";
print "struct mark_entry\n";
print "{\n";
print "    const WCHAR* name;\n";
print "    const unsigned int lineno;\n";
print "};\n";
print "\n";
print "static const struct mark_entry ditto_mark_entries[] = {\n";

my $str_enum = "enum mark_enum {\n";

while ($#ARGV >= 0)
{
    my $file = shift @ARGV;

    open(my $fh, $file) || die("Couldn't open $file");
    while (<$fh>)
    {
	if ($_ =~ /MARK\((.*)\)/)
	{
	    print "    {L\"$file\", $.},\n";
	    $str_enum = "    ${str_enum}MARK_$1,\n";
	}
    }
    close $fh;
}
print "};\n";
print "\n";
print "$str_enum};\n";
print "\n";
print "#define LINENO(x) (ditto_mark_entries[x].lineno)\n";
print "\n";
