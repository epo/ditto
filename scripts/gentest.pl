use strict;

my %targets;
my $WINEWOW32;
my $WINEWOW64;
my $WINE32;
my $WINE64;
my $WINENEWWOW;

my $num_quadruplets;

sub generate_aggregates($)
{
    my ($target) = @_;
    for my $k (keys %targets)
    {
	print "$k:$targets{$k}\n";
    }
    print ".PHONY:: help\n";
    print "help::\n";
    for my $k (keys %targets)
    {
	print "\t\@echo $k\n" if ($k ne '.PHONY:');
    }
    print STDERR "Generated $num_quadruplets quadruplets for $target\n";
}

sub generate_make_target()
{
    my ($code, $winedir, $tester, $subpcs, $debuggee) = @_;
    my $targetname = "target_${code}_$debuggee";
    my $winenick;
    my $sfx;

    # skip when not configured
    return if (length($winedir) <= 0);
    $winenick = 'Wine64' if ($winedir eq $WINE32);
    $winenick = 'Wine32' if ($winedir eq $WINE64);
    $winenick = 'Wine32 Wow' if ($winedir eq $WINEWOW32);
    $winenick = 'Wine64 Wow' if ($winedir eq $WINEWOW64);
    $winenick = 'Wine New Wow' if ($winedir eq $WINENEWWOW);
    # put in paranoid mode all pure $winedir
    $sfx = '-paranoid' if (index($winenick, 'Wow') <= 0);

    if ($debuggee =~ /debuggee--([^-]*)-(.*)--(.*)--(.*)\.dll/)
    {
	my $machine = $1;
	my $target = $2;
	my $compiler = $3;
	my $options = $4;
	my $debugfmt = substr($options, 0, index($options, '-'));

	print "${targetname}_internal:\n";
	print "\t\$(Q)echo -e \"[\${green}Testing\${reset}] ($winenick, ${tester}, " .
	    (defined($subpcs) ? "out-process $subpcs" : 'in-process') .
	    ", \${lblue}$debuggee\${reset})\"\n";
	print "\t\$(Q)$winedir/wine build/${tester}/${tester} test " .
	    (defined($subpcs) ? "--subprocess build/${subpcs}/${subpcs} " : "") .
	    "build/${debuggee}/${debuggee}\n";

	print "$targetname: ditto debuggee remote\n";
	print "\t\$(Q)+if [[ -t 1 ]] && [[ ! -v DITTO_TEST_COLOR ]]; then export DITTO_TEST_COLOR=1; fi &&\\\n";
	print "\t\tmake --no-print-directory --output-sync=recurse ${targetname}_internal\n";

	# hack to generate a .PHONY:: make target
	$targets{".PHONY:"} .= " $targetname ${targetname}_internal";
	$targets{"test-ditto$sfx"} .= " $targetname";
	$targets{"test$sfx-$machine"} .= " $targetname";
	$targets{"test$sfx-$compiler"} .= " $targetname";
	$targets{"test$sfx-$target"} .= " $targetname";
	$targets{"test$sfx-$debugfmt"} .= " $targetname";
	# allow for compiler and debug format to aggregate on various values
	# like dwarf4 => dwarf, gcc-12.3 => gcc
	if ($compiler =~ /^([a-z]+)([\-\.0-9]*)$/)
	{
	    $targets{"test$sfx-$1"} .= " $targetname";
	}
	if ($debugfmt =~ /^([a-z]+)([\-\.0-9]*)$/)
	{
	    $targets{"test$sfx-$1"} .= " $targetname";
	}
	$num_quadruplets++ if (index($winenick, 'Wow') <= 0);
    }
}

sub generate_log_target()
{
    my ($winedir, $ditto, $debuggee) = @_;

    # hack to generate a .PHONY:: make target
    $targets{".PHONY:"} .= " log_$debuggee";
    $targets{"log"} .= " log_$debuggee";

    print "log_$debuggee: ditto debuggee\n";
    print "\t$winedir/wine build/$ditto/$ditto matrix build/$debuggee/$debuggee >& build/$debuggee/matrix-wine.log\n";
    print "\t$winedir/wine build/$ditto/$ditto edge   build/$debuggee/$debuggee >& build/$debuggee/edge-wine.log\n";
    print "\t$winedir/wine build/$ditto/$ditto stats  build/$debuggee/$debuggee >& build/$debuggee/stats-wine.log\n";
    print "\t$winedir/wine build/$ditto/$ditto map --with-inlined    build/$debuggee/$debuggee >& build/$debuggee/map-addr-wine.log\n";
    print "\t$winedir/wine build/$ditto/$ditto map --with-inlined --hierarchy build/$debuggee/$debuggee >& build/$debuggee/map-hierarchy-wine.log\n";
}

sub generate_from()
{
    my ($build) = @_;
    my $targetname;

    my $ditto32 = 'ditto32.exe';
    my $ditto64 = 'ditto64.exe';

    # hack to generate a .PHONY:: make target
    $targets{".PHONY:"} .= " test log";

    opendir my $dh, $build or die "$0: opendir $build: $!";

    while (defined(my $debuggee = readdir($dh)))
    {
	next if (! -d "$build/$debuggee" || $debuggee !~ /debuggee--.*\.dll/);
	if ($debuggee =~ /debuggee--([^-]*)-(.*)--(.*)--(.*)\.dll/)
	{
	    my $machine = $1;
	    my $target = $2;
	    my $compiler = $3;
	    my $options = $4;

	    if ($machine eq 'i686')
	    {
		&generate_make_target('E32', $WINEWOW32,  $ditto32, $ditto32, $debuggee);
		&generate_make_target('I32', $WINEWOW32,  $ditto32, undef,    $debuggee);
		&generate_make_target('WOW', $WINEWOW64,  $ditto64, $ditto32, $debuggee);

		&generate_make_target('F32', $WINE32,     $ditto32, $ditto32, $debuggee);
		&generate_make_target('J32', $WINE32,     $ditto32, undef,    $debuggee);

		&generate_make_target('G32', $WINENEWWOW, $ditto32, $ditto32, $debuggee);
		&generate_make_target('K32', $WINENEWWOW, $ditto32, undef,    $debuggee);
		&generate_make_target('NWW', $WINENEWWOW, $ditto64, $ditto32, $debuggee);

		&generate_log_target($WINEWOW32, $ditto32, $debuggee);
		next;
	    }
	    if ($machine eq 'x86_64')
	    {
		&generate_make_target('E64', $WINEWOW64,  $ditto64, $ditto64, $debuggee);
		&generate_make_target('I64', $WINEWOW64,  $ditto64, undef,    $debuggee);

		&generate_make_target('F64', $WINE64,     $ditto64, $ditto64, $debuggee);
		&generate_make_target('J64', $WINE64,     $ditto64, undef,    $debuggee);

		&generate_make_target('G64', $WINENEWWOW, $ditto64, $ditto64, $debuggee);
		&generate_make_target('K64', $WINENEWWOW, $ditto64, undef,    $debuggee);

		&generate_log_target($WINEWOW64, $ditto64, $debuggee);
		next;
	    }
	}
	print STDERR "Unknown directory $debuggee\n";
    }
    closedir $dh;
}

sub generate_for_ditto()
{
    print "# Automatically generated file. Do not modify.\n\n";
    &generate_from('build');
    print("test::test-ditto\n");
    print("test-paranoid::test-ditto-paranoid\n");
    &generate_aggregates('ditto');
    return 0;
}

sub generate_wdtp_target($$$$$)
{
    my ($build, $objdir, $winedbg_machine, $wdtp, $test) = @_;

    my $targetname = "wdtp_${winedbg_machine}_${wdtp}_${test}";
    # IMAGEHLP_MODULE stores module name in a 32 char array, so crop accordingly
    # (until winedbg is fixed to use the long module format)
    my $short_wdtp = substr($wdtp, 0, 31);

    if ($wdtp =~ /wdtp--([^-]*)-(.*)--(.*)--(.*)\.exe/)
    {
	my $wdtp_machine = $1;
	my $target = $2;
	my $compiler = $3;
	my $options = $4;
	my $debugfmt = substr($options, 0, index($options, '-'));

	print "${targetname}: wdtp remote\n";
	print "\t\$(Q)echo -e \"[\${green}Testing\${reset}] (${test}.wdtp against \${lblue}${wdtp}.exe\${reset})\"\n";
	print "\t\$(Q)-${objdir}/wine ${objdir}/programs/winedbg/${winedbg_machine}/winedbg.exe --file ${build}/${test}.wdtp ${build}/${wdtp}/${wdtp} `cat ${build}/${test}.args` > ${build}/${test}-${wdtp}.out\n";
	print "\t\$(Q)-perl scripts/wdtp.pl compare --var module=${short_wdtp} ${build}/${test}-${wdtp}.out src/wdtp/${test}.tmpl ${build}/wdtp_markers\n";

	$targets{".PHONY:"} .= " $targetname";
	$targets{"test-wdtp"} .= " $targetname";
	$targets{"test-wdtp-${test}"} .= " $targetname";
	$targets{"test-$wdtp_machine"} .= " $targetname";
	$targets{"test-$compiler"} .= " $targetname";
	$targets{"test-$target"} .= " $targetname";
	$targets{"test-$debugfmt"} .= " $targetname";
	$num_quadruplets++;
    }
}

sub generate_for_wdtp()
{
    my $build = 'build';
    $num_quadruplets = 0;

    my @tests = ('expr', 'type', 'xpoint');
    opendir my $dh, $build or die "$0: opendir $build: $!";
    while (defined(my $wdtp = readdir($dh)))
    {
	next if (! -d "$build/$wdtp" || $wdtp !~ /wdtp--.*\.exe/);
	if ($wdtp =~ /wdtp--([^-]*)-(.*)--(.*)--(.*)\.exe/)
	{
	    my $machine = $1;
	    if ($machine eq 'i686')
	    {
		for my $test (@tests)
		{
		    &generate_wdtp_target($build, $WINEWOW32, 'i386-windows',   $wdtp, $test);
		    &generate_wdtp_target($build, $WINEWOW64, 'x86_64-windows', $wdtp, $test);
		}
	    }
	    elsif ($machine eq 'x86_64')
	    {
		for my $test (@tests)
		{
		    &generate_wdtp_target($build, $WINEWOW64, 'x86_64-windows', $wdtp, $test);
		}
	    }
	    else
	    {
		print STDERR "Unknown directory $wdtp\n";
	    }
	}
    }
    closedir $dh;
    print(".PHONY::test-wdtp\n");
    print("test::test-wdtp\n");
    &generate_aggregates('wdtp');
    return 0;
}

sub set_wine_objdir()
{
    for my $arg (@ARGV)
    {
	$WINEWOW32=substr($arg, 8)  if (index($arg, "--wow32=") == 0);
	$WINEWOW64=substr($arg, 8)  if (index($arg, "--wow64=") == 0);
	$WINE32=substr($arg, 5)     if (index($arg, "--32=") == 0);
	$WINE64=substr($arg, 5)     if (index($arg, "--64=") == 0);
	$WINENEWWOW=substr($arg, 9) if (index($arg, "--newwow=") == 0);
    }
    die "WINEWOW32 and WINEWOW64 should be configured in Makefile.config\n" if (length($WINEWOW32) <= 0 || length($WINEWOW64) <= 0);
}

die "No arguments\n" if ($#ARGV < 0);
my $cmd = shift;
&set_wine_objdir();
exit &generate_for_ditto() if ($cmd eq 'ditto');
exit &generate_for_wdtp()  if ($cmd eq 'wdtp');
die "Unknonwn target $cmd\n";
