@echo off

setlocal

set green=[92m
set lblue=[96m
set reset=[0m

set DITTO64=ditto--x86_64-windows--msvc.exe
set DITTO32=ditto--i686-windows--msvc.exe

if "%~1"=="" goto :test
if "%~1"=="test" goto :test
if "%~1"=="log" goto :log
if "%~1"=="dump" goto :dump
if "%~1"=="pgo64" goto :pgo
if "%~1"=="pgo" goto :pgo

echo Unknown argument '%1'

goto :EOF

:test

rem drwxr-xr-x. 1 eric eric    480 21 févr. 17:51 debuggee--i686-windows--msvc--pdb-O1.dll
rem drwxr-xr-x. 1 eric eric    480 21 févr. 17:51 debuggee--i686-windows--msvc--pdb-O2.dll
rem drwxr-xr-x. 1 eric eric    500 21 févr. 17:51 debuggee--x86_64-windows--msvc--pdb-O1.dll
rem drwxr-xr-x. 1 eric eric    500 21 févr. 17:51 debuggee--x86_64-windows--msvc--pdb-O2.dll

echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto64-msvc, %lblue%debuggee--x86_64-windows--msvc--pdb-O1%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO64%\%DITTO64% build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, Internal, %lblue%debuggee--x86_64-windows--msvc--pdb-O1%reset%^)
build\%DITTO64%\%DITTO64% test build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto64-msvc, %lblue%debuggee--x86_64-windows--msvc--pdb-O2%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO64%\%DITTO64% build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, Internal, %lblue%debuggee--x86_64-windows--msvc--pdb-O2%reset%^)
build\%DITTO64%\%DITTO64% test build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll
echo [%green%Testing%reset%] ^(Windows, ditto32-msvc, ditto32-msvc, %lblue%debuggee--i686-windows--msvc--pdb-O1%reset%^)
build\%DITTO32%\%DITTO32% test --subprocess build\%DITTO32%\%DITTO32% build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll
echo [%green%Testing%reset%] ^(Windows, ditto32-msvc, Internal, %lblue%debuggee--i686-windows--msvc--pdb-O1%reset%^)
build\%DITTO32%\%DITTO32% test build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll
echo [%green%Testing%reset%] ^(Windows, ditto32-msvc, ditto32-msvc, %lblue%debuggee--i686-windows--msvc--pdb-O2%reset%^)
build\%DITTO32%\%DITTO32% test --subprocess build\%DITTO32%\%DITTO32% build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll
echo [%green%Testing%reset%] ^(Windows, ditto32-msvc, Internal, %lblue%debuggee--i686-windows--msvc--pdb-O2%reset%^)
build\%DITTO32%\%DITTO32% test build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto32-msvc, %lblue%debuggee--i686-windows--msvc--pdb-O1%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO32%\%DITTO32% build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto32-msvc, %lblue%debuggee--i686-windows--msvc--pdb-O2%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO32%\%DITTO32% build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll

goto :EOF

:log

echo [%green%Log%reset%] debuggee--x86_64-windows--msvc--pdb-O1
build\%DITTO64%\%DITTO64% matrix build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\matrix-windows.log
build\%DITTO64%\%DITTO64% edge   build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\edge-windows.log
build\%DITTO64%\%DITTO64% stats  build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\stats-windows.log
build\%DITTO64%\%DITTO64% map --with-inlined    build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\map-addr-windows.log
build\%DITTO64%\%DITTO64% map --with-inlined --hierarchy build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\map-hierarchy-windows.log

echo [%green%Log%reset%] debuggee--x86_64-windows--msvc--pdb-O2
build\%DITTO64%\%DITTO64% matrix build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\matrix-windows.log
build\%DITTO64%\%DITTO64% edge   build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\edge-windows.log
build\%DITTO64%\%DITTO64% stats  build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\stats-windows.log
build\%DITTO64%\%DITTO64% map --with-inlined    build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\map-addr-windows.log
build\%DITTO64%\%DITTO64% map --with-inlined --hierarchy build\debuggee--x86_64-windows--msvc--pdb-O2.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\map-hierarchy-windows.log

echo [%green%Log%reset%] debuggee--i686-windows--msvc--pdb-O1
build\%DITTO32%\%DITTO32% matrix build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\matrix-windows.log
build\%DITTO32%\%DITTO32% edge   build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\edge-windows.log
build\%DITTO32%\%DITTO32% stats  build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\stats-windows.log
build\%DITTO32%\%DITTO32% map --with-inlined    build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\map-addr-windows.log
build\%DITTO32%\%DITTO32% map --with-inlined --hierarchy build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\map-hierarchy-windows.log

echo [%green%Log%reset%] debuggee--i686-windows--msvc--pdb-O2
build\%DITTO32%\%DITTO32% matrix build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\matrix-windows.log
build\%DITTO32%\%DITTO32% edge   build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\edge-windows.log
build\%DITTO32%\%DITTO32% stats  build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\stats-windows.log
build\%DITTO32%\%DITTO32% map --with-inlined    build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\map-hierarchy-windows.log
build\%DITTO32%\%DITTO32% map --with-inlined --hierarchy build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\map-hierarchy-windows.log

goto :EOF

:dump

echo [%green%Dump%reset%] debuggee--x86_64-windows--msvc--pdb-O1
build\%DITTO64%\%DITTO64% dump build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O1.dll > build\debuggee--x86_64-windows--msvc--pdb-O1\dump-windows.log

echo [%green%Dump%reset%] debuggee--x86_64-windows--msvc--pdb-O2
build\%DITTO64%\%DITTO64% dump build\debuggee--x86_64-windows--msvc--pdb-O1.dll\debuggee--x86_64-windows--msvc--pdb-O2.dll > build\debuggee--x86_64-windows--msvc--pdb-O2\dump-windows.log

echo [%green%Dump%reset%] debuggee--i686-windows--msvc--pdb-O1
build\%DITTO32%\%DITTO32% dump build\debuggee--i686-windows--msvc--pdb-O1.dll\debuggee--i686-windows--msvc--pdb-O1.dll > build\debuggee--i686-windows--msvc--pdb-O1\dump-windows.log

echo [%green%Dump%reset%] debuggee--i686-windows--msvc--pdb-O2
build\%DITTO32%\%DITTO32% dump build\debuggee--i686-windows--msvc--pdb-O2.dll\debuggee--i686-windows--msvc--pdb-O2.dll > build\debuggee--i686-windows--msvc--pdb-O2\dump-windows.log

goto :EOF

:pgo

echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto64-msvc, %lblue%debuggee-pe64-msvc-pdb-pgoO1%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO64%\%DITTO64% build\debuggee-pe64-msvc-pdb-pgoO1.dll\debuggee-pe64-msvc-pdb-pgoO1.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, Internal, %lblue%debuggee-pe64-msvc-pdb-pgoO1%reset%^)
build\%DITTO64%\%DITTO64% test build\debuggee-pe64-msvc-pdb-pgoO1.dll\debuggee-pe64-msvc-pdb-pgoO1.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, ditto64-msvc, %lblue%debuggee-pe64-msvc-pdb-pgoO2%reset%^)
build\%DITTO64%\%DITTO64% test --subprocess build\%DITTO64%\%DITTO64% build\debuggee-pe64-msvc-pdb-pgoO2.dll\debuggee-pe64-msvc-pdb-pgoO2.dll
echo [%green%Testing%reset%] ^(Windows, ditto64-msvc, Internal, %lblue%debuggee-pe64-msvc-pdb-pgoO2%reset%^)
build\%DITTO64%\%DITTO64% test build\debuggee-pe64-msvc-pdb-pgoO2.dll\debuggee-pe64-msvc-pdb-pgoO2.dll
goto :EOF

:EOF

endlocal
