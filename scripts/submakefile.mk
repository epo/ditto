V?=0

red=`tput setaf 1`
green=`tput setaf 10`
lblue=`tput setaf 14`
reset=`tput sgr0`

ifeq ($(V), 0)
Q = @
define colorecho
	@echo -e "[$(green)$1$(reset)] $2"
endef
else
define colorecho
endef
endif

ifeq (,$(wildcard $(WINEWOW64)/dlls/kernel32/x86_64-windows))
	WINEGCC_LIB_SUFFIX = --lib-suffix=.cross.a
endif
