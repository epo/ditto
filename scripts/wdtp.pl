# !/usr/bin/perl -w

use strict;

my $verbose = 0;

my %re_keywords = ('%hex%'     => '(0x)?[0-9a-zA-Z]+',
		   '%integer%' => '[0-9]+',
		   '%anypath%' => '([a-zA-Z]:)?[a-zA-Z0-9_\\\\/.\\-]*[\\\\/]' # FIXME not 100% perfect (double \\ allowed for example)
    );

# transform all variables into their respective regexp...
# quoting the rest of the string
sub rewrite_re($)
{
    my ($re) = @_;
    my $ret = '';
    my $start;
    my $end = 0;

    while (($start = index($re, '%', $end)) >= 0)
    {
	$ret .= quotemeta substr($re, $end, $start - $end);
	$end = index($re, '%', $start + 1);
	my $kw = substr($re, $start, $end - $start + 1);
	$ret .= defined($re_keywords{$kw}) ? $re_keywords{$kw} : substr($kw, 1, -1);
	$end++;
    }
    $ret .= quotemeta substr($re, $end);
    return $ret;
}

sub compare($$)
{
    my ($input, $template, $marker) = @_;
    my $line_input;
    my $line_template;
    my $re_line_template;
    my $diffs = 0;
    my $last_error;
    my $lineno_input = 0;
    my $lineno_template = 0;
    my $in_resync = 1;

    open(my $fh_input, "$input") || die("Couldn't open $input");
    open(my $fh_template, "$template") || die("Couldn't open $template");

    # insert marker file into variables
    open(my $fh_marker, $marker) || die("Couldn't open $marker");
    while (<$fh_marker>)
    {
	chomp($_);
	if ($_ =~ /^([^ ]*) ([^ ]*) ([^ ]*)$/)
	{
	    $re_keywords{"%file($1)%"} = $2;
	    $re_keywords{"%line($1)%"} = $3;
	}
	else
	{
	    die "Unexpected marker $_\n";
	}
    }
    close($fh_marker);

    for (;;)
    {
	$line_input = <$fh_input>;
	$line_template = <$fh_template>;
	last if (!defined($line_input) || !defined($line_template));

	$lineno_input++;
	$lineno_template++;
	chomp($line_input);
	# FIXME hack, should find better solution
	if ($last_error && $line_input =~ /^[ \t]*$/)
	{
	    $line_input = <$fh_input>;
	    chomp($line_input);
	    $lineno_input++;
	}
	for (;;)
	{
	    chomp($line_template);
	    last if (index($line_template, '##:') != 0);
	    $line_template = <$fh_template>;
	    $lineno_template++;
	}
	$last_error = 0;
	if ($line_template =~ /^\^([a-z]*)\^/)
	{
	    $line_template = $';
	    die("Unsupported keyword $1\n") if ($1 ne 'resync');
	    print("enter resync with <$line_template>\n") if ($verbose > 1);
	    $in_resync = 1;
	}
	$re_line_template = &rewrite_re($line_template);
	print "<$line_input> <$line_template> <$re_line_template> <$in_resync>\n" if ($verbose);
	do {
	    if (!($line_input =~ m/($re_line_template)/))
	    {
		if ($in_resync)
		{
		    print("skip <$line_input> while in resync\n") if ($verbose);
		    $line_input = <$fh_input>;
		    last if (!defined($line_input));
		    next;
		}
		$diffs++;
		print "Diff between $input:$lineno_input $template:$lineno_template\n";
		print "<<<\n";
		print "$line_input\n";
		print "---\n";
		print "$line_template\n";
		print ">>>\n";
		$last_error = 1;
	    }
	    else
	    {
		$in_resync = 0;
	    }
	} while ($in_resync);
	undef $line_template;
    }
    if ($line_input)
    {
	print "$lineno_input $lineno_template\n<<<\n";
	do
	{
	    print $line_input;
	    $lineno_input++;
	    $diffs++;
	} while (defined($line_input = <$fh_input>));
	print "---\n>>>\n";
    }
    if ($line_template)
    {
	if ($in_resync)
	{
	    print "$lineno_input ^resync^$lineno_template\n<<<\n---\n";
	}
	else
	{
	    print "$lineno_input $lineno_template\n<<<\n---\n";
	}
	do
	{
	    print $line_template;
	    $lineno_template++;
	    $diffs++;
	} while (defined($line_template = <$fh_template>));
	print ">>>\n";
    }
    close $fh_input;
    close $fh_template;

    print "$lineno_template template lines matched against $lineno_input output lines: $diffs difference(s)\n";
    return $diffs != 0;
}

sub generate_marker(@)
{
    my $lineno = 1;

    foreach my $file (@_)
    {
	open(my $fh, $file) || die("Couldn't open $file");
	while (<$fh>)
	{
	    print "$1 $file $lineno\n" if ($_ =~ /WDTP_INSN_BARRIER\((.+)\)/);
	    $lineno++;
	}
	close($fh);
    }
}

# FIXME don't use globals
my %markers_file;
my %markers_lineno;

sub rewrite_marker()
{
    my ($line) = @_;

    for (;;)
    {
	if ($line =~ /%line\((.*)\)%/)
	{
	    die "Undefined marker $1\n" if (!defined($markers_lineno{$1}));
	    $line = $` . $markers_lineno{$1} . $';
	}
	elsif ($line =~ /%file\((.*)\)%/)
	{
	    die "Undefined marker $1\n" if (!defined($markers_file{$1}));
	    $line = $` . $markers_file{$1} . $';
	}
	else
	{
	    return $line;
	}
    }
}

sub generate_input()
{
    my ($input, $marker) = @_;

    open(my $fh, $marker) || die("Couldn't open $marker");
    while (<$fh>)
    {
	chomp($_);
	if ($_ =~ /^([^ ]*) ([^ ]*) ([^ ]*)$/)
	{
	    $markers_file{$1} = $2;
	    $markers_lineno{$1} = $3;
	}
	else
	{
	    die "Unexpected marker $_\n";
	}
    }
    close($fh);
    open(my $fh, $input) || die("Couldn't open $input");
    while (<$fh>)
    {
	print &rewrite_marker($_);
    }
    close($fh);
    return 0;
}

sub generate_arguments()
{
    my ($input) = @_;

    open(my $fh, $input) || die("Couldn't open $input");
    while (<$fh>)
    {
	print "$1" if ($_ =~ /^#!\s*(.*)$/g);
	last;
    }
    close($fh);

    return 0;
}

exit 127 if ($#ARGV < 0);
my $cmd = shift @ARGV;
if ($cmd eq 'compare')
{
    while ($ARGV[0] eq '--var' && $#ARGV >= 1)
    {
	if ($ARGV[1] =~ /^([a-z]+)=(.*)$/)
	{
	    $re_keywords{"%$1%"} = lc $2;
	    shift @ARGV;
	    shift @ARGV;
	}
	else
	{
	    die("Bad variable $ARGV[1]\n");
	}
    }
    exit 127 if ($#ARGV != 2);
    exit &compare($ARGV[0], $ARGV[1], $ARGV[2]);
}
elsif ($cmd eq 'markers')
{
    &generate_marker(shift @ARGV) while ($#ARGV >= 0);
    exit 0;
}
elsif ($cmd eq 'input')
{
    exit 127 if ($#ARGV != 1);
    exit &generate_input($ARGV[0], $ARGV[1]);
}
elsif ($cmd eq 'args')
{
    exit 127 if ($#ARGV != 0);
    exit &generate_arguments($ARGV[0]);
}

die "Unknown command $cmd";
