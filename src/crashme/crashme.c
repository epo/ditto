/*
 * DITTO (tool to testing debug information).
 * Generating exceptions.
 *
 * Copyright (C) 2023-2024 Eric Pouech for CodeWeavers.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define _CRT_SECURE_NO_WARNINGS 1
#include <windows.h>
#include <dbghelp.h>
#include <stdlib.h>
#include <stdio.h>
#include <winternl.h>
#include <psapi.h>

#if defined(_MSC_VER)
#  include <intrin.h>
#else
#  include "wine/exception.h"
#endif

#include "ids.h"

#if defined(__GNUC__)
#  define INLINE inline __attribute__((always_inline))
#  define NOINLINE __attribute__((noinline))
#  define EXPORT
#elif defined(_MSC_VER)
#  define INLINE __forceinline
#  define NOINLINE __declspec(noinline)
#  define EXPORT __declspec(dllexport)
#  define ARRAY_SIZE ARRAYSIZE
#else
#  define INLINE
#  define NOINLINE
#  define EXPORT
#endif

#if defined(__TRY) && defined(__ENDTRY)
#  define HAS_WINE_SEH 1
#endif

#if defined(_MSC_VER)
#  define HAS_NATIVE_SEH 1
#endif

struct crash_with
{
    unsigned with; /* one of IDC_WITH constants */
    DWORD crash_exception;
    DWORD count;
    unsigned with_wine_seh;
    unsigned with_native_seh;
    unsigned with_vectored_seh;
};

static FILE *log_stream;

static void usage(void)
{
    printf("crashme [options]\n");
    printf("How to crash (one of the following):\n");
    printf("  (default)        by calling DbgBreak()\n");
    printf("  --segfault       by generating an NULL deref\n");
    printf("  --exception NN   by raising an exception (0 args)\n");
    printf("Install handler(s) around the crash\n");
    printf("  (default)        no crash handler\n");
    printf("  --wine-seh       install Wine SEH handler\n");
#if !defined(HAS_WINE_SEH)
    printf("                   not available in this version (compiler doesn't support it)\n");
#endif
    printf("  --seh            install native SEH handler\n");
#if !defined(HAS_NATIVE_SEH)
    printf("                   not available in this version (compiler doesn't support it)\n");
#elif defined(HAS_WINE_SEH)
    printf("                   not to be used with --wine-seh\n");
#endif
    printf("  --vectored       install vectored SEH handler\n");
    printf("                   (can be used in conjonction with one of previous seh options)\n");
    printf("What to do before crashing\n");
    printf("  (default)        display a dialog box to modify crash options\n");
    printf("  --sleep NN       waits for NN ms before crashing (for example, to have time to attach a debugger)\n");
    printf("  --log XX         writes messages to file XX (default message boxes)\n");

    exit(1);
}

static void message(const char *format, ...)
{
    va_list vargs;

    va_start(vargs, format);
    if (log_stream)
        vfprintf(log_stream, format, vargs);
    else
    {
        char buffer[1024];
        vsprintf(buffer, format, vargs);
        printf("%s\n", buffer);
//        MessageBoxA(NULL, buffer, "printf", MB_OK);
    }
    va_end(vargs);
}

static inline void dump_data( const BYTE *ptr, unsigned int size, const char *prefix )
{
    unsigned offset = 0, i;
    char tmp[256];

    if (!ptr)
    {
        message("%s%08x: NULL\n", prefix, 0);
        return;
    }
    while (offset < size)
    {
        unsigned len = min(size - offset, 16);
        sprintf(tmp, "%s%08x: ", prefix, offset);
        for (i = 0; i < len; i++)
            sprintf(tmp + strlen(tmp), "%02x%c", ptr[i], i == 7 ? '-' : ' ' );
        for (; i < 16; i++)
            sprintf(tmp + strlen(tmp), "  %c", i == 7 ? '-' : ' ' );
        strcat(tmp, " ");
        for (i = 0; i < len; i++)
            sprintf(tmp + strlen(tmp), "%c", isprint(ptr[i]) ? ptr[i] : '.' );
        message("%s\n", tmp);
        offset += len;
        ptr += len;
    }
}

EXPORT NOINLINE DWORD function_top(DWORD a, struct crash_with *crash)
{
    TEB *teb = NtCurrentTeb();
    if (log_stream)
    {
        message("Current TEB %p:\n", teb);
        dump_data((const BYTE*)teb, sizeof(*teb), "\t");
        message("PE image (main module=%p):\n", GetModuleHandleW(NULL));
        dump_data((const BYTE*)GetModuleHandleW(NULL), 0x21000, "\t");
    }
    else
    {
        DWORD_PTR *x = (DWORD_PTR*)teb;
        message("TEB=%p [%Ix %Ix %Ix %Ix %Ix %Ix %Ix]\n", teb, x[0], x[1], x[2], x[3], x[4], x[5], x[6]);
    }
    switch (crash->with)
    {
    case IDC_WITH_SEGFAULT:
        {
            char* volatile ptr = (char *)(DWORD_PTR)11;
            a += *ptr;
        }
        break;
    case IDC_WITH_EXCEPTION:
        RaiseException(crash->crash_exception, 0, 0, NULL);
        break;
    case IDC_WITH_DBGBREAK:
        DebugBreak();
        break;
    }
    return a ^ 0xa500005a;
}

EXPORT NOINLINE int function_medium(int b, struct crash_with *crash)
{
    return function_top(2 * b, crash) * (b + 1);
}

EXPORT BOOL CALLBACK my_cb(const WCHAR* name, DWORD64 base, void* usr)
{
    struct crash_with *crash = (struct crash_with*)usr;

    crash->count ^= function_medium((DWORD)base | (DWORD)(base >> 32), crash);
    crash->count = (crash->count * name[0]) + name[1];

    return FALSE;
}

static INT_PTR CALLBACK dlg_proc(HWND dlg, UINT msg, WPARAM wparam, LPARAM lparam)
{
    struct crash_with *crash;
    char tmp[16];

    switch (msg)
    {
    case WM_INITDIALOG:
        crash = (struct crash_with*)lparam;
        SetWindowLongPtrW(dlg, DWLP_USER, (DWORD_PTR)crash);
        SendDlgItemMessageW(dlg, crash->with, BM_SETCHECK, BST_CHECKED, 0);
        snprintf(tmp, ARRAYSIZE(tmp), "%lu", crash->crash_exception);
        SendDlgItemMessageA(dlg, IDC_WITH_EXCEPTION_VALUE, WM_SETTEXT, 0, (DWORD_PTR)tmp);
        if (crash->with_wine_seh)
            SendDlgItemMessageW(dlg, IDC_SEH_WINE, BM_SETCHECK, BST_CHECKED, 0);
        if (crash->with_native_seh)
            SendDlgItemMessageW(dlg, IDC_SEH_NATIVE, BM_SETCHECK, BST_CHECKED, 0);
        if (crash->with_vectored_seh)
            SendDlgItemMessageW(dlg, IDC_SEH_VECTORED, BM_SETCHECK, BST_CHECKED, 0);
#if !defined(HAS_NATIVE_SEH)
        EnableWindow(GetDlgItem(dlg, IDC_SEH_NATIVE), FALSE);
#endif
#if !defined(HAS_WINE_SEH)
        EnableWindow(GetDlgItem(dlg, IDC_SEH_WINE), FALSE);
#endif
        return TRUE;
   case WM_COMMAND:
        crash = (struct crash_with*)GetWindowLongPtrW(dlg, DWLP_USER);
        switch (LOWORD(wparam))
        {
        case IDC_WITH_SEGFAULT:
        case IDC_WITH_DBGBREAK:
        case IDC_WITH_EXCEPTION:
            crash->with = LOWORD(wparam);
            break;
        case IDC_SEH_NATIVE:
            crash->with_native_seh = SendDlgItemMessageA(dlg, IDC_SEH_NATIVE, BM_GETCHECK, 0, 0) == BST_CHECKED;
            break;
        case IDC_SEH_WINE:
            crash->with_wine_seh = SendDlgItemMessageA(dlg, IDC_SEH_WINE, BM_GETCHECK, 0, 0) == BST_CHECKED;
            break;
        case IDC_SEH_VECTORED:
            crash->with_vectored_seh = SendDlgItemMessageA(dlg, IDC_SEH_VECTORED, BM_GETCHECK, 0, 0) == BST_CHECKED;
            break;
        case IDOK:
            GetWindowTextA(GetDlgItem(dlg, IDC_WITH_EXCEPTION_VALUE), tmp, sizeof(tmp));
            crash->crash_exception = atoi(tmp);
            EndDialog(dlg, 1);
            PostQuitMessage(0);
            break;
        case IDCANCEL:
            EndDialog(dlg, 0);
            PostQuitMessage(0);
            break;
        }
        return TRUE;
    }
    return FALSE;
}

#if defined(HAS_NATIVE_SEH)
static int msc_filter_exception(int code, EXCEPTION_POINTERS *ex)
{
    message("Exception %x %lx\n", code, ex->ExceptionRecord->ExceptionCode);
    return EXCEPTION_EXECUTE_HANDLER;
}
#endif
#if defined(HAS_WINE_SEH)
LONG CALLBACK wine_exception_filter( EXCEPTION_POINTERS *ex )
{
    message("Exception %lx\n", ex->ExceptionRecord->ExceptionCode);
    return EXCEPTION_EXECUTE_HANDLER;
}
#endif

static LONG WINAPI vectored_exception_handler(EXCEPTION_POINTERS *ex)
{
    message("Vectored> Exception %lx\n", ex->ExceptionRecord->ExceptionCode);
    return EXCEPTION_CONTINUE_SEARCH;
}

static void generate_stack_and_crash(struct crash_with *crash)
{
    if (crash->with_vectored_seh)
        AddVectoredExceptionHandler(1, vectored_exception_handler);
    /* interleave in callstack mingw & msvc module */
    SymInitializeW(GetCurrentProcess(), NULL, TRUE);
    if (crash->with_wine_seh)
    {
#if defined(HAS_WINE_SEH)
        __TRY
        {
            SymEnumerateModulesW64(GetCurrentProcess(), my_cb, crash);
        }
        __EXCEPT(wine_exception_filter)
        {
            message("got wine exception\n");
        }
        __ENDTRY
#endif
    }
    else if (crash->with_native_seh)
    {
#if defined(HAS_NATIVE_SEH)
        __try
        {
            SymEnumerateModulesW64(GetCurrentProcess(), my_cb, crash);
        }
        __except(msc_filter_exception(GetExceptionCode(), GetExceptionInformation()))
        {
            message("got native exception\n");
        }
#endif
    }
    else
        SymEnumerateModulesW64(GetCurrentProcess(), my_cb, crash);

    SymCleanup(GetCurrentProcess());
}

int main(int argc, char* argv[])
{
    struct crash_with crash = {IDC_WITH_DBGBREAK, 0, (UINT)argc};
    DWORD sleep = (DWORD)-1;
    int i;

    for (i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "--segfault"))
            crash.with = IDC_WITH_SEGFAULT;
        else if (!strcmp(argv[i], "--exception") && i + 1 < argc)
        {
            crash.with = IDC_WITH_EXCEPTION;
            crash.crash_exception = atoi(argv[++i]);
        }
        else if (!strcmp(argv[i], "--sleep") && i + 1 < argc)
            sleep = atoi(argv[++i]);
        else if (!strcmp(argv[i], "--log") && i + 1 < argc)
        {
            i++;
            if (!strcmp(argv[i], "--"))
                log_stream = stdout;
            else
                log_stream = fopen(argv[i], "w+");
            if (!log_stream)
            {
                printf("Unable to open log file %s\n", argv[i]);
                exit(2);
            }
            setvbuf(log_stream, NULL, _IONBF, 0);
        }
        else if (!strcmp(argv[i], "--wine-seh"))
        {
#if defined(HAS_WINE_SEH)
            crash.with_wine_seh = TRUE;
#else
            message("Wine SEH not supported by compiler, aborting\n");
            exit(1);
#endif
        }
        else if (!strcmp(argv[i], "--seh"))
        {
#if defined(HAS_NATIVE_SEH)
            crash.with_native_seh = TRUE;
#else
            message("Native SEH not supported by compiler, aborting\n");
            exit(1);
#endif
        }
        else if (!strcmp(argv[i], "--vectored"))
            crash.with_vectored_seh = TRUE;
        else
        {
            message("Unknown option: %s\n", argv[i]);
            usage();
        }
    }
    if (crash.with_wine_seh && crash.with_native_seh)
    {
        message("Can't use --wine-seh and --seh simultaneously\n");
        usage();
    }
    message("I am %lu\n", GetCurrentProcessId());
    if (sleep == (DWORD)-1)
    {
        if (!DialogBoxParamW(GetModuleHandleW(NULL), MAKEINTRESOURCEW(IDD_OPTION), NULL, dlg_proc, (DWORD_PTR)&crash))
        {
            message("canceled\n");
            exit(1);
        }
    }
    else
        Sleep(sleep);

    message("ready to crash\n");
    generate_stack_and_crash(&crash);

    return (int)crash.count;
}
