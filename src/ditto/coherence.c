/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Coherence tests.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "test.h"
#include "ditto.h"
#include <oaidl.h>

static unsigned S_with_details = 1;

#define failure(a, b, c) _failure(__LINE__, (a), (b), (c))
static BOOL _failure(unsigned line, const struct vacuum* v, DWORD id, IMAGEHLP_SYMBOL_TYPE_INFO ti)
{
    char buffer[sizeof(VARIANT)];
    BOOL ret = SymGetTypeInfo(v->dbg->process, v->base, id, ti, buffer);
    ok_(__FILE__, line)(!ret, "Expecting SymGetTypeInfo %s to fail on %s\n", ti2str(ti), id2str(v, id));
    if (ret && S_with_details)
    {
        printf("\t");
        dump_symbol(v, id);
    }
    return ret;
}

enum action
{
    ACT_EXIST,     /* tests only that TI_ request succeeds */
    ACT_EQUAL32,   /* success + request's value (32bit) equals to passed value */
    ACT_EQUAL64,   /* success + request's value (64bit) equals to passed value */
    ACT_DIFFER32,  /* success + request's value (32bit) differs from passed value */
    ACT_DIFFER64,  /* success + request's value (64bit) differs from passed value */
    ACT_ID_TAG,    /* success + request's value (tag = 32bit) is in tag-mask in passed value */
};

static BOOL is_cplusplus(const struct vacuum* v, DWORD id, DWORD tag);

#define success(a, b, c, d, e) _success(__LINE__, (a), (b), (c), (d), (e))
static BOOL _success(unsigned line, const struct vacuum* v, DWORD id, IMAGEHLP_SYMBOL_TYPE_INFO ti, enum action act, DWORD64 value)
{
    char buffer[sizeof(VARIANT)];
    BOOL ret = SymGetTypeInfo(v->dbg->process, v->base, id, ti, buffer);
    DWORD id2;

    if (act == ACT_EXIST) return ret;
    ok(ret, "Expecting SymGetTypeInfo %s to succeed on %s\n", ti2str(ti), id2str(v, id));
    if (ret)
    {
        switch (act)
        {
        case ACT_EXIST:
            break;
        case ACT_DIFFER64:
            ret = *(DWORD64*)buffer != value;
            ok_(__FILE__, line)(ret, "Expecting different value (but got %I64x) from %s on %s\n",
               *(DWORD64*)buffer, ti2str(ti), id2str(v, id));
            break;
        case ACT_DIFFER32:
            ret = *(DWORD*)buffer != (DWORD)value;
            ok_(__FILE__, line)(ret, "Expecting different value (but got %lu) from %s on %s\n", (DWORD)value, ti2str(ti), id2str(v, id));
            break;
        case ACT_EQUAL64:
            ret = *(DWORD64*)buffer == value;
            ok_(__FILE__, line)(ret, "Expecting value64 %I64x (but got %I64x) value, from %s on %s\n",
               value, *(DWORD64*)buffer, ti2str(ti), id2str(v, id));
            break;
        case ACT_EQUAL32:
            ret = *(DWORD*)buffer == (DWORD)value;
            ok_(__FILE__, line)(ret, "Expecting value32 %lu (but got %lu) value, from %s on %s\n",
               (DWORD)value, *(DWORD*)buffer, ti2str(ti), id2str(v, id));
            break;
        case ACT_ID_TAG:
            id2 = *(DWORD*)buffer;
            ret = SymGetTypeInfo(v->dbg->process, v->base, id2, TI_GET_SYMTAG, buffer);
            if (ret && !is_cplusplus(v, id2, *(DWORD*)buffer)) /* skip methods from C++ classes for now */
            {
                ret = ret && (value & tag2mask(*(DWORD*)buffer)) != 0;
                ok_(__FILE__, line)(ret, "Unexpected tag (%s) from %s\n", id2str(v, id2), ti2str(ti));
            }
            break;
        default:;
        }
    }
    if (!ret && S_with_details)
    {
        printf("\t");
        dump_symbol(v, id);
    }
    return ret;
}

static BOOL is_cplusplus(const struct vacuum* v, DWORD id, DWORD tag)
{
    switch (tag)
    {
    case SymTagFunction: return success(v, id, TI_GET_CLASSPARENTID, ACT_EXIST, 0);
    default: return FALSE;
    }
}

static tagmask_t coherence_fields(const struct vacuum *v)
{
    unsigned i;
    BOOL ret;
    enum SymTagEnum tag;
    enum DataKind datakind;
    tagmask_t all_tag_mask = 0;

    for (i = 0; i < v->num_ids; ++i)
    {
        ret = SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_SYMTAG, &tag);
        if (!ret)
        {
            if (S_with_details > 1)
            {
                /* it seems dbghelp stores more information, and returns it in enum & child info */
                printf("Got failing id for tag: ");
                dump_symbol(v, v->ids[i]);
            }
            continue;
        }
        all_tag_mask |= tag2mask(tag);

        switch (tag)
        {
        case SymTagExe:
            failure(v, v->ids[i], TI_GET_ADDRESS);
            failure(v, v->ids[i], TI_GET_LENGTH);
            failure(v, v->ids[i], TI_GET_LEXICALPARENT);
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagCompiland:
            failure(v, v->ids[i], TI_GET_ADDRESS);
            failure(v, v->ids[i], TI_GET_LENGTH);
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagData:
            success(v, v->ids[i], TI_GET_TYPE, ACT_DIFFER32, 0);
            success(v, v->ids[i], TI_GET_TYPEID, ACT_DIFFER32, 0);
            ret = SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_DATAKIND, &datakind);
            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
            if (ret)
            {
                DWORD sym_flags;
                tagmask_t tag_mask;

                switch (datakind)
                {
                case DataIsFileStatic:
                    if (get_symbol_flags(v->dbg, v->base, v->ids[i], &sym_flags) && (sym_flags & SYMFLAG_TLSREL))
                    {
                        failure(v, v->ids[i], TI_GET_ADDRESS);
                        tag_mask = tag2mask(SymTagExe) | tag2mask(SymTagCompiland);
                    }
                    else
                    {
                        success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
                        tag_mask = tag2mask(SymTagCompiland);
                    }
                    // failure(v, v->ids[i], TI_GET_LENGTH); FIXME native fails or succeeds depending on symbol, why??
                    failure(v, v->ids[i], TI_GET_OFFSET);
                    break;
                case DataIsStaticLocal:
                    if (get_symbol_flags(v->dbg, v->base, v->ids[i], &sym_flags) && (sym_flags & SYMFLAG_TLSREL))
                        failure(v, v->ids[i], TI_GET_ADDRESS);
                    else
                        success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
                    // failure(v, v->ids[i], TI_GET_LENGTH); FIXME native fails or succeeds depending on symbol, why??
                    failure(v, v->ids[i], TI_GET_OFFSET);
                    tag_mask = tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite);
                    break;
                case DataIsGlobal:
                    if (get_symbol_flags(v->dbg, v->base, v->ids[i], &sym_flags) && (sym_flags & SYMFLAG_TLSREL))
                        failure(v, v->ids[i], TI_GET_ADDRESS);
                    else
                        success(v, v->ids[i], TI_GET_ADDRESS, ACT_EXIST, 0); /* MSVC generates symbol __ImageBase at 0! */
                    // failure(v, v->ids[i], TI_GET_LENGTH); FIXME native fails or succeeds depending on symbol, why??
                    failure(v, v->ids[i], TI_GET_OFFSET);
                    tag_mask = tag2mask(SymTagExe) | tag2mask(SymTagCompiland);
                    break;
                case DataIsLocal:
                    failure(v, v->ids[i], TI_GET_ADDRESS);
                    // failure(v, v->ids[i], TI_GET_LENGTH); FIXME native fails or succeeds depending on symbol, why??
                    success(v, v->ids[i], TI_GET_OFFSET, ACT_EXIST, 0);
                    tag_mask = tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite);
                    break;
                case DataIsParam:
                    failure(v, v->ids[i], TI_GET_ADDRESS);
                    // failure(v, v->ids[i], TI_GET_LENGTH); FIXME native fails or succeeds depending on symbol, why??
                    success(v, v->ids[i], TI_GET_OFFSET, ACT_EXIST, 0);
                    tag_mask = tag2mask(SymTagFunction) | tag2mask(SymTagInlineSite);
                    break;
                case DataIsConstant:
                    failure(v, v->ids[i], TI_GET_ADDRESS);
                    failure(v, v->ids[i], TI_GET_OFFSET);
                    tag_mask = tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite) |
                        tag2mask(SymTagEnum) | tag2mask(SymTagCompiland) | tag2mask(SymTagExe);
                    break;
                    /* FIXME to be done */
                case DataIsMember:
                case DataIsObjectPtr: /* C++ */
                case DataIsStaticMember: /* C++ */
                    tag_mask = 0;
                    break;
                case DataIsUnknown:
                default:
                    tag_mask = 0;
                    ok(0, "Unlisted datakind %s\n", datakind2str(datakind));
                }
                if (tag_mask)
                    success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag_mask);
            }
            break;
        case SymTagFunction:
            if (!is_cplusplus(v, v->ids[i], tag)) /* skip methods from C++ classes for now */
            {
                success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
                success(v, v->ids[i], TI_GET_LENGTH, ACT_DIFFER64, 0);
                success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagCompiland));
            }
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagBlock:
            success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
            /* dwarf reports blocks of 0-byte size... so only test for existence, not != 0 */
            success(v, v->ids[i], TI_GET_LENGTH, ACT_EXIST /*ACT_DIFFER64*/, 0);
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite));
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagInlineSite:
            success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
            // seems to fail? success(v, v->ids[i], TI_GET_LENGTH, ACT_DIFFER64, 0);
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite));
            success(v, v->ids[i], TI_GET_TYPE, ACT_ID_TAG, tag2mask(SymTagFunctionType));
            success(v, v->ids[i], TI_GET_TYPEID, ACT_ID_TAG, tag2mask(SymTagFunctionType));
            failure(v, v->ids[i], TI_GET_OFFSET);
            // should success failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagInlinee:
            success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
            success(v, v->ids[i], TI_GET_LENGTH, ACT_DIFFER64, 0);
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagFunction) | tag2mask(SymTagBlock) | tag2mask(SymTagInlineSite));
            success(v, v->ids[i], TI_GET_TYPE, ACT_ID_TAG, tag2mask(SymTagFunctionType));
            success(v, v->ids[i], TI_GET_TYPEID, ACT_ID_TAG, tag2mask(SymTagFunctionType));
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagLabel:
            success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
            failure(v, v->ids[i], TI_GET_LENGTH);
            /* FIXME inlinesite is needed in builtin/dwarf... but not needed with native
             * not sure it's the right thing to do! (check why not needed on native: not seen or label attached to top level function?
             */
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagCompiland) | tag2mask(SymTagFunction));// | tag2mask(SymTagInlineSite));
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagPublicSymbol:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            failure(v, v->ids[i], TI_GET_CHILDRENCOUNT);
            break;
        case SymTagUDT:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagEnum:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_DIFFER32, 0);
            break;
        case SymTagFunctionType:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            failure(v, v->ids[i], TI_GET_LENGTH);
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagFunctionArgType:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            failure(v, v->ids[i], TI_GET_LENGTH);
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagPointerType:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            //failure(v, v->ids[i], TI_GET_LENGTH); /* ? */
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagArrayType:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
        case SymTagBaseType:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            failure(v, v->ids[i], TI_GET_ADDRESS);
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagTypedef:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe) | tag2mask(SymTagCompiland) | tag2mask(SymTagFunction) /* C++ */);
            // needs to be investigated, fails
            // success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
        case SymTagBaseClass:
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            /* FIXME success(v, v->ids[i], TI_GET_CHILDRENCOUNT, NONZERO32);
             * most of them are <> 0, but a couple are :-(
             */
            break;
        case SymTagFuncDebugStart:
        case SymTagFuncDebugEnd:
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            failure(v, v->ids[i], TI_GET_LENGTH);
            /* this isn't true.... lots of values seen
             * need to investigate what they are
             * success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
             */
            failure(v, v->ids[i], TI_GET_SYMNAME);
            break;
            /* unfortunately, not gotten from vacuum as of today */
#if 0
        case SymTagCustom:
            success(v, v->ids[i], TI_GET_ADDRESS, ACT_DIFFER64, 0);
            success(v, v->ids[i], TI_GET_LEXICALPARENT, ACT_ID_TAG, tag2mask(SymTagExe));
            success(v, v->ids[i], TI_GET_CHILDRENCOUNT, ACT_EQUAL32, 0);
            /* length is doc:ed as optional */
            failure(v, v->ids[i], TI_GET_TYPE);
            failure(v, v->ids[i], TI_GET_TYPEID);
            failure(v, v->ids[i], TI_GET_OFFSET);
            break;
#endif
        default:;
        }
    }
    return all_tag_mask;
}

static BOOL get_up(const struct vacuum* v, DWORD* id, tagmask_t mask)
{
    enum SymTagEnum tag;
    while (SymGetTypeInfo(v->dbg->process, v->base, *id, TI_GET_SYMTAG, &tag))
    {
        if ((mask & tag2mask(tag)) != 0) return TRUE; /* found */
        if (!SymGetTypeInfo(v->dbg->process, v->base, *id, TI_GET_LEXICALPARENT, id)) break;
    }
    return FALSE;
}

static void check_embedding(const struct vacuum *v, DWORD idfrom, DWORD idto)
{
    DWORD64 addrfrom, addrto;
    DWORD64 sizefrom, sizeto;
    enum SymTagEnum tagfrom, tagto;

    if (SymGetTypeInfo(v->dbg->process, v->base, idfrom, TI_GET_ADDRESS, &addrfrom) &&
        SymGetTypeInfo(v->dbg->process, v->base, idto,   TI_GET_ADDRESS, &addrto  ) &&
        SymGetTypeInfo(v->dbg->process, v->base, idto,   TI_GET_LENGTH,  &sizeto  ))
    {
        /* some symbols (Label, InlineSite) don't have length, so take that into account */
        if (!SymGetTypeInfo(v->dbg->process, v->base, idfrom, TI_GET_LENGTH,  &sizefrom)) sizefrom = 0;
        /* MSVC generates labels outside functions in C++
         * try to silence those
         * FIXME: the C++ test isn't working... strange indeed; so the workaround below
         * encompasses too many cases... narrow it down!
         */
        if (!(SymGetTypeInfo(v->dbg->process, v->base, idfrom, TI_GET_SYMTAG, &tagfrom) &&
              SymGetTypeInfo(v->dbg->process, v->base, idto,   TI_GET_SYMTAG, &tagto) &&
              tagfrom == SymTagLabel &&
              tagto   == SymTagFunction /*&&
                                          is_cplusplus(v, idto, tagto)*/))
        ok(addrto <= addrfrom && addrfrom + sizefrom <= addrto + sizeto,
           "Symbol %s[%s, %s] isn't embedded into symbol %s[%s, %s]\n",
           id2str(v, idfrom), addr2str(v->dbg, addrfrom), addr2str(v->dbg, addrfrom + sizefrom),
           id2str(v, idto),   addr2str(v->dbg, addrto),   addr2str(v->dbg, addrto + sizeto));
    }
}

static void coherence_embedding(const struct vacuum *v)
{
    DWORD tag, id;
    unsigned i;
    IMAGEHLP_MODULE64 im;

    im.SizeOfStruct = sizeof(im);
    ok(SymGetModuleInfo64(v->dbg->process, v->base, &im), "SymGetModuleInfo64 failed: %lu\n", GetLastError());
    /* we cannot assume that block A, which parent is B, is embedded inside B
     * we check that:
     * + every block and inline function is embedded in parent first function or inlined function
     * + every hierarchy point is embedded in upper block
     */
    for (i = 0; i < v->num_edges; ++i)
    {
        if (v->edges[i].what == TI_GET_LEXICALPARENT)
        {
            if (SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].from, TI_GET_SYMTAG, &tag))
            {
                id = v->edges[i].to;
                switch (tag)
                {
                case SymTagBlock:
                case SymTagFunction:
                    if (get_up(v, &id, tag2mask(SymTagFunction)))
                        check_embedding(v, v->edges[i].from, id);
                    break;
                case SymTagLabel:
                case SymTagFuncDebugStart:
                case SymTagFuncDebugEnd:
                    check_embedding(v, v->edges[i].from, id);
                    break;
                }
            }
            if (SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].to, TI_GET_SYMTAG, &tag))
            {
                DWORD64 addrfrom, sizefrom;
                /* check that all global symbols with address are within module's boundaries */
                if ((tag == SymTagExe || tag == SymTagCompiland) &&
                    SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].from, TI_GET_ADDRESS, &addrfrom) &&
                    SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].from, TI_GET_LENGTH,  &sizefrom))
                {
                    BOOL broken = addrfrom == 0;
                    ok((im.BaseOfImage <= addrfrom && addrfrom + sizefrom <= im.BaseOfImage + im.ImageSize) || broken,
                       "Symbol %s[%s, %s] isn't embedded into module %s[%s, %s]\n",
                       id2str(v, v->edges[i].from), addr2str(v->dbg, addrfrom),       addr2str(v->dbg, addrfrom + sizefrom),
                       id2str(v, v->edges[i].to),   addr2str(v->dbg, im.BaseOfImage), addr2str(v->dbg, im.BaseOfImage + im.ImageSize));
                }
            }
        }
    }
}

struct overlap_element
{
    DWORD64 low;
    DWORD id;
};

static int overlap_compar(const void* _p1, const void* _p2)
{
    const struct overlap_element* p1 = _p1;
    const struct overlap_element* p2 = _p2;

    if (p1->low < p2->low) return -1;
    if (p1->low > p2->low) return +1;
    return 0;
}

static void check_overlap(const struct vacuum *v, DWORD parent, tagmask_t tagmask)
{
    DWORD tag;
    DWORD64 length1, length2;
    unsigned i, num, num_alloc = 16;
    struct overlap_element* elts = malloc(num_alloc * sizeof(elts[0]));

    if (!elts) return;
    for (num = i = 0; i < v->num_edges; ++i)
    {
        /* gather all children that match tagmask */
        if (v->edges[i].from == parent &&
            v->edges[i].what == ~0 &&
            SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].to, TI_GET_SYMTAG, &tag) &&
            (tagmask & tag2mask(tag)))
        {
            if (num >= num_alloc)
            {
                struct overlap_element* new = realloc(elts, num_alloc * 2 * sizeof(elts[0]));
                if (!new)
                {
                    free(elts);
                    printf("OOM\n");
                    return;
                }
                elts = new;
                num_alloc *= 2;
            }
            if (SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].to, TI_GET_ADDRESS, &elts[num].low))
            {
                elts[num].id = v->edges[i].to;
                num++;
            }
        }
    }
    if (num)
    {
        qsort(elts, num, sizeof(elts[0]), overlap_compar);
        for (i = 0; i < num - 1; i++)
        {
            if (elts[i].id != elts[i + 1].id && /* just in case */
                SymGetTypeInfo(v->dbg->process, v->base, elts[i].id, TI_GET_LENGTH, &length1) &&
                SymGetTypeInfo(v->dbg->process, v->base, elts[i + 1].id, TI_GET_LENGTH, &length2))
                /* native (esp. with C++ templates) has duplicates in functions, so eliminate them */
                if (elts[i].low != elts[i + 1].low || length1 != length2)
                    ok(elts[i].low + length1 <= elts[i + 1].low,
                       "Overlap between %s[%I64x-%I64x] and %s[%I64x-%I64x], children of %s\n",
                       id2str(v, elts[i    ].id), elts[i    ].low, elts[i    ].low + length1,
                       id2str(v, elts[i + 1].id), elts[i + 1].low, elts[i + 1].low + length1,
                       id2str(v, parent));
        }
    }
    free(elts);
}

static void coherence_overlap(const struct vacuum *v)
{
    unsigned i;
    DWORD tag;

    /* Check that there's no overlap between children from:
     * - compiland: functions and global variables
     * - function/inlinesite/block: blocks inside functions
     */
    for (i = 0; i < v->num_ids; ++i)
    {
        if (SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_SYMTAG, &tag) &&
            /* FIXME this currently fails with dwarf */
            (tag == SymTagCompiland /*|| tag == SymTagBlock || tag == SymTagFunction || tag == SymTagInlineSite)*/))
            check_overlap(v, v->ids[i], tag2mask(SymTagFunction) | tag2mask(SymTagData) | tag2mask(SymTagBlock));
    }
}

static void coherence_bitfield(const struct vacuum *v)
{
    unsigned i;
    DWORD pos, tag, id, bt;
    BOOL ret;

    for (i = 0; i < v->num_ids; ++i)
    {
        if (!SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_BITPOSITION, &pos)) continue;
        ret = SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetInfo failed\n");
        if (!ret) continue;
        ok(tag == SymTagData, "bit position set on %s\n", tag2str(tag));
        /* check that lexical parent of v->ids[i] is a class/struct UDT */
        ret = SymGetTypeInfo(v->dbg->process, v->base, v->ids[i], TI_GET_TYPE, &id);
        ok(ret, "SymGetInfo failed\n");
        if (!ret) continue;
        /* FIXME it seems that in some case SymTagTypedef responds to BASETYPE requests, but not always
         * until the various cases are fully understood, force typedef resolution here.
         */
        id = untypedef(v->dbg, id);
        ret = SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetInfo failed\n");
        ok(tag == SymTagBaseType || tag == SymTagEnum, "unexpected underlying type for bitfield %s\n", tag2str(tag));
        ret = SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_BASETYPE, &bt);
        ok(ret, "SymGetInfo failed\n");
        ok(!ret || (bt == btInt || bt == btUInt || bt == btLong || bt == btULong ||
                    bt == btChar || bt == btWChar || bt == btChar8 || bt == btChar16 || bt == btChar32),
           "Unexpected underlying basetype for bitfield %s %s\n", basetype2str(bt), id2str(v, id));
    }
}

struct process_enum_data
{
    const struct debuggee* dbg;
    tagmask_t              all_tag_mask;
};

static BOOL CALLBACK process_enum_modules_cb(PCSTR name, DWORD64 base, PVOID user)
{
    struct process_enum_data *data = user;
    struct vacuum v;
    BOOL ret;

    winetest_push_context("module '%s'", name);
    ret = create_vacuum(data->dbg, (ULONG_PTR)base, &v);
    ok(ret, "Couldn't create vacuum on %s\n", name);
    if (ret)
    {
        data->all_tag_mask |= coherence_fields(&v);
        coherence_embedding(&v);
        coherence_overlap(&v);
        coherence_bitfield(&v);
        dispose_vacuum(&v);
    }
    winetest_pop_context();
    return TRUE;
}

START_TEST(coherence)
{
    BOOL ret;
    tagmask_t actual_mask;
    struct process_enum_data data = {.dbg = dbg, .all_tag_mask = 0};

    ret = SymEnumerateModules64(dbg->process, process_enum_modules_cb, &data);
    ok(ret, "SymEnumerateModules64 failed: %lu\n", GetLastError());
    /* FIXME must be kept in sync fix list in function above */
    actual_mask = tag2mask(SymTagExe) |
                  tag2mask(SymTagCompiland) |
                  tag2mask(SymTagData) |
                  tag2mask(SymTagFunction) |
                  tag2mask(SymTagBlock) |
                  tag2mask(SymTagInlineSite) |
                  tag2mask(SymTagLabel) |
                  tag2mask(SymTagFuncDebugStart) |
                  tag2mask(SymTagFuncDebugEnd) |
                  tag2mask(SymTagPublicSymbol) |
        /* tag2mask(SymTagCustom) | */
                  tag2mask(SymTagFunctionType) |
                  tag2mask(SymTagUDT) |
                  tag2mask(SymTagEnum) |
                  tag2mask(SymTagFunctionType) |
                  tag2mask(SymTagPointerType) |
                  tag2mask(SymTagArrayType) |
                  tag2mask(SymTagBaseType) |
                  tag2mask(SymTagTypedef) |
/* FIXME add proper C++ class testing tag2mask(SymTagBaseClass) | */
                  tag2mask(SymTagFunctionArgType);
    if ((actual_mask & data.all_tag_mask) != actual_mask)
    {
        enum SymTagEnum tag;
        tagmask_t todo_mask = 0;

        /* we don't expose epilog/prolog from dwarf yet */
        if (ok_running_wine && strstr(dbg->module_name, "-dwarf"))
            todo_mask |= tag2mask(SymTagFuncDebugStart) | tag2mask(SymTagFuncDebugEnd);
        /* latest msvc started not to emit SymTagBlock on x86_64 */
        if (strstr(dbg->module_name, "-msvc") && strstr(dbg->module_name, "-x86_64"))
            todo_mask = tag2mask(SymTagBlock);
        todo_for((actual_mask & (todo_mask | data.all_tag_mask)) == actual_mask)
        ok(FALSE, "Some tags not seen:");
        for (tag = 0; tag < SymTagMax; ++tag)
            if ((actual_mask & tag2mask(tag)) && !(data.all_tag_mask & tag2mask(tag)))
                printf(" %s", tag2str(tag));
        printf("\n");
    }
}
