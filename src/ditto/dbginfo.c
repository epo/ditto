/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Basic tests for debug information gotten from dbghelp.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define _FORCENAMELESSUNION
#include "test.h"
#include "ditto.h"
#include "../../build/mark.h"
#include <oaidl.h>

/* Helper functions */

static BOOL ends_with(const WCHAR* in, const WCHAR* with)
{
    SIZE_T inlen = wcslen(in);
    SIZE_T withlen = wcslen(with);

    if (inlen < withlen) return FALSE;
    return wcsnicmp(in + inlen - withlen, with, withlen) == 0;
}

static BOOL ends_with_file(const WCHAR* in, const WCHAR* with)
{
    SIZE_T inlen = wcslen(in);
    SIZE_T withlen = wcslen(with);
    const WCHAR* first;

    if (inlen < withlen) return FALSE;
    first = in + inlen - withlen;
    if (wcsnicmp(first, with, withlen) != 0) return FALSE;
    if (first == in) return TRUE;
    first--;
    return *first == '\\' || *first == '/';
}

static BOOL isfile(const char* str, const char* ext, const WCHAR* name)
{
    DWORD sz1 = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0) - 1;
    DWORD sz2 = MultiByteToWideChar(CP_ACP, 0, ext, -1, NULL, 0);
    WCHAR* nameW;
    BOOL ret = FALSE;

    if ((nameW = malloc((sz1 + sz2) * sizeof(WCHAR))) != NULL)
    {
        MultiByteToWideChar(CP_ACP, 0, str, -1, nameW, sz1);
        MultiByteToWideChar(CP_ACP, 0, ext, -1, nameW + sz1, sz2);
        ret = ends_with_file(name, nameW);
        free(nameW);
    }
    return ret;
}

static BOOL iscompiland(const WCHAR* compilandname, const char* base)
{
    /* object filenames differ (unix/windows), and some debug format return
     * the source filename instead of the object filename (FIXME).
     */
    return isfile(base, ".o", compilandname) ||
           isfile(base, ".obj", compilandname) ||
        (ok_running_wine && (isfile(base, ".c", compilandname) || isfile(base, ".cc", compilandname))); // FIXME better way?
}

static BOOL ismodulename(const char* dllname, const WCHAR* modname)
{
    size_t len = strlen(dllname);
    struct split_path sp;
    DWORD sz;
    WCHAR* wstr;
    int ret;

    if (len < 4) return FALSE;
    split_path(dllname, &sp);
    if (strcmp(sp.ext, ".dll")) return FALSE;
    sz = MultiByteToWideChar(CP_ACP, 0, sp.filename, sp.filename_len, NULL, 0);
    wstr = malloc(sz * sizeof(WCHAR));
    if (!wstr) return FALSE;
    MultiByteToWideChar(CP_ACP, 0, sp.filename, sp.filename_len, wstr, sz);
    ret = wcsnicmp(wstr, modname, sz);
    free(wstr);
    return ret == 0;
}

/* MS compiler attaches global symbols on '* Linker *' module
 * on first compilation, but then on SymTagExe on subsequent compilations.
 * Detect both cases.
 */
#define check_top_level(a, b) _check_top_level(__LINE__, (a), (b))
static void _check_top_level(unsigned line, const struct debuggee* dbg, DWORD id)
{
    DWORD tag;
    BOOL ret;
    WCHAR* name;

    ret = SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_SYMTAG, &tag);
    ok_(__FILE__, line)(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_SYMNAME, &name);
    ok_(__FILE__, line)(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    if (!ret) name = NULL;

    if (tag == SymTagExe)
        ok_(__FILE__, line)(ismodulename(dbg->dll_name, name), "Unexpected module %ls\n", name);
    else if (tag == SymTagCompiland)
        ok_(__FILE__, line)(!wcscmp(name, L"* Linker *"), "Unexpected compiland %ls\n", name);
    else
        ok_(__FILE__, line)(0, "Unexpected tag %s\n", tag2str(tag));
    HeapFree(GetProcessHeap(), 0, name);
}

/* we could get symbols by name from other modules... (we're using DllMain) */
static BOOL get_symbol_in_debuggee(const struct debuggee* dbg, const char* name, SYMBOL_INFO* si)
{
    const size_t sz = strlen(dbg->module_name) + 1 + strlen(name) + 1;
    char* fullname = malloc(sz);
    BOOL ret;

    snprintf(fullname, sz, "%s!%s", dbg->module_name, name);
    ret = SymFromName(dbg->process, fullname, si);
    free(fullname);
    return ret;
}

struct collect_symbols
{
    unsigned    count;
    tagmask_t   tagmask;
    DWORD*      ids;
    unsigned    max_ids;
};

static BOOL CALLBACK collect_symbols_cb(PSYMBOL_INFO sym, ULONG sz, void* user)
{
    struct collect_symbols* cs = user;

    UNREFERENCED_PARAMETER(sz);
    if (cs->tagmask & tag2mask(sym->Tag))
    {
        if (cs->ids && cs->count < cs->max_ids)
            cs->ids[cs->count] = sym->Index;
        cs->count++;
    }
    return TRUE;
}

static unsigned collect_symbols_by_name(const struct debuggee* dbg, DWORD64 base,
                                        const char* str, tagmask_t tagmask,
                                        DWORD* ids, unsigned max_ids)
{
    struct collect_symbols cs;

    cs.count = 0;
    cs.tagmask = tagmask;
    cs.ids = ids;
    cs.max_ids = max_ids;
    if (!SymEnumSymbols(dbg->process, base, str, collect_symbols_cb, &cs)) return ~0u;
    return cs.count;
}

static BOOL build_frame_context(const struct debuggee* dbg, const char* func, IMAGEHLP_STACK_FRAME* isf)
{
    SYMBOL_INFO si;

    si.SizeOfStruct = sizeof(si);
    si.MaxNameLen = 1;
    if (!get_symbol_in_debuggee(dbg, func, &si)) return FALSE;
    memset(isf, 0, sizeof(*isf));
    isf->InstructionOffset = si.Address;
    /* dummy values */
    isf->ReturnOffset = 2;
    isf->FrameOffset = 3;
    isf->StackOffset = 4;
    return TRUE;
}

/* Inspection */

static unsigned get_machine_ptr_word(unsigned machine)
{
    switch (machine)
    {
        case IMAGE_FILE_MACHINE_AMD64: return 8;
        case IMAGE_FILE_MACHINE_I386:  return 4;
        default: ok(FALSE, "unknown machine %x\n", machine);
    }
    return 0;
}

struct field_info_s
{
    const WCHAR* name;
    enum SymTagEnum tag; /* of type of field */
    ULONG offset; /* in structure */
    ULONG size; /* of field, underlying integral for bitfields */
    ULONG bits_pos; /* bitfield when != ~0UL, number of bits from offset */
    ULONG bits_length; /* bitfield when != ~0UL, number of bits for field */
    ULONG array_count; /* ~0UL when not an array, otherwise number of elements in array */
};

struct foobar_info_s
{
    unsigned machine;
    unsigned size;
    struct field_info_s fields[6];
};

/* The contents here must be kept in sync with struct foobar definition */
static struct foobar_info_s foobar_info_machine[] = {
{
    IMAGE_FILE_MACHINE_AMD64,
    112,
    {
        {L"ch",    SymTagBaseType,    0,  1, ~0UL, ~0UL, ~0UL},
        {L"array", SymTagArrayType,   8, 96, ~0UL, ~0UL,   12},
        {L"bf1",   SymTagBaseType,  104,  4,    0,    3, ~0UL},
        {L"bf2",   SymTagBaseType,  104,  4,    3,    7, ~0UL},
        {L"bf3",   SymTagBaseType,  104,  4,   10,    2, ~0UL},
        {L"u",     SymTagBaseType,  108,  4, ~0UL, ~0UL, ~0UL},
    },
},
{
    IMAGE_FILE_MACHINE_I386,
    60,
    {
        {L"ch",    SymTagBaseType,    0,  1, ~0UL, ~0UL, ~0UL},
        {L"array", SymTagArrayType,   4, 48, ~0UL, ~0UL,   12},
        {L"bf1",   SymTagBaseType,   52,  4,    0,    3, ~0UL},
        {L"bf2",   SymTagBaseType,   52,  4,    3,    7, ~0UL},
        {L"bf3",   SymTagBaseType,   52,  4,   10,    2, ~0UL},
        {L"u",     SymTagBaseType,   56,  4, ~0UL, ~0UL, ~0UL},
    },
},
};
static struct foobar_info_s* foobar_info;

static BOOL set_machine_info(const struct debuggee* dbg)
{
    unsigned i;
    for (i = 0; i < ARRAYSIZE(foobar_info_machine); ++i)
        if (foobar_info_machine[i].machine == dbg->machine)
        {
            foobar_info = &foobar_info_machine[i];
            return TRUE;
        }
    fprintf(stderr, "Unsupported machine %#lx\n", dbg->machine);
    return FALSE;
}

static struct
{
    const WCHAR* name;
    int value;
}
myenum_values[] =
{
    {L"FIRST_VALUE",   0},
    {L"SECOND_VALUE",  1},
    {L"THIRD_VALUE",  57},
    {L"FOURTH_VALUE", 58},
    {L"FIFTH_VALUE",  -1},
};

static void test_symname(const struct debuggee* dbg)
{
    char sibuf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* siA = (SYMBOL_INFO*)sibuf;
    SYMBOL_INFOW* siW = (SYMBOL_INFOW*)sibuf;
    BOOL ret;

    siA->SizeOfStruct = sizeof(*siA);
    siA->MaxNameLen = sizeof(sibuf) - sizeof(*siA);

    ret = get_symbol_in_debuggee(dbg, "function_AA", siA);
    ok(ret, "Couldn't get function_AA: %lu\n", GetLastError());
    ok(siA->NameLen == 11, "Wrong length %lu\n", siA->NameLen);
    ok(!strcmp(siA->Name, "function_AA"), "Wrong name %s %lu\n", siA->Name, siA->NameLen);

    siW->SizeOfStruct = sizeof(*siW);
    siW->MaxNameLen = 8;

    ret = SymFromNameW(dbg->process, L"function_AA", siW);
    ok(ret, "Couldn't get function_AA: %lu\n", GetLastError());
    ok(siW->NameLen == 11, "Wrong length %lu\n", siW->NameLen);
    ok(!lstrcmpW(siW->Name, L"functio"), "Wrong name %ls\n", siW->Name);
}

static void test_symbols_function_AA(const struct debuggee* dbg)
{
    /* we inspect (with dbghelp) all those local variables & function parameters
     * so check out the code below when changing those variables
     */
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    DWORD i, j, type, tag, bt, kind, parentid;
    DWORD function_AA_symid;
    ULONG64 len;
    WCHAR* name;
    TI_FINDCHILDREN_PARAMS* tfp;
    IMAGEHLP_STACK_FRAME isf;
    static struct
    {
        const char* name;
        const WCHAR* nameW;
        SIZE_T size;
        enum DataKind kind;
    }
    locpmts[] =
    {
        /* we assume we have same size on this exec and children process */
        {"base",       L"base",       sizeof(DWORD64),  DataIsParam},
        {"other",      L"other",      sizeof(DWORD),    DataIsLocal},
        {"static_int", L"static_int", sizeof(int),      DataIsStaticLocal},
    };

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = get_symbol_in_debuggee(dbg, "function_AA", si);
    ok(ret, "SymFromName failed: %lu\n", GetLastError());
    ok(si->NameLen == 11, "Wrong name len %lu\n", si->NameLen);
    ok(!strcmp(si->Name, "function_AA"), "Wrong name %s\n", si->Name);
    ok(si->Size, "Should have a size\n");
    ok(si->ModBase == dbg->base, "Wrong module base: expecting %I64x but got %I64x\n",
       dbg->base, si->ModBase);
    /* FIXME on W10, we get 0 in Flags... */
    ok(si->Tag == SymTagFunction, "Wrong tag %s\n", tag2str(si->Tag));
    function_AA_symid = si->Index;

    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_TYPEID, &type);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(type == si->TypeIndex, "wrong typeid %lu (expecting %lu)\n", type, si->TypeIndex);

    /* checking return type in signature */
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_TYPEID, &type);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(tag == SymTagBaseType, "wrong tag %s\n", tag2str(tag));
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
    ok(!ret, "SymGetTypeInfo should fail\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_BASETYPE, &bt);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(bt == btUInt, "Wrong base-type %lu\n", bt);

    /* checking return parameters's type in signature */
    tfp = retrieve_children(dbg, si->TypeIndex);
    if (tfp)
    {
        ok(tfp->Count == 2, "Wrong number of parameters' type\n");

        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[0], TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagFunctionArgType, "wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[0], TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagPointerType, "Wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagUDT, "wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        if (ret)
        {
            ok(!lstrcmpW(name, L"foobar"), "UDT name is %ls\n", name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[1], TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagFunctionArgType, "wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[1], TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        type = untypedef(dbg, type);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_BASETYPE, &bt);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(bt == btUInt, "Wrong base-type %s\n", basetype2str(bt));
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(len == sizeof(DWORD64), "Wrong length %I64x (expecting %Iu)\n",
           len, sizeof(DWORD64));

        free(tfp);
    }

    /* Checking children (params, local variables) */
    tfp = retrieve_children(dbg, si->Index);
    if (tfp)
    {
        BOOL labelfound = FALSE;

        /* 5 made of: 2 parameters, one local, one static local, one label */
        ok(tfp->Count >= 5, "Wrong number of parameters+local variables' type %lu\n", tfp->Count);

        /* there are other children than parameters / local variables, so need to lookup */
        for (j = 0; j < ARRAYSIZE(locpmts); ++j)
        {
            BOOL found = FALSE;
            winetest_push_context("%s (#%lu)", locpmts[j].name, j);
            for (i = 0; i < tfp->Count; ++i)
            {
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                switch (tag)
                {
                case SymTagData: /* the parameters and local variables */
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                    if (ret)
                    {
                        if (!wcscmp(name, locpmts[j].nameW))
                        {
                            found = TRUE;
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_TYPE, &type);
                            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                            ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
                            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                            ok(len == locpmts[j].size, "Local variable/parameter %ls's size is wrong %I64x (expecting %Ix)\n",
                               name, len, locpmts[j].size);
                            /* FIXME: how to test the returned value? Moreover, is it changing when local context is
                             * updated? More testing required here.
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_OFFSET, &len);
                            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                            */
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_DATAKIND, &kind);
                            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                            ok((enum DataKind)kind == locpmts[j].kind,
                               "Local variable/parameter %ls's kind is wrong %lu (expecting %u)\n", name, kind, locpmts[j].kind);
                            ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_LEXICALPARENT, &parentid);
                            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                            ok(parentid == function_AA_symid, "wrong lexical parent %lu\n", parentid);
                        }
                        HeapFree(GetProcessHeap(), 0, name);
                    }
                    break;
                case SymTagLabel: /* labels inside the function */
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                    if (ret)
                    {
                        labelfound |= ends_with(name, L"label"); /* CL.EXE prepends a '$' to the name of the label */
                        HeapFree(GetProcessHeap(), 0, name);
                    }
                    break;
                default:
                    break;
                }
            }
            winetest_pop_context();
            ok(found, "Couldn't find local variable/parameter %s\n", locpmts[j].name);
        }
        ok(labelfound, "Couldn't find the label 'label'\n");
        free(tfp);
    }
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_ADDRESS, &len);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
       "Wrong address %I64x wrt [%I64x, %I64x]\n", si->Address, dbg->base, dbg->base + dbg->size);
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &parentid);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(tag == SymTagCompiland, "Wrong tag %s\n", tag2str(tag));
    ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    if (ret)
    {
        ok(iscompiland(name, "debuggee"), "Wrong compiland name %ls\n", name);
        HeapFree(GetProcessHeap(), 0, name);
    }

    /* checking local & parameters with SymFromName() */
    ret = build_frame_context(dbg, "function_AA", &isf);
    ok(ret, "Couldn't build frame context\n");
    ret = SymSetContext(dbg->process, &isf, NULL);
    ok(ret, "SymSetContext failed: %lu\n", GetLastError());
    for (i = 0; i < ARRAYSIZE(locpmts); i++)
    {
        ret = SymFromName(dbg->process, locpmts[i].name, si);
        ok(ret, "SymFromName failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(len == locpmts[i].size, "Local variable/parameter %ls's size is wrong %I64x (expecting %Ix)\n",
           name, len, locpmts[i].size);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_DATAKIND, &kind);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok((enum DataKind)kind == locpmts[i].kind,
           "Local variable/parameter %ls's kind is wrong %lu (expecting %u)\n", name, kind, locpmts[i].kind);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &parentid);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(parentid == function_AA_symid, "wrong lexical parent %lu\n", parentid);
    }
    /* labels are not returned by SymFromName! */
    ret = SymFromName(dbg->process, "label", si);
    ok(!ret && GetLastError() == ERROR_MOD_NOT_FOUND, "SymFromName should have failed: %lu\n", GetLastError());
    ret = SymFromName(dbg->process, "_dont_exist__", si);
    ok(!ret && GetLastError() == ERROR_MOD_NOT_FOUND, "SymFromName should have failed: %lu\n", GetLastError());
}

static void test_symbols_foobar(const struct debuggee* dbg)
{
    /* we inspect (with dbghelp) all those local variables & function parameters
     * so check out the code below when changing those variables
     */
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    DWORD type, tag, kind;
    ULONG64 len;
    WCHAR* name;
    static struct
    {
        const char* name;
        enum DataKind datakind;
        const char* compiland;
    } once_symbols[] =
    {
        {"static_myfoobar",     DataIsFileStatic,  "debuggee"},
        {"global_myfoobar",     DataIsGlobal,      NULL},
    };
    unsigned i;

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    for (i = 0; i < ARRAYSIZE(once_symbols); ++i)
    {
        winetest_push_context("%s (#%u)", once_symbols[i].name, i);
        ret = get_symbol_in_debuggee(dbg, once_symbols[i].name, si);
        ok(ret, "SymFromName failed: %lu\n", GetLastError());
        ok(si->NameLen == strlen(once_symbols[i].name), "Wrong name len %lu\n", si->NameLen);
        ok(!strcmp(si->Name, once_symbols[i].name), "Wrong name %s\n", si->Name);
        ok(si->Size == foobar_info->size, "Wrong size %lu\n", si->Size);
        ok(si->ModBase == dbg->base, "Wrong module base: expecting %I64x but got %I64x\n",
           dbg->base, si->ModBase);
        /* FIXME on W10, we get 0 in Flags... */
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_DATAKIND, &kind);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok((enum DataKind)kind == once_symbols[i].datakind, "Wrong data-kind %s\n", datakind2str(kind));
        ok(si->Tag == SymTagData, "Wrong tag %s\n", tag2str(si->Tag));
        /* absolute location, inside module */
        ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
           "Wrong address %I64x wrt [%I64x, %I64x]\n", si->Address, dbg->base, dbg->base + dbg->size);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_ADDRESS, &len);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(dbg->base <= si->Address && si->Address < dbg->base + dbg->size,
           "Wrong address %I64x wrt [%I64x, %I64x]\n", si->Address, dbg->base, dbg->base + dbg->size);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(type == si->TypeIndex, "Wrong type\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(len == foobar_info->size, "Wrong size %I64x\n", len);
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        if (once_symbols[i].compiland)
        {
            ok(tag == SymTagCompiland, "Wrong tag %s\n", tag2str(tag));
            ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMNAME, &name);
            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
            if (ret)
            {
                ok(isfile(once_symbols[i].compiland, ".o", name) ||
                   isfile(once_symbols[i].compiland, ".obj", name) ||
                   (ok_running_wine && isfile(once_symbols[i].compiland, ".c", name)), // FIXME better way?
                   "Wrong compiland name %ls\n", name);
                HeapFree(GetProcessHeap(), 0, name);
            }
        }
        else
        {
            check_top_level(dbg, type);
        }
        winetest_pop_context();
    }
}

static void test_symbols_globalstatic(const struct debuggee* dbg)
{
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    DWORD tag, datakind, parentid;
    WCHAR* name;
    DWORD ids[2];
    unsigned i, j, num;
    unsigned filemask = 0;
    IMAGEHLP_STACK_FRAME isf;
    static struct
    {
        const char* function;
        const WCHAR* functionW;
        const char* variable;
        BOOL        present;
    }
    local_static[] =
    {
        {"function_AA",       L"function_AA",       "static_int",      TRUE},
        {"function_AA",       L"function_AA",       "static_func_int", FALSE},
        {"DllMain",           L"DllMain",           "static_int",      FALSE},
        {"DllMain",           L"DllMain",           "static_func_int", TRUE},
        {"stack_walk_thread", L"stack_walk_thread", "static_int",      FALSE},
        {"stack_walk_thread", L"stack_walk_thread", "static_func_int", FALSE},
    };

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    /* testing global variables */
    ret = get_symbol_in_debuggee(dbg, "myvar_CHAR", si);
    ok(ret, "SymFromName failed: %lu\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(tag == SymTagData, "Unexpected tag %s\n", tag2str(tag));
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_DATAKIND, &datakind);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(datakind == DataIsGlobal, "wrong storage for myvar_CHAR\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &parentid);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    check_top_level(dbg, parentid);

    num = collect_symbols_by_name(dbg, dbg->base, "myvar_CHAR",
                                  tag2mask(SymTagData), ids, ARRAYSIZE(ids));
    ok(num == 1, "Didn't expect %#x instances\n", num);

    /* Reset local context to a function that doesn't use static_myvar_int
     * MSVC/PDB can make a reference to it (it can store a ref to a global in a
     * register).
     */
    ret = build_frame_context(dbg, "DllMain", &isf);
    ok(ret, "SymFromName failed: %lu\n", GetLastError());
    ret = SymSetContext(dbg->process, &isf, NULL);
    ok(ret, "SymSetContext failed: %lu\n", GetLastError());

    /* testing file static variables */
    ret = get_symbol_in_debuggee(dbg, "static_myvar_int", si);
    ok(ret, "Should have found one of static_myvar_int\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(tag == SymTagData, "Unexpected tag %s\n", tag2str(tag));
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_DATAKIND, &datakind);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(datakind == DataIsFileStatic, "wrong storage for static_myvar_int\n");
    ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_LEXICALPARENT, &parentid);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    ok(tag == SymTagCompiland, "Wrong tag %s\n", tag2str(tag));
    ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    if (ret)
    {
        /* no way to tell (or ensure) which one of the two variables is returned */
        ok(iscompiland(name, "debuggee") || iscompiland(name, "debuggee_cc"), "Wrong compiland %ls\n", name);
        HeapFree(GetProcessHeap(), 0, name);
    }

    num = collect_symbols_by_name(dbg, 0, "static_myvar_int",
                                  tag2mask(SymTagData), ids, ARRAYSIZE(ids));
    ok(num == 0, "Didn't expect %#x instances\n", num);
    num = collect_symbols_by_name(dbg, dbg->base, "static_myvar_int",
                                  tag2mask(SymTagData), ids, ARRAYSIZE(ids));
    ok(num == 2, "Didn't expect %#x instances\n", num);
    for (i = 0; i < num; i++)
    {
        ret = SymGetTypeInfo(dbg->process, dbg->base, ids[i], TI_GET_DATAKIND, &datakind);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(datakind == DataIsFileStatic, "wrong storage for static_myvar_int\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, ids[i], TI_GET_LEXICALPARENT, &parentid);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagCompiland, "Wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        if (ret)
        {
            if (iscompiland(name, "debuggee")) filemask |= 1;
            if (iscompiland(name, "debuggee_cc")) filemask |= 2;
            HeapFree(GetProcessHeap(), 0, name);
        }
    }
    ok(filemask == 3, "Didn't see all instances (%x)\n", filemask);

    for (i = 0; i < ARRAYSIZE(local_static); ++i)
    {
        winetest_push_context("%s/%s", local_static[i].function, local_static[i].variable);

        ret = build_frame_context(dbg, local_static[i].function, &isf);
        ok(ret, "build_frame_context failed %lu\n", GetLastError());
        ret = SymSetContext(dbg->process, &isf, NULL);
        /* SymSetContext returns FALSE+GLE=ERROR_SUCCESS when setting again the same context,
         * which we do here...
         */
        ok(ret || GetLastError() == ERROR_SUCCESS, "SymSetContext failed: %lu\n", GetLastError());

        ret = SymFromName(dbg->process, local_static[i].variable, si);
        if (local_static[i].present)
        {
            ok(ret, "SymFromName failed: %lu\n", GetLastError());
        }
        else
        {
            ok(!ret, "SymFromName should have failed\n");
            ok(GetLastError() == ERROR_MOD_NOT_FOUND, "Unexpected last error %lu\n", GetLastError());
        }

        /* search in module should fail */
        num = collect_symbols_by_name(dbg, dbg->base, local_static[i].variable,
                                      tag2mask(SymTagData), ids, ARRAYSIZE(ids));
        ok(num == ~0u || num == 0, "Unexpected num %x\n", num);
        /* search in local context should succeed */
        num = collect_symbols_by_name(dbg, 0, local_static[i].variable,
                                      tag2mask(SymTagData), ids, ARRAYSIZE(ids));
        if (local_static[i].present)
        {
            /* sadly, native returns two instances here... one with address&addressoffset, and one without */
            ok(num >= 1, "Didn't expect these %x\n", num);
            for (j = 0; j < num; j++)
            {
                DWORD64 addr;
                ret = SymGetTypeInfo(dbg->process, dbg->base, ids[j], TI_GET_DATAKIND, &datakind);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok(datakind == DataIsStaticLocal, "Unexpected datakind %s\n", datakind2str(datakind));
                ret = SymGetTypeInfo(dbg->process, dbg->base, ids[j], TI_GET_LEXICALPARENT, &parentid);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok(tag == SymTagFunction, "Unexpected tag %s\n", tag2str(tag));
                ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                if (ret)
                {
                    ok(!wcscmp(name, local_static[i].functionW), "Unexpected parent %ls %s\n", name, local_static[i].function);
                    HeapFree(GetProcessHeap(), 0, name);
                }
                /* The symbol should be visible by address */
                if (SymGetTypeInfo(dbg->process, dbg->base, ids[j], TI_GET_ADDRESS, &addr))
                {
                    SYMBOL_INFO si2;

                    si2.SizeOfStruct = sizeof(si2);
                    si2.MaxNameLen = 200;
                    ret = SymFromAddr(dbg->process, addr, NULL, &si2);
                    ok(ret, "SymFromAddr failed: %lu\n", GetLastError());
                    ok(si2.Index == ids[j], "Mismatch in indexes\n");
                }
            }
        }
        else
        {
            ok(num == 0, "Didn't expect these %x\n", num);
        }

        winetest_pop_context();
    }
}

static void test_types(const struct debuggee* dbg)
{
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* si = (SYMBOL_INFO*)si_buf;
    BOOL ret;
    TI_FINDCHILDREN_PARAMS* tfp;
    static struct basic_type {const char* name; DWORD base_type32; unsigned size32; DWORD base_type64; unsigned size64;}
        basic_types[] = {
        /* yes the C basic types are not reported (by name) in debug information */
        { "int",                btNoType,        0,     btNoType,        0 },
        { "long int",           btNoType,        0,     btNoType,        0 },
        { "unsigned",           btNoType,        0,     btNoType,        0 },
        { "unsigned long",      btNoType,        0,     btNoType,        0 },
        { "long long",          btNoType,        0,     btNoType,        0 },
        { "unsigned long long", btNoType,        0,     btNoType,        0 },
        /* despite the existence of such types in CV internals:
         * - VARIANT, _Fcomplex and _Dcomplex are reported as UDT (even when builtin?) and not as their CV equivalent
         * - HRESULT is reported as a typedef to long
         */
        /* let's try some VC & SDK typedefs */
        { "CHAR",               btChar,          1,     btChar,          1 },
        { "SCHAR",              btChar,          1,     btChar,          1 }, /* to be retested in C++, answer shall be different */
        { "UCHAR",              btUInt,          1,     btUInt,          1 },
        { "WCHAR",              btWChar,         2,     btWChar,         2 }, /* that's funny, as WCHAR is a typedef to wchar_t */
        { "wchar_t",            btUInt,          2,     btUInt,          2 },
        { "INT",                btInt,           4,     btInt,           4 },
        { "UINT",               btUInt,          4,     btUInt,          4 },
        { "LONG",               btLong,          4,     btLong,          4 },
        { "ULONG",              btULong,         4,     btULong,         4 },
        { "DWORD",              btULong,         4,     btULong,         4 },
        { "LONG_PTR",           btLong,          4,     btInt,           8 },
        { "ULONG_PTR",          btULong,         4,     btUInt,          8 },
        { "DWORD_PTR",          btULong,         4,     btUInt,          8 },
        { "INT_PTR",            btInt,           4,     btInt,           8 },
        { "UINT_PTR",           btUInt,          4,     btUInt,          8 },
        { "LONG64",             btInt,           8,     btInt,           8 },
        { "DWORD64",            btUInt,          8,     btUInt,          8 },
        { "HRESULT",            btLong,          4,     btLong,          4 }, /* so btHresult isn't used!!! */
        { "BOOL",               btInt,           4,     btInt,           4 },
        { "BOOLEAN",            btUInt,          1,     btUInt,          1 },
        { "INT8",               btChar,          1,     btChar,          1 },
        { "INT16",              btInt,           2,     btInt,           2 },
        { "INT32",              btInt,           4,     btInt,           4 },
        { "INT64",              btInt,           8,     btInt,           8 },
        { "UINT8",              btUInt,          1,     btUInt,          1 },
        { "UINT16",             btUInt,          2,     btUInt,          2 },
        { "UINT32",             btUInt,          4,     btUInt,          4 },
        { "UINT64",             btUInt,          8,     btUInt,          8 },
        { "LONGLONG",           btInt,           8,     btInt,           8 },
        { "ULONGLONG",          btUInt,          8,     btUInt,          8 },
        { "mytype_LONGLONG",    btInt,           8,     btInt,           8 },
        { "mytype_ULONGLONG",   btUInt,          8,     btUInt,          8 },
        { "FLOAT",              btFloat,         4,     btFloat,         4 },
        { "DOUBLE",             btFloat,         8,     btFloat,         8 },
        { "_BOOL",              btBool,          1,     btBool,          1 },
        /* and from C++ */
        { "CXX_bool",           btBool,          1,     btBool,          1 },
        { "CXX_CHAR8",          btChar8,         1,     btChar8,         1 }, /* requires C++20 */
        { "CXX_CHAR16",         btChar16,        2,     btChar16,        2 },
        { "CXX_CHAR32",         btChar32,        4,     btChar32,        4 },
        /* unlike C, char, signed char and unsigned char are 3 different types */
        { "CXX_CHAR",           btChar,          1,     btChar,          1 },
        { "CXX_SCHAR",          btInt,           1,     btInt,           1 },
        { "CXX_UCHAR",          btUInt,          1,     btUInt,          1 },
        { "CXX_WCHAR",          btWChar,         2,     btWChar,         2 },
        { "CXX_wchar_t",        btWChar,         2,     btWChar,         2 }, /* here we don't get btUInt as in C */
    };
    unsigned i;

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    for (i = 0; i < ARRAYSIZE(basic_types); i++)
    {
        DWORD tag, bt;
        DWORD64 size;
        unsigned expbt, expsize;

        winetest_push_context("%s (%u)", basic_types[i].name, i);
        if (get_machine_ptr_word(dbg->machine) == 4)
        { expbt = basic_types[i].base_type32; expsize = basic_types[i].size32; }
        else
        { expbt = basic_types[i].base_type64; expsize = basic_types[i].size64; }
        ret = SymGetTypeFromName(dbg->process, dbg->base, basic_types[i].name, si);
        if (expbt == btNoType)
            ok(!ret, "SymGetTypeFromName shouldn't succeed: %lu\n", GetLastError());
        else if (!strcmp(basic_types[i].name, "CXX_CHAR8"))
        {
            /* it's broken on gcc and msvc latest versions... first version not fully stabilized */
            if (ok_running_wine) todo_wine ok(0, "Skipped CXX_CHAR8 test\n");
        }
        else
        {
            ok(ret, "SymGetTypeFromName %s failed: %lu\n", basic_types[i].name, GetLastError());
            si->TypeIndex = untypedef(dbg, si->TypeIndex);
            ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_SYMTAG, &tag);
            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
            ok(tag == SymTagBaseType, "Unexpected tag %s\n", tag2str(tag));
            ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_BASETYPE, &bt);
            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
            ok(bt == expbt, "Unexpected basetype %s (%s)\n", basetype2str(bt), basetype2str(expbt));
            ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &size);
            ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
            ok(size == expsize, "Unexpected size %I64u (expecting %u)\n", size, expsize);
        }
        winetest_pop_context();
    }

    ret = SymGetTypeFromName(dbg->process, dbg->base, "foobar", si);
    ok(ret, "SymGetTypeFromName failed: %lu\n", GetLastError());
    if (ret)
    {
        DWORD tag, info, type, parentid;
        ULONG64 len;
        WCHAR* name;

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_UDTKIND, &info);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(info == UdtStruct, "Unexpected udt kind %lu\n", info);

        ok(si->NameLen == 6, "Wrong name len but got %lu\n", si->NameLen);
        ok(!strcmp(si->Name, "foobar"), "Wrong name, expecting foobar but got %s\n", debugstr_a(si->Name));
        ok(si->Size == foobar_info->size, "Wrong size, got %lu\n", si->Size);
        ok(si->Index == si->TypeIndex, "Index and TypeIndex shoud be identical\n");
        ok(si->ModBase == dbg->base, "Wrong module base\n");
        ok(si->Tag == SymTagUDT, "Unexpected tag %s\n", tag2str(si->Tag));

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagUDT, "Unexpected tag %s\n", tag2str(tag));

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_CLASSPARENTID, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LEXICALPARENT, &parentid);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagExe, "Wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        if (ret)
        {
            ok(ismodulename(dbg->dll_name, name), "Unexpected name %ls %s\n", name, dbg->dll_name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        tfp = retrieve_children(dbg, si->TypeIndex);
        if (tfp)
        {
            ok(tfp->Count == ARRAYSIZE(foobar_info->fields), "Wrong number of fields in foobar %lu\n", tfp->Count);

            for (i = 0; i < ARRAYSIZE(foobar_info->fields); ++i)
            {
                winetest_push_context("%ls (%u)", foobar_info->fields[i].name, i);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                if (ret)
                {
                    ok(!lstrcmpW(name, foobar_info->fields[i].name), "Wrong field name got %ls (expecting %ls)\n",
                       name, foobar_info->fields[i].name);
                    ok(HeapSize(GetProcessHeap(), 0, name) != (SIZE_T)-1,
                       "Expecting name to be allocated on process heap\n");
                    HeapFree(GetProcessHeap(), 0, name);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_TYPE, &type);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok((enum SymTagEnum)tag == foobar_info->fields[i].tag, "Wrong tag %s (expecting %s)\n",
                   tag2str(tag), tag2str(foobar_info->fields[i].tag));
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok(len == foobar_info->fields[i].size, "Wrong length %I64x (expecting %lu)\n",
                   len, foobar_info->fields[i].size);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_OFFSET, &info);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok(info == foobar_info->fields[i].offset, "Wrong offset %lu (expecting %lu)\n",
                   info, foobar_info->fields[i].offset);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_BITPOSITION, &info);
                if (foobar_info->fields[i].bits_length == ~0UL)
                    ok(!ret, "SymGetTypeInfo shouldn't succeed\n");
                else
                {
                    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                    ok(info == foobar_info->fields[i].bits_pos, "Wrong bit position %lu (expecting %lu)\n",
                       info, foobar_info->fields[i].bits_pos);
                    ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_LENGTH, &len);
                    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                    ok(len == foobar_info->fields[i].bits_length, "Wrong bit length %I64x (expecting %lu)\n",
                       len, foobar_info->fields[i].bits_length);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_COUNT, &info);
                if (foobar_info->fields[i].array_count == ~0UL)
                    ok(!ret, "SymGetTypeInfo shouldn't succeed\n");
                else
                {
                    ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                    ok(info == foobar_info->fields[i].array_count,
                       "Wrong array count position %lu (expecting %lu)\n",
                       info, foobar_info->fields[i].array_count);
                }
                winetest_pop_context();
            }
            free(tfp);
        }
    }

    memset(si, 0, sizeof(*si));
    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;

    ret = SymGetTypeFromName(dbg->process, dbg->base, "myenum", si);
    ok(ret, "SymGetTypeFromName failed: %lu\n", GetLastError());
    if (ret)
    {
        DWORD tag, info, type, parentid;
        DWORD64 len, len2;
        WCHAR* name;
        VARIANT v;

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_UDTKIND, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");

        ok(si->NameLen == 6, "Wrong name len but got %lu\n", si->NameLen);
        ok(!strcmp(si->Name, "myenum"), "Wrong name, expecting foobar but got %s\n", debugstr_a(si->Name));
        ok(si->Index == si->TypeIndex, "Index and TypeIndex shoud be identical\n");
        ok(si->ModBase == dbg->base, "Wrong module base\n");
        ok(si->Tag == SymTagEnum, "Unexpected tag %s\n", tag2str(si->Tag));

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagEnum, "Unexpected tag %s\n", tag2str(tag));

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_CLASSPARENTID, &info);
        ok(!ret, "SymGetTypeInfo should fail\n");
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LEXICALPARENT, &parentid);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagExe, "Wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, parentid, TI_GET_SYMNAME, &name);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        if (ret)
        {
            ok(ismodulename(dbg->dll_name, name), "Unexpected name %ls\n", name);
            HeapFree(GetProcessHeap(), 0, name);
        }

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(tag == SymTagBaseType, "Unexpected tag %#lx\n", tag);
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_LENGTH, &len);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(len == 1 || len == 2 || len == 4, "Unexpected length %I64x\n", len);

        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_BASETYPE, &info);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(info == btInt, "Wrong tag %s\n", tag2str(tag));
        ret = SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &len2);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
        ok(len2 == len, "Length should match underlying type %I64x %I64x\n", len, len2);

        // FIXME should dig a bit more about actual integral type
        tfp = retrieve_children(dbg, si->TypeIndex);
        if (tfp)
        {
            ok(tfp->Count == ARRAYSIZE(myenum_values), "Wrong number of values in myenum %lu\n", tfp->Count);

            for (i = 0; i < ARRAYSIZE(myenum_values); ++i)
            {
                winetest_push_context("%ls (#%u)", myenum_values[i].name, i);
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_SYMNAME, &name);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                if (ret)
                {
                    ok(!lstrcmpW(name, myenum_values[i].name), "Wrong value name got %ls (expecting %ls)\n", name, myenum_values[i].name);
                    ok(HeapSize(GetProcessHeap(), 0, name) != (SIZE_T)-1, "Expecting name to be allocated on process heap\n");
                    HeapFree(GetProcessHeap(), 0, name);
                }
                ret = SymGetTypeInfo(dbg->process, dbg->base, tfp->ChildId[i], TI_GET_VALUE, &v);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                /* we have little control on the size of the underlying integral type for the enum
                 * so support various integral sizes
                 */
                switch (V_VT(&v))
                {
                case VT_I1: ret = V_I1(&v) == myenum_values[i].value; break;
                case VT_I2: ret = V_I2(&v) == myenum_values[i].value; break;
                case VT_I4: ret = V_I4(&v) == myenum_values[i].value; break;
                default: ok(FALSE, "unexpected variant type %u\n", V_VT(&v)); break;
                }
                winetest_pop_context();
            }
            free(tfp);
        }
    }
}

struct aggregate_module_all
{
    const char* modname;
    DWORD64 addr;
    unsigned cname;
    unsigned caddr;
};

static BOOL aggregate_module_all_cb(const char* name, const IMAGEHLP_MODULE64* im, void* usr)
{
    struct aggregate_module_all* ama = usr;

    if (!strcasecmp(name, ama->modname)) ama->cname++;
    if (im->BaseOfImage == ama->addr) ama->caddr++;
    return TRUE;
}

struct aggregate_modules
{
    const struct debuggee* dbg;
    BOOL (*cb)(const char* name, const IMAGEHLP_MODULE64*, void*);
    void* user;
};

static BOOL CALLBACK process_aggregate_modules_cb(PCSTR name, DWORD64 base, PVOID user)
{
    struct aggregate_modules* am = user;
    IMAGEHLP_MODULE64 im;
    BOOL ret;

    memset(&im, 0, sizeof(im));
    im.SizeOfStruct = sizeof(im);
    ret = SymGetModuleInfo64(am->dbg->process, base, &im);
    ok(ret, "Couldn't get module info\n");
    if (ret)
    {
        /* The name of module in IMAGEHLP_MODULE64 is truncated to 32 chars...
         * While internally dbghelp stores up to 64 characters, and can pass up to those 64 characters in callbacks
         * (sigh)
         * so don't rely on ModuleName field, and pass the full module name
         */
        ok(!strncasecmp(name, im.ModuleName, strlen(im.ModuleName)), "Wrong module name %s %s\n", name, im.ModuleName);
        ok(base == im.BaseOfImage, "Wrong module base address\n");
        (am->cb)(name, &im, am->user);
    }
    return TRUE;
}

static BOOL aggregate_modules(const struct debuggee* dbg, BOOL (*cb)(const char*, const IMAGEHLP_MODULE64*, void*),
                              void* user)
{
    struct aggregate_modules am;

    memset(&am, 0, sizeof(am));
    am.dbg = dbg;
    am.cb = cb;
    am.user = user;
    return SymEnumerateModules64(dbg->process, process_aggregate_modules_cb, &am);
}

struct module_info
{
    const struct debuggee* dbg;
    const char* modname;
    IMAGEHLP_MODULE64* modinfo;
    unsigned count;
};

static BOOL aggregate_module_info_cb(const char* name, const IMAGEHLP_MODULE64* im, void* usr)
{
    struct module_info* mi = usr;
    if (!strcasecmp(mi->modname, name))
    {
        if (!mi->count++) *mi->modinfo = *im;
        return FALSE;
    }
    return TRUE;
}

static BOOL get_module_info(const struct debuggee* dbg, const char* modname, IMAGEHLP_MODULE64* im)
{
    struct module_info mi;
    mi.dbg = dbg;
    mi.modname = modname;
    mi.modinfo = im;
    mi.count = 0;
    return aggregate_modules(dbg, aggregate_module_info_cb, &mi) && mi.count == 1;
}

struct count_symbols
{
    unsigned count;
    DWORD tag;
};

static BOOL CALLBACK count_symbols_cb(PSYMBOL_INFO sym, ULONG sz, void* user)
{
    struct count_symbols* cs = user;

    UNREFERENCED_PARAMETER(sz);
    if (!cs->tag || cs->tag == sym->Tag) cs->count++;
    return TRUE;
}

static unsigned count_symbols(const struct debuggee* dbg, const char* str, DWORD tag)
{
    struct count_symbols cs;

    memset(&cs, 0, sizeof(cs));
    cs.tag = tag;
    if (!SymEnumSymbols(dbg->process, 0, str, count_symbols_cb, &cs)) return ~0u;
    return cs.count;
}

static void test_modules(const struct debuggee* dbg)
{
    BOOL ret;
    DWORD64 base, altbase;
    IMAGEHLP_MODULE64 im;
    char tmp[256];

    /* try reloading dbg->dll_name with same parameters */
    base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, dbg->base, (DWORD)dbg->size, NULL, 0);
    ok(base == 0, "SymLoadModuleEx shouldn't have succeeded\n");
    ok(GetLastError() == ERROR_SUCCESS, "no success\n");

    /* try reloading dbg->dll_name changing the size */
    base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, dbg->base, (DWORD)dbg->size * 2, NULL, 0);
    ok(base == 0, "SymLoadModuleEx shouldn't have succeeded\n");
    ok(GetLastError() == ERROR_SUCCESS, "no success\n");
    memset(&im, 0, sizeof(im));
    im.SizeOfStruct = sizeof(im);
    ret = SymGetModuleInfo64(dbg->process, dbg->base, &im);
    ok(ret, "SymGetModuleInfo64 failed: %lu\n", GetLastError());
    ok(im.ImageSize == dbg->size, "Size shouldn't have changed\n");

    {
        struct aggregate_module_all ama = {dbg->module_name, dbg->base};
        ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
        ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
        ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
        ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
    }
    {
        struct aggregate_module_all ama = {dbg->module_name, dbg->base * 2};
        ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
        ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
        ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
        ok(ama.caddr == 0, "wrong count by address of modules %u\n", ama.caddr);
    }

    /* try reloading dbg->dll_name with forward overlap of same DLL...
     * module info is shifted to new location!
     */
    if (dbg->size)
    {
        SYMBOL_INFO_PACKAGE sip;
        DWORD64 destbase = dbg->base + dbg->size / 2;
        DWORD64 disp;

        sip.si.SizeOfStruct = sizeof(sip.si);
        sip.si.MaxNameLen = MAX_SYM_NAME;
        ret = SymFromName(dbg->process, "function_AA", &sip.si);
        ok(ret, "Couldn't get address for function_AA");

        base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, destbase, (DWORD)dbg->size, NULL, 0);
        ok(base == destbase, "SymLoadModuleEx has failed: %lu\n", GetLastError());
        memset(&im, 0, sizeof(im));
        im.SizeOfStruct = sizeof(im);
        ret = SymGetModuleInfo64(dbg->process, dbg->base, &im);
        ok(!ret, "SymGetModuleInfo64 should have failed\n");
        ret = SymGetModuleInfo64(dbg->process, destbase, &im);
        ok(ret, "SymGetModuleInfo64 shouldn't fail %lu\n", GetLastError());
        ok(im.BaseOfImage == destbase, "Base should have been updated\n");
        ok(im.ImageSize == dbg->size, "Size shouldn't have changed\n");

        {
            struct aggregate_module_all ama = {dbg->module_name, dbg->base};
            ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
            ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
            ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
            ok(ama.caddr == 0, "wrong count by address of modules %u\n", ama.caddr);
        }
        {
            struct aggregate_module_all ama = {dbg->module_name, destbase};
            ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
            ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
            ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
            ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
        }
        ret = SymFromAddr(dbg->process, sip.si.Address + destbase - dbg->base, &disp, &sip.si);
        ok(!strcmp(((PSYMBOL_INFO)&sip)->Name, "function_AA") && disp == 0, "Symbols should have been relocated\n");

        /* strangely enough, moving backwards isn't supported
         * so unload and load to get it back to where it was at first
         */
        ret = SymUnloadModule64(dbg->process, destbase);
        ok(ret, "Couldn't unload module\n");
        base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, dbg->base, (DWORD)dbg->size, NULL, 0);
        ok(base == dbg->base, "Should load module\n");
    }

    snprintf(tmp, sizeof(tmp), "%s!function_AA", dbg->module_name);
    ok(count_symbols(dbg, tmp, SymTagFunction) == 1, "mismatch on function_AA count\n");

    /* try reloading dbg->dll_name at non overlapping base */
    altbase = dbg->base * 2;
    base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, altbase, (DWORD)dbg->size * 3, NULL, 0);
    ok(base == altbase, "SymLoadModuleEx has failed: %lu\n", GetLastError());
    if (base)
    {
        memset(&im, 0, sizeof(im));
        im.SizeOfStruct = sizeof(im);
        ret = SymGetModuleInfo64(dbg->process, altbase, &im);
        ok(ret, "SymGetModuleInfo64 shouldn't fail %lu\n", GetLastError());
        ok(im.BaseOfImage == altbase, "Base should have been updated\n");
        ok(im.ImageSize == dbg->size * 3, "Size should have trippled\n");

        {
            struct aggregate_module_all ama = {dbg->module_name, dbg->base};
            ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
            ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
            ok(ama.cname == 2, "wrong count by name of modules %u\n", ama.cname);
            ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
        }
        {
            struct aggregate_module_all ama = {dbg->module_name, altbase};
            ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
            ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
            ok(ama.cname == 2, "wrong count by name of modules %u\n", ama.cname);
            ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
        }

        /* we should have two copies of function_AA now */
        snprintf(tmp, sizeof(tmp), "%s!function_AA", dbg->module_name);
        ok(count_symbols(dbg, tmp, SymTagFunction) == 2, "mismatch on function_AA count\n");

        ret = SymUnloadModule64(dbg->process, altbase);
        ok(ret, "SymUnloadModule64 has failed: %lu\n", GetLastError());
    }

    snprintf(tmp, sizeof(tmp), "%s!function_AA", dbg->module_name);
    ok(count_symbols(dbg, tmp, SymTagFunction) == 1, "mismatch on function_AA count\n");

    /* we should try to overlap another DLL (kernel32 for example) */
    ret = get_module_info(dbg, "kernel32", &im);
    ok(ret, "Couldn't get kernel32 info\n");
    altbase = im.BaseOfImage;
    base = SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, altbase, (DWORD)dbg->size * 3, NULL, 0);
    ok(base == 0, "SymLoadModuleEx should have failed: %lu\n", GetLastError());
}

static void test_virtual(const struct debuggee* dbg)
{
    BOOL ret;
    const DWORD64 hbase36L = 0x1234560000ULL;
    const DWORD64 hbase72L = 0x1234570000ULL;
    DWORD64 base;
    SYMBOL_INFO_PACKAGE sip;

    /* we add a module with 36 chars */
    base = SymLoadModuleEx(dbg->process, NULL, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.dll",
                           NULL, hbase36L, 0x40, NULL, SLMFLAG_VIRTUAL);
    ok(base == hbase36L, "SymLoadModuleEx failed: %lu\n", GetLastError());

    /* and another one with 72 chars */
    base = SymLoadModuleEx(dbg->process, NULL, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.dll",
                           NULL, hbase72L, 0x40, NULL, SLMFLAG_VIRTUAL);
    ok(base == hbase72L, "SymLoadModuleEx failed: %lu\n", GetLastError());

    {
        struct aggregate_module_all ama = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", hbase36L};
        ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
        ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
        ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
        ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
    }
    {
        struct aggregate_module_all ama = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", hbase72L};
        ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
        ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
        ok(ama.cname == 0, "wrong count by name of modules %u\n", ama.cname);
        ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
    }
    {
        struct aggregate_module_all ama = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0", hbase72L};
        ret = aggregate_modules(dbg, aggregate_module_all_cb, &ama);
        ok(ret, "couldn't aggregate modules %lu\n", GetLastError());
        ok(ama.cname == 1, "wrong count by name of modules %u\n", ama.cname);
        ok(ama.caddr == 1, "wrong count by address of modules %u\n", ama.caddr);
    }

    ret = SymAddSymbol(dbg->process, hbase36L, "justme36", hbase36L + 1, 3, 0);
    ok(ret, "SymAddSymbol failed: %lu\n", GetLastError());
    ret = SymAddSymbol(dbg->process, hbase36L, "justme36_2", hbase36L + 7, 11, 0);
    ok(ret, "SymAddSymbol failed: %lu\n", GetLastError());
    ret = SymAddSymbol(dbg->process, hbase72L, "justme72", hbase72L + 1, 7, 0);
    ok(ret, "SymAddSymbol failed: %lu\n", GetLastError());

    sip.si.SizeOfStruct = sizeof(sip.si);
    sip.si.MaxNameLen = MAX_SYM_NAME;
    /* no truncation on 36L module */
    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!justme36",
                      &sip.si);
    ok(ret, "SymFromNameA failed: %lu\n", GetLastError());
    ok(sip.si.Size == 3, "wrong size\n");
    ok(sip.si.ModBase == hbase36L, "wrong modbase\n");
    ok(sip.si.Address == hbase36L + 1, "wrong address\n");
    ok(sip.si.Tag == SymTagCustom, "wrong tag %ld\n", sip.si.Tag);
    ok(sip.si.Index == 0x80000000, "wrong index %lx\n", sip.si.Index);
    ok(sip.si.TypeIndex == 0, "wrong type index %lx\n", sip.si.TypeIndex);
    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!justme36_2",
                      &sip.si);
    ok(ret, "SymFromNameA failed: %lu\n", GetLastError());
    ok(sip.si.Size == 11, "wrong size\n");
    ok(sip.si.ModBase == hbase36L, "wrong modbase\n");
    ok(sip.si.Address == hbase36L + 7, "wrong address\n");
    ok(sip.si.Tag == SymTagCustom, "wrong tag %ld\n", sip.si.Tag);
    ok(sip.si.Index == 0x80000001, "wrong index %lx\n", sip.si.Index);
    ok(sip.si.TypeIndex == 0, "wrong type index %lx\n", sip.si.TypeIndex);

    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345678Z!justme36",
                      &sip.si);
    ok(!ret, "SymFromNameA should have failed: %lu\n", GetLastError());
    /* need truncation on 72L module */
    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!justme72",
                      &sip.si);
    ok(!ret, "SymFromNameA should have failed: %lu\n", GetLastError());
    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0!justme72",
                      &sip.si);
    ok(ret, "SymFromNameA failed: %lu\n", GetLastError());
    ok(sip.si.Size == 7, "wrong size\n");
    ok(sip.si.ModBase == hbase72L, "wrong modbase\n");
    ok(sip.si.Address == hbase72L + 1, "wrong address\n");
    ok(sip.si.Tag == SymTagCustom, "wrong tag %ld\n", sip.si.Tag);
    ok(sip.si.Index == 0x80000000, "wrong index %lx\n", sip.si.Index);
    ok(sip.si.TypeIndex == 0, "wrong type index %lx\n", sip.si.TypeIndex);
    ret = SymFromName(dbg->process,
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ012345zzzz!justme72",
                      &sip.si);
    ok(!ret, "SymFromNameA should have failed: %lu\n", GetLastError());

    ok(SymGetModuleBase64(dbg->process, hbase36L) == hbase36L, "SymGetModuleBase64 failed\n");
    ok(SymGetModuleBase64(dbg->process, hbase36L + 19) == hbase36L, "SymGetModuleBase64 failed\n");

    ret = SymUnloadModule64(dbg->process, hbase36L);
    ok(ret, "SymUnloadModule failed: %lu\n", GetLastError());
    ret = SymUnloadModule64(dbg->process, hbase72L);
    ok(ret, "SymUnloadModule failed: %lu\n", GetLastError());
}

static void test_sourcefiles(const struct debuggee* dbg)
{
    BOOL ret;
    char sibuf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* siA = (SYMBOL_INFO*)sibuf;
    IMAGEHLP_LINEW64 line;
    DWORD disp;

    siA->SizeOfStruct = sizeof(*siA);
    siA->MaxNameLen = sizeof(sibuf) - sizeof(*siA);

    ret = SymFromName(dbg->process, "function_AA", siA);
    ok(ret, "Couldn't get function_AA: %lu\n", GetLastError());

    line.SizeOfStruct = sizeof(line);
    ret = SymGetLineFromAddrW64(dbg->process, siA->Address, &disp, &line);
    ok(ret, "SymGetLineFromAddrW64 failed: %lu\n", GetLastError());
    if (ret)
    {
        ok(line.Address == siA->Address, "Wrong address of line\n");
        ok(line.FileName && ends_with_file(line.FileName, L"debuggee.c"), "Wrong file name %ls\n", line.FileName);
        ok(line.LineNumber == LINENO(MARK_function_AA) + 1, "Wrong line number: %lu\n", line.LineNumber);
        ok(disp == 0, "Wrong displacement %lu\n", disp);
    }
#if 0
    /* line next/prev behavior on native should be better explored */
    /* since first line of func, should always fail */
    ret = SymGetLinePrevW64(dbg->process, &line);
    ok(!ret, "SymGetLinePrevW64 should have failed\n");
    ok(GetLastError() == ERROR_NOT_FOUND, "wrong error code\n");

    /* LineNext doesn't always succeed on Windows, so not tests for now */
#endif
}

static void test_setcontext(const struct debuggee* dbg)
{
    BOOL ret;
    IMAGEHLP_STACK_FRAME isf;

    ret = build_frame_context(dbg, "DllMain", &isf);
    ok(ret, "SymFromName failed: %lu\n", GetLastError());
    ret = SymSetContext(dbg->process, &isf, NULL);
    ok(ret, "SymSetContext failed: %lu\n", GetLastError());
    ret = SymSetContext(dbg->process, &isf, NULL);
    ok(!ret, "SymSetContext should have failed\n");
    ok(GetLastError() == ERROR_SUCCESS, "expecting ERROR_SUCCESS but got %lu\n", GetLastError());
    isf.InstructionOffset = dbg->base;
    ret = SymSetContext(dbg->process, &isf, NULL);
    ok(ret, "SymSetContext failed: %lu\n", GetLastError());
    ret = SymSetScopeFromAddr(dbg->process, dbg->base);
    ok(ret, "SymSetScopeFromAddr failed: %lu\n", GetLastError());
}

union dbg_context
{
    CONTEXT current;
    WOW64_CONTEXT wow64;
};

static BOOL fetch_frame(const struct debuggee* dbg, union dbg_context* ctx, STACKFRAME64* sf)
{
    BOOL ret;

#if defined(__i386__) || defined(_M_IX86)
    ctx->current.ContextFlags = CONTEXT_CONTROL;
    ret = GetThreadContext(dbg->thread, &ctx->current);
    ok(ret, "got error %u\n", ret);

    sf->AddrPC.Mode = AddrModeFlat;
    sf->AddrPC.Segment = (WORD)ctx->current.SegCs;
    sf->AddrPC.Offset = ctx->current.Eip;
    sf->AddrFrame.Mode = AddrModeFlat;
    sf->AddrFrame.Segment = (WORD)ctx->current.SegSs;
    sf->AddrFrame.Offset = ctx->current.Ebp;
    sf->AddrStack.Mode = AddrModeFlat;
    sf->AddrStack.Segment = (WORD)ctx->current.SegSs;
    sf->AddrStack.Offset = ctx->current.Esp;
#elif defined(__x86_64__) || defined(_M_AMD64)
    if (dbg->machine == IMAGE_FILE_MACHINE_I386)
    {
        /* it's a WoW64 subprocess */
        ctx->wow64.ContextFlags = CONTEXT_CONTROL;
        ret = Wow64GetThreadContext(dbg->thread, &ctx->wow64);
        ok(ret, "got error %u\n", ret);
        sf->AddrPC.Mode = AddrModeFlat;
        sf->AddrPC.Segment = (WORD)ctx->wow64.SegCs;
        sf->AddrPC.Offset = ctx->wow64.Eip;
        sf->AddrFrame.Mode = AddrModeFlat;
        sf->AddrFrame.Segment = (WORD)ctx->wow64.SegSs;
        sf->AddrFrame.Offset = ctx->wow64.Ebp;
        sf->AddrStack.Mode = AddrModeFlat;
        sf->AddrStack.Segment = (WORD)ctx->wow64.SegSs;
        sf->AddrStack.Offset = ctx->wow64.Esp;
    }
    else
    {
        ctx->current.ContextFlags = CONTEXT_CONTROL;
        ret = GetThreadContext(dbg->thread, &ctx->current);
        ok(ret, "got error %u\n", ret);

        sf->AddrPC.Mode = AddrModeFlat;
        sf->AddrPC.Segment = ctx->current.SegCs;
        sf->AddrPC.Offset = ctx->current.Rip;
        sf->AddrFrame.Mode = AddrModeFlat;
        sf->AddrFrame.Segment = ctx->current.SegSs;
        sf->AddrFrame.Offset = ctx->current.Rbp;
        sf->AddrStack.Mode = AddrModeFlat;
        sf->AddrStack.Segment = ctx->current.SegSs;
        sf->AddrStack.Offset = ctx->current.Rsp;
    }
#else
    ret = FALSE;
    ok(ret, "Unsupported CPU\n");
#endif
    return ret;
}

struct frame_symbols
{
    const char* name;
    DWORD expected_setflags;
};

struct frame_symbols_user
{
    unsigned num_fsyms;
    const struct frame_symbols* fsyms;
    DWORD found;
    DWORD count;
};

static BOOL WINAPI frame_symbols_cb(SYMBOL_INFO* sym, ULONG sz, void* user)
{
    struct frame_symbols_user* fsu = user;
    unsigned i;

    UNREFERENCED_PARAMETER(sz);
    // printf("\t\tGot %s/%u flags=%#x reg=%u addr=%I64x\n",
    // sym->Name, sz, sym->Flags, sym->Register, sym->Address);
    fsu->count++;
    for (i = 0; i < fsu->num_fsyms; ++i)
    {
        if (!strcmp(sym->Name, fsu->fsyms[i].name))
        {
            fsu->found |= 1u << i;
            ok((sym->Flags & fsu->fsyms[i].expected_setflags) == fsu->fsyms[i].expected_setflags,
               "Wrong flags: expecting %#lx but got %#lx\n",
               fsu->fsyms[i].expected_setflags, sym->Flags);
        }
    }
    return TRUE;
}

static void check_frame_symbols(const struct debuggee* dbg, const struct frame_symbols* fsyms, unsigned num_fsyms)
{
    struct frame_symbols_user fsu;
    BOOL ret;

    fsu.num_fsyms = num_fsyms;
    fsu.fsyms = fsyms;
    fsu.found = 0;
    fsu.count = 0;
    ret = SymEnumSymbols(dbg->process, 0 /* local */, NULL, frame_symbols_cb, &fsu);
    ok(ret, "SymEnumSymbols failed: %lu\n", GetLastError());
    ok(fsu.found + 1 == 1u << num_fsyms,
       "Didn't find all local symbols: %#lx for %u\n", fsu.found, num_fsyms);
    ok(fsu.count == num_fsyms,
       "Bad count for local symbols: %lu for %u\n", fsu.count, num_fsyms);
}

static struct frame_symbols swt_symbols[] =
{
    {"arg", SYMFLAG_LOCAL | SYMFLAG_PARAMETER},
};

static void test_stack_walk(const struct debuggee* dbg)
{
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO *si = (SYMBOL_INFO *)si_buf;
    STACKFRAME64 frame = {{0}}, frame0;
    unsigned nframe = 0, our_frame = ~0u;
    DWORD64 disp;
    union dbg_context ctx;
    BOOL ret;

    if (!fetch_frame(dbg, &ctx, &frame)) return;
    frame0 = frame;

    do
    {
        ret = StackWalk64(dbg->machine, dbg->process, dbg->thread, &frame, &ctx, NULL,
                          SymFunctionTableAccess64, SymGetModuleBase64, NULL);
        ok(ret, "StackWalk64() failed: %lu\n", GetLastError());
        if (nframe == 0)
        {
            /* first invocation just calculates the return address */
            ok(frame.AddrPC.Offset == frame0.AddrPC.Offset, "expected %#I64x, got %#I64x\n",
               frame0.AddrPC.Offset, frame.AddrPC.Offset);
            ok(frame.AddrStack.Offset == frame0.AddrStack.Offset, "expected %#I64x, got %#I64x\n",
               frame0.AddrStack.Offset, frame.AddrStack.Offset);
            ok(frame.AddrReturn.Offset && frame.AddrReturn.Offset != frame.AddrPC.Offset,
               "got bad return address %#I64x\n", frame.AddrReturn.Offset);
        }
        si->SizeOfStruct = sizeof(SYMBOL_INFO);
        si->MaxNameLen = 200;
        if (SymFromAddr(dbg->process, frame.AddrPC.Offset, &disp, si) && !strcmp(si->Name, "stack_walk_thread"))
        {
            our_frame = nframe;
            SymSetScopeFromAddr(dbg->process, frame.AddrPC.Offset);
            check_frame_symbols(dbg, swt_symbols, ARRAYSIZE(swt_symbols));
        }
        nframe++;
    } while (frame.AddrReturn.Offset);

    ret = StackWalk64(dbg->machine, dbg->process, dbg->thread, &frame, &ctx, NULL,
        SymFunctionTableAccess64, SymGetModuleBase64, NULL);
    ok(!ret, "StackWalk64() should have failed\n");

    /* depending on OS version, we have optionnaly {Nt|Zw}SuspendThread(), and Suspendthread() */
    ok(our_frame >= 1 && our_frame <= 2, "didn't find stack_walk_thread frame %u\n", our_frame);
}

static struct frame_symbols verify3_symbols[] =
{
    {"ht", SYMFLAG_LOCAL | SYMFLAG_PARAMETER},
    {"c3", SYMFLAG_LOCAL | SYMFLAG_PARAMETER},
};

static struct inline_details
{
    const char* symbol;
    BOOL inlined;
    unsigned mark_low;
    unsigned mark_high;
    unsigned mark_todo;
    const struct frame_symbols* fsyms;
    unsigned num_fsyms;
}
inline_details[] =
{
    {"stack_walk_thread", FALSE, MARK_BEG_stack_walk_thread, MARK_END_stack_walk_thread, ~0u,          swt_symbols,     ARRAYSIZE(swt_symbols)},
    {"verify3",           TRUE,  MARK_verify3,               MARK_verify3,               MARK_verify1, verify3_symbols, ARRAYSIZE(verify3_symbols)},
    {"verify2",           TRUE,  MARK_verify2,               MARK_verify2,               ~0u,          NULL,            0},
    {"verify1",           TRUE,  MARK_verify1,               MARK_verify1,               ~0u,          NULL,            0},
};

#if defined(INLINE_FRAME_CONTEXT_INIT)//FIXME to be removed
static DWORD handle_inline_frame(const struct debuggee* dbg, STACKFRAME_EX* frame)
{
    DWORD64 addr = frame->AddrPC.Offset;
    char si_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO *si = (SYMBOL_INFO *)si_buf;
    char si_buf2[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO *si2 = (SYMBOL_INFO *)si_buf2;
    DWORD nframe = ARRAYSIZE(inline_details); /* set default value when no frame found */
    enum SymTagEnum tag;
    DWORD64 disp64;
    BOOL ret;
    unsigned i;
    DWORD disp32;
    IMAGEHLP_LINEW64 line;

    si->SizeOfStruct = sizeof(SYMBOL_INFO);
    si->MaxNameLen = 200;
    /* nothing garantees that we have a known symbol for any unwinded frame */
    if (!SymFromInlineContext(dbg->process, addr, frame->InlineFrameContext, &disp64, si))
        return nframe;

    // printf("\tGot %s inlinectx %x %I64x\n", si->Name, frame->InlineFrameContext, si->Address);

    line.SizeOfStruct = sizeof(line);

    /* sigh FIXME: native sets the Size field at 0... cannot test upper bound */
    ok(si->Address <= addr /*&& addr < si->Address + si->Size*/,
       "PC isn't inside function %#I64x [%I64x, %lx]\n", addr, si->Address, si->Size);

    for (i = 0; i < ARRAYSIZE(inline_details); ++i)
    {
        winetest_push_context("%s (#%u)", inline_details[i].symbol, i);
        if (!strcmp(si->Name, inline_details[i].symbol))
        {
            nframe = i;
            ret = SymGetLineFromInlineContextW(dbg->process, addr, frame->InlineFrameContext, 0, &disp32, &line);
            ok(ret, "SymGetLineFromInlineContextW failed: %lu\n", GetLastError());
            if (ret)
            {
                ok(ends_with_file(line.FileName, L"debuggee.c"), "Wrong filename %ls\n", line.FileName);
                todo_wine_if(inline_details[i].mark_todo != ~0u && line.LineNumber == LINENO(inline_details[i].mark_todo))
                ok(line.LineNumber >= LINENO(inline_details[i].mark_low) && line.LineNumber <= LINENO(inline_details[i].mark_high),
                   "Wrong line number %lu [%u, %u]\n", line.LineNumber,
                   LINENO(inline_details[i].mark_low), LINENO(inline_details[i].mark_high));
            }
            if (inline_details[i].inlined)
            {
                ret = SymGetTypeInfo(dbg->process, dbg->base, si->Index, TI_GET_SYMTAG, &tag);
                ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
                ok(tag == SymTagInlineSite, "Unexpected tag %#x\n", tag);

                si2->SizeOfStruct = sizeof(*si2);
                si2->MaxNameLen = sizeof(si_buf2) - sizeof(*si2);
                /* si2 contains non inlined version of inlined function */
                ret = SymFromName(dbg->process, si->Name, si2);
                /* FIXME some compilers (like gcc) don't generate the non inlined function! */
                if (ret)
                {
                    /* si and si2 shouldn't overlap */
                    ok(si->Address < si2->Address || si2->Address + si2->Size >= si->Address,
                       "expected inlined function isn't actually inlined\n");
                }
                /* si2 contains function containing inlined function */
                ret = SymFromAddr(dbg->process, addr, &disp64, si2);
                ok(ret, "SymFromAddr failed: %lu\n", GetLastError());
                ok(strcmp(si->Name, si2->Name),
                   "expected inlined function isn't actually inlined %s %s\n", si->Name, si2->Name);
                /* si should be inside si2 */
                ok(si2->Address <= si->Address && si2->Address + si2->Size >= si->Address + si->Size,
                   "inlined function isn't inside container [%I64x,%lx] [%I64x,%lx]\n",
                   si2->Address, si2->Size, si->Address, si->Size);
            }
            if (inline_details[i].num_fsyms)
            {
                ret = SymSetScopeFromInlineContext(dbg->process, addr, frame->InlineFrameContext);
                ok(ret, "SymSetScopeFromInlineContext failed: %lu\n", GetLastError());
                check_frame_symbols(dbg, inline_details[i].fsyms, inline_details[i].num_fsyms);
            }
        }
        winetest_pop_context();
    }

    return nframe;
}
#endif

static void test_stack_walk_ex(const struct debuggee* dbg)
{
#if defined(INLINE_FRAME_CONTEXT_INIT)//FIXME to be removed
    STACKFRAME_EX frame = {{0}}, frame0;
    DWORD nframe = 0;
    unsigned found_frames = 0;
    BOOL ret;
    union dbg_context ctx;
    DWORD frame_mask = (1L << ARRAYSIZE(inline_details)) - 1;
    /* all fields in STACKFRAME64 are present in STACKFRAME_EX at same offset */
    if (!fetch_frame(dbg, &ctx, (STACKFRAME64*)&frame)) return;
    frame.StackFrameSize = sizeof(frame);
    frame.InlineFrameContext = INLINE_FRAME_CONTEXT_INIT;

    for (;;)
    {
        frame0 = frame;
        ret = StackWalkEx(dbg->machine, dbg->process, dbg->thread, &frame, &ctx, NULL,
                          SymFunctionTableAccess64, SymGetModuleBase64, NULL, SYM_STKWALK_DEFAULT);
        if (!ret)
        {
            ok(nframe != 0, "StackWalkEx() failed: %lu\n", GetLastError()); /* cannot fail on first frame */
            break;
        }
        if (nframe == 0)
        {
            /* first invocation just calculates the return address */
            ok(frame.AddrPC.Offset == frame0.AddrPC.Offset, "expected %#I64x, got %#I64x\n",
               frame0.AddrPC.Offset, frame.AddrPC.Offset);
            ok(frame.AddrStack.Offset == frame0.AddrStack.Offset, "expected %#I64x, got %#I64x\n",
               frame0.AddrStack.Offset, frame.AddrStack.Offset);
            ok(frame.AddrReturn.Offset && frame.AddrReturn.Offset != frame.AddrPC.Offset,
               "got bad return address %#I64x\n", frame.AddrReturn.Offset);
        }
        // printf("Frame %#I64x %#I64x %#I64x\n",
        //       frame.AddrPC.Offset, frame.AddrStack.Offset, frame.AddrReturn.Offset);
        found_frames |= 1 << handle_inline_frame(dbg, &frame);
        nframe++;
    }
    ok(!frame0.AddrReturn.Offset, "pref addr return must be 0\n");

    ret = StackWalkEx(dbg->machine, dbg->process, dbg->thread, &frame, &ctx, NULL,
                      SymFunctionTableAccess64, SymGetModuleBase64, NULL, SYM_STKWALK_DEFAULT);
    ok(!ret, "StackWalkEx() should have failed\n");

    /* Be happy when one frame is present. Getting all frames needs more compiler tweaking (FIXME) */
    ok(found_frames & frame_mask, "didn't find all expected frames %lx\n", found_frames & frame_mask);
#endif
}

START_TEST(debuggee)
{
    if (!set_machine_info(dbg)) return;
    /* skip those tests... until a way is found for the frame to be present with msvc /O2 */
    if (!strstr(dbg->dll_name, "-msvc-pdb-O2."))
    {
        test_stack_walk(dbg);
        test_stack_walk_ex(dbg);
    }
    test_setcontext(dbg);
    test_symname(dbg);
    test_symbols_function_AA(dbg);
    test_symbols_foobar(dbg);
    test_symbols_globalstatic(dbg);
    test_types(dbg);
    test_modules(dbg);
    test_virtual(dbg);
    test_sourcefiles(dbg);
}
