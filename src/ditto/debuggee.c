/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Entry point for managing command line and driving internals.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <windows.h>

#include "debuggee_cc.hh"

static int static_myvar_int; /* also defined in debuggee.cc */

unsigned char icompute_uc(unsigned char c)
{
    return (c >> 4) | ((c & 0xf) << 4) | 0x18;
}

DWORD icompute_dword(DWORD c)
{
    return ((c >> 4) | ((c & 0xf) << 4)) ^ ~0x18811881;
}

/* use function pointer to avoid compiler optimization */
unsigned char (*compute_uc)(unsigned char);
DWORD (*compute_dword)(DWORD);
/* There are used with some kind of black magic in order the
 * compiler is does too many optimizations:
 * - avoid interline optimizations in expression computation,
 * - avoid (some) call-tail optimizations
 */

INLINE DWORD verify1(HANDLE ht, unsigned char c1)
{
    if (ht == (HANDLE)1) goto label;
    if (compute_dword(c1) == 0) ht = (HANDLE)4321;
    return compute_dword(SuspendThread(ht)); // MARK(verify1)
label:
    return 15;
}

INLINE DWORD verify2(HANDLE ht, unsigned char c2)
{
    if (ht == (HANDLE)2) return 16;
    c2 = compute_uc(c2);
    if (ht) return compute_dword(verify1(ht, c2)); // MARK(verify2)
    return 0;
}

INLINE DWORD verify3(HANDLE ht, unsigned char c3)
{
    if (ht == (HANDLE)3) return 17;
    c3 = compute_uc(c3);
    if (ht) return compute_dword(verify2(ht, c3));  // MARK(verify3)
    return 0;
}

NOINLINE DWORD CALLBACK stack_walk_thread(void *arg)
{ // MARK(BEG_stack_walk_thread)
    UNREFERENCED_PARAMETER(arg);
    return compute_dword(verify3(GetCurrentThread(), 0));
} // MARK(END_stack_walk_thread)

struct foobar
{
    char ch;
    char* array[12];
    unsigned bf1 : 3, bf2 : 7, bf3 : 2;
    unsigned u;
};

static struct foobar static_myfoobar;
       struct foobar global_myfoobar;

enum myenum
{
    FIRST_VALUE,
    SECOND_VALUE,
    THIRD_VALUE = 57,
    FOURTH_VALUE,
    /* use one negative value to force signed enum
     * (gcc tends to use unsigned values when all constants are > = 0)
     */
    FIFTH_VALUE = -1,
};

static enum myenum enum_value = FIRST_VALUE;

static void (*change)(DWORD* v);

NOINLINE unsigned function_AA(struct foobar* fb, DWORD64 base) // MARK(function_AA)
{
    static int static_int;

    DWORD other = (DWORD)base;
label:
    fb->u += other + enum_value + static_myvar_int++ + static_int++;
    if (fb->ch != 0)  /* this path is never taken, so feel free to do dumb things */
    {
        int f1 = (3 * static_int) % 13;
        if (f1 == 5) other++;
        change(&other); /* as we use its address, compiler likely won't throw it away */
        goto label;
    }
    return fb->bf1 == 0;
}

BOOL WINAPI DllMain(HINSTANCE instance_new, DWORD reason, LPVOID reserved)
{
    static int static_func_int;

    UNREFERENCED_PARAMETER(instance_new);

    static_func_int++;
    compute_uc = &icompute_uc;
    compute_dword = &icompute_dword;
    static_func_int += justin_case(static_func_int);
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
        function_AA(&static_myfoobar, (DWORD_PTR)reserved + static_func_int);
        break;
    }
    return TRUE;
}

/* force use of some SDK types, so we ensure they are present in all debug information formats */
typedef signed char SCHAR;
typedef double DOUBLE; /* defined in ntdef.h */
typedef long double LDOUBLE;
typedef _Bool _BOOL;
/* and even define our own */
typedef long long mytype_LONGLONG; /* don't use LONGLONG as it's *NOT* defined as long long */
typedef unsigned long long mytype_ULONGLONG;

CHAR myvar_CHAR;
SCHAR myvar_SCHAR;
UCHAR myvar_UCHAR;
WCHAR myvar_WCHAR;
wchar_t myvar_wchar_t;
INT myvar_INT;
UINT myvar_UINT;
LONG myvar_LONG;
ULONG myvar_ULONG;
DWORD myvar_DWORD;
LONG_PTR myvar_LONG_PTR;
ULONG_PTR myvar_ULONG_PTR;
DWORD_PTR myvar_DWORD_PTR;
INT_PTR myvar_INT_PTR;
UINT_PTR myvar_UINT_PTR;
LONG64 myvar_LONG64;
DWORD64 myvar_DWORD64;
HRESULT myvar_HRESULT;
BOOL myvar_BOOL;
BOOLEAN myvar_BOOLEAN;
INT8 myvar_INT8;
INT16 myvar_INT16;
INT32 myvar_INT32;
INT64 myvar_INT64;
UINT8 myvar_UINT8;
UINT16 myvar_UINT16;
UINT32 myvar_UINT32;
UINT64 myvar_UINT64;
LONGLONG myvar_LONGLONG;
ULONGLONG myvar_ULONGLONG;
mytype_LONGLONG myvar_mytype_LONGLONG;
mytype_ULONGLONG myvar_mytype_ULONGLONG;
FLOAT myvar_FLOAT;
DOUBLE myvar_DOUBLE;
LDOUBLE myvar_LDOUBLE;
_BOOL myvar__BOOL;

