/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Entry point for managing command line and driving internals.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <windows.h>
#include "debuggee_cc.hh"

static int static_myvar_int; /* also defined in debuggee.cc */

int justin_case(int v)
{ // MARK(BEG_justin_case)
    v = my_increment(3 * v + 2);
    if (v == 0)
        v = justin_case(13);
    return 2 * v + static_myvar_int++; // MARK(LAST_justin_case)
}

/* force use of some SDK types, so we ensure they are present in all debug information formats */
/* also renamed types with typedefs so that we can differentiate between C and C++ compilation */
typedef bool CXX_bool;
typedef CHAR CXX_CHAR;
typedef signed char CXX_SCHAR;
typedef UCHAR CXX_UCHAR;
typedef WCHAR CXX_WCHAR;
typedef wchar_t CXX_wchar_t;
typedef char8_t CXX_CHAR8;
typedef char16_t CXX_CHAR16;
typedef char32_t CXX_CHAR32;

CXX_bool mycxxvar_bool;
CXX_CHAR mycxxvar_CHAR;
CXX_SCHAR mycxxvar_SCHAR;
CXX_UCHAR mycxxvar_UCHAR;
CXX_WCHAR mycxxvar_WCHAR;
CXX_wchar_t mycxxvar_wchar_t;
CXX_CHAR8 mycxxvar_char8_t;
CXX_CHAR16 mycxxvar_char16_t;
CXX_CHAR32 mycxxvar_char32_t;
