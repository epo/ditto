/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Basic tests for debug information gotten from dbghelp.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if defined(__GNUC__)
#define INLINE inline __attribute__((always_inline))
#define NOINLINE __attribute__((noinline))
/* mingw doesn't support windows's style tls variables... */
#define THREAD_STORAGE
#elif defined(_MSC_VER)
#define INLINE __forceinline
#define NOINLINE __declspec(noinline)
#define THREAD_STORAGE __declspec(thread)
#else
#define INLINE
#define NOINLINE
#define THREAD_STORAGE
#endif

#ifdef __cplusplus
template <typename T>
static inline T my_increment(T u)
{ // MARK(my_increment)
    return u + 1;
}
extern "C" NOINLINE int justin_case(int); // so that we don't get a mangled name
#else
extern NOINLINE int justin_case(int);
#endif

