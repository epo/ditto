/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Internals definitions.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <dbghelp.h>
#include <cvconst.h>

#ifdef _MSC_VER
#define strdup      _strdup
#define strncasecmp _strnicmp
#define strcasecmp  _stricmp
#define wcsdup      _wcsdup
#define wcsnicmp    _wcsnicmp
#endif

#if defined(_MSC_VER)
#define NORETURN __declspec(noreturn)
#elif defined(__GNUC__)
#define NORETURN __attribute__((noreturn))
#else
#define NORETURN
#endif

enum debuggee_kind
{
    EXTERNAL_PROCESS = 0x02,
    MODULE_LOADLIB = 0x01,
};

struct debuggee
{
    const char* dll_name;
    const char* module_name; /* construct upon dll_name */
    enum debuggee_kind kind; /* mask of */
    HANDLE process;
    HANDLE thread;
    DWORD machine;
    DWORD64 base;
    DWORD64 size;
};

extern void debuggee_tests(const struct debuggee* dbg);
extern void coherence_tests(const struct debuggee* dbg);

/* from helper.c */

struct split_path
{
    const char* dir;
    unsigned    dir_len;
    const char* filename;
    unsigned    filename_len;
    const char* ext;
    unsigned    ext_len;
};

extern void split_path(const char* path, struct split_path* sp);

struct string_cache
{
    const char** cache;
    unsigned int allocated;
};

extern void        string_cache_init(struct string_cache* cache);
extern void        string_cache_dispose(struct string_cache* cache);
extern const char* string_cache_insert(struct string_cache* cache, const char* s);

extern const char* basetype2str(DWORD bt);
extern const char* tag2str(enum SymTagEnum tag);
extern const char* ti2str(IMAGEHLP_SYMBOL_TYPE_INFO ti);
extern const char* basetype2str(DWORD bt);
extern const char* datakind2str(DWORD dk);

extern DWORD       get_top_symbol(const struct debuggee* dbg);
extern DWORD       untypedef(const struct debuggee* dbg, DWORD type);
extern TI_FINDCHILDREN_PARAMS* retrieve_children(const struct debuggee* dbg, DWORD id);
extern BOOL        get_symbol_flags(const struct debuggee* dbg, DWORD64 base, DWORD id, DWORD* flags);

struct edge
{
    DWORD from;
    DWORD to;
    DWORD what; // ~0u when children, or TI_
};

struct vacuum
{
    const struct debuggee*      dbg;
    ULONG_PTR                   base; /* address inside module to explore */
    DWORD*                      ids; /* array of generated id:s */
    unsigned                    num_ids, alloc_ids;
    struct edge*                edges; /* array of generated edges between id:s */
    unsigned                    num_edges, alloc_edges;
};

struct ti_info
{
    unsigned size;
    unsigned recurse;
    const char* display;
};
extern const struct ti_info ti_info[IMAGEHLP_SYMBOL_TYPE_INFO_MAX];

extern BOOL create_vacuum(const struct debuggee* dbg, ULONG_PTR base, struct vacuum* v);
extern void dispose_vacuum(struct vacuum* v);
extern const char* id2str(const struct vacuum* v, DWORD id);
extern void dump_symbol(const struct vacuum* v, DWORD id);
extern void dump_symbol_compact(const struct vacuum* v, DWORD id);

typedef DWORD64 tagmask_t;

C_ASSERT(SymTagMax <= sizeof(tagmask_t) * 8);
static inline tagmask_t tag2mask(enum SymTagEnum tag)
{
    return ((tagmask_t)1) << tag;
}

typedef DWORD64 timask_t;

C_ASSERT(IMAGEHLP_SYMBOL_TYPE_INFO_MAX <= sizeof(timask_t) * 8);
static inline timask_t ti2mask(IMAGEHLP_SYMBOL_TYPE_INFO ti)
{
    return ((timask_t)1) << ti;
}
extern const char* addr2str(const struct debuggee* dbg, DWORD64 addr);

/* from explore.c */
extern void do_dump(const struct debuggee* dbg, const char* filter, BOOL compact);
extern void do_matrix(const struct debuggee* dbg);
extern void do_edge(const struct debuggee* dbg, const char* filter, BOOL with_dot);
enum map_modes
{
    MAP_BY_ADDRESS = 0,
    MAP_HIERARCHY = 0x80,
    MAP_WITH_LINES = 0x40,
    MAP_WITH_INLINED = 0x20,
};

extern void do_map(const struct debuggee* dbg, enum map_modes mode, const char* filter);
extern void do_statistics(const struct debuggee* dbg);
