/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Exploration routines
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "ditto.h"
#include <oaidl.h>

void do_dump(const struct debuggee* dbg, const char* filter, BOOL compact)
{
    struct vacuum v;
    tagmask_t tagmask;
    if (filter)
    {
        const char* ptr;
        unsigned len;
        int i;
        tagmask = 0;
        for (ptr = filter; *ptr; ptr += len)
        {
            const char* next = strchr(ptr, ';');
            len = (unsigned)((next) ? (next - ptr) : strlen(ptr));

            for (i = 0; i < SymTagMax; i++)
            {
                const char* tagstr = tag2str(i);
                if ((!strncmp(tagstr, ptr, len) && !tagstr[len]) ||
                    (!strncmp(tagstr + 6, ptr, len) && !tagstr[6 + len]))
                {
                    tagmask |= tag2mask(i);
                    break;
                }
            }
            if (i >= SymTagMax)
            {
                printf("Unknown tag %.*s in %s\n", len, ptr, filter);
                return;
            }
            if (next) len++;
        }
    }
    else tagmask = ~(DWORD64)0;
    if (create_vacuum(dbg, (ULONG_PTR)dbg->base, &v))
    {
        unsigned i;
        DWORD tag;

        for (i = 0; i < v.num_ids; ++i)
        {
            if (SymGetTypeInfo(dbg->process, dbg->base, v.ids[i], TI_GET_SYMTAG, &tag) && tag < SymTagMax &&
                (tagmask & tag2mask(tag)))
            {
                if (compact)
                    dump_symbol_compact(&v, v.ids[i]);
                else
                    dump_symbol(&v, v.ids[i]);
            }
        }
        dispose_vacuum(&v);
    }
}

void do_matrix(const struct debuggee* dbg)
{
    timask_t tag_ti_success[SymTagMax];
    timask_t tag_ti_failure[SymTagMax];
    IMAGEHLP_SYMBOL_TYPE_INFO ti;
    char buf[256];
    enum SymTagEnum tag;
    struct vacuum v;

    if (create_vacuum(dbg, (ULONG_PTR)dbg->base, &v))
    {
        unsigned i;

        memset(tag_ti_success, 0, sizeof(tag_ti_success));
        memset(tag_ti_failure, 0, sizeof(tag_ti_failure));
        for (i = 0; i < v.num_ids; ++i)
        {
            if (!SymGetTypeInfo(dbg->process, dbg->base, v.ids[i], TI_GET_SYMTAG, &tag) || tag >= SymTagMax)
                /* we don't know where to store the error */
                continue;

            for (ti = 0; ti < IMAGEHLP_SYMBOL_TYPE_INFO_MAX; ++ti)
            {
                if (!ti_info[ti].size) continue;
                if (tag == SymTagData && ti == TI_GET_VALUE)
                {
                    // native crashes on this one... don't quite get why... internal overflow?
                    // tried VariantClear but without improvement
                    continue;
                    //dump_symbol(&v, v.ids[i]); fflush(stdout);
                }
                if (SymGetTypeInfo(dbg->process, dbg->base, v.ids[i], ti, buf))
                {
                    tag_ti_success[tag] |= ti2mask(ti);
                    if (ti == TI_GET_SYMNAME)
                        LocalFree(*(WCHAR**)buf);
                }
                else
                    tag_ti_failure[tag] |= ti2mask(ti);
            }
        }
        printf("\nMatrix tag x TI\n");
        for (i = SymTagNull; i < SymTagMax; ++i)
        {
            printf("%-25s:", tag2str(i));
            if ((tag_ti_success[i] | tag_ti_failure[i]) == 0)
                printf(" <<never seen>>\n");
            else
            {
                for (ti = 0; ti < IMAGEHLP_SYMBOL_TYPE_INFO_MAX; ++ti)
                    if (tag_ti_success[i] & ti2mask(ti))
                        printf(" %s%s", (tag_ti_failure[i] & ti2mask(ti)) ? "~" : "", ti_info[ti].display);
                printf("\n");
            }
        }
        dispose_vacuum(&v);
    }
}

void do_statistics(const struct debuggee* dbg)
{
    tagmask_t tag_count[SymTagMax];
    enum SymTagEnum tag;
    struct vacuum v;

    memset(tag_count, 0, sizeof(tag_count));
    if (create_vacuum(dbg, (ULONG_PTR)dbg->base, &v))
    {
        unsigned i;

        for (i = 0; i < v.num_ids; ++i)
        {
            if (!SymGetTypeInfo(dbg->process, dbg->base, v.ids[i], TI_GET_SYMTAG, &tag) || tag >= SymTagMax)
                /* we don't know where to store the error */
                continue;
            tag_count[tag]++;
        }
        printf("\nStatistics\n");
        for (i = SymTagNull; i < SymTagMax; ++i)
        {
            printf("%-25s: %I64u\n", tag2str(i), tag_count[i]);
        }
        dispose_vacuum(&v);
    }
}

static void do_edge_helper(const struct vacuum* v, unsigned kind, const char* explain, BOOL with_dot)
{
    ULONG64 tag_parents[SymTagMax];
    enum SymTagEnum tag, parenttag;
    unsigned i, j;

    memset(tag_parents, 0, sizeof(tag_parents));
    for (i = 0; i < v->num_edges; ++i)
    {
        if (v->edges[i].what == kind)
        {
            if (SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].from, TI_GET_SYMTAG, &tag) &&
                SymGetTypeInfo(v->dbg->process, v->base, v->edges[i].to,   TI_GET_SYMTAG, &parenttag))
            {
                tag_parents[tag] |= tag2mask(parenttag);
            }
        }
    }
    /* so that we remove the SymTag prefix */
#define SYMTAGOFFSET 6
    if (with_dot)
    {
        printf("digraph dbg_%s {\n", explain);
        for (i = 0; i < SymTagMax; ++i)
            for (j = 0; j < SymTagMax; ++j)
                if (tag_parents[i] & tag2mask(j))
                    printf("\t%s -> %s\n", tag2str(j) + SYMTAGOFFSET, tag2str(i) + SYMTAGOFFSET);
        printf("\tlabel=\"Matrix tag => %s-tag\"\n", explain);
        printf("}\n");
    }
    else
    {
        printf("\nMatrix tag => %s-tag\n", explain);
        for (i = 0; i < SymTagMax; ++i)
        {
            printf("%s:", tag2str(i) + SYMTAGOFFSET);
            for (j = 0; j < SymTagMax; ++j)
                if (tag_parents[i] & tag2mask(j))
                    printf(" %s", tag2str(j) + SYMTAGOFFSET);
            printf("\n");
        }
    }
#undef SYMTAGOFFSET
}

void do_edge(const struct debuggee* dbg, const char* filter, BOOL with_dot)
{
    struct vacuum v;

    if (create_vacuum(dbg, (ULONG_PTR)dbg->base, &v))
    {
        if (filter)
        {
            DWORD kind;
            if (!strcmp(filter, "lexical")) kind = TI_GET_LEXICALPARENT;
            else if (!strcmp(filter, "symindex")) kind = TI_GET_SYMINDEX;
            else if (!strcmp(filter, "type")) kind = TI_GET_TYPE;
            else if (!strcmp(filter, "typeid")) kind = TI_GET_TYPEID;
            else if (!strcmp(filter, "class")) kind = TI_GET_CLASSPARENTID;
            else if (!strcmp(filter, "child")) kind = ~0u;
            else
            {
                printf("Unknown filter %s\n", filter);
                exit(1);
            }
            do_edge_helper(&v, kind, filter, with_dot);
        }
        else
        {
            do_edge_helper(&v, TI_GET_LEXICALPARENT, "lexical", with_dot);
            do_edge_helper(&v, TI_GET_SYMINDEX, "symindex", with_dot);
            do_edge_helper(&v, TI_GET_TYPE, "type", with_dot);
            do_edge_helper(&v, TI_GET_TYPEID, "typeid", with_dot);
            do_edge_helper(&v, TI_GET_CLASSPARENTID, "class", with_dot);
            do_edge_helper(&v, ~0u, "child", with_dot);
        }
        dispose_vacuum(&v);
    }
}

/* FIXME: before closing the patch
 * - inside mode to be rechecked
 * - string_cache to be rewritten
 * - wine integration (get rid of most of WINESRC)
 * - print_inline_block the %-30s vs inlinectx_displacement isn't nice to see
 */

struct inline_block
{
    unsigned depth;
    struct
    {
        const char* function;
        const char* file;
        unsigned line;
        unsigned line_displacement;
        unsigned inlinectx_displacement;
    }* elements;
};

static struct string_cache mycache;

static void grab_inline_block(const struct debuggee* dbg, const SYMBOL_INFO* si, DWORD64 addr, struct inline_block* ib, BOOL with_lines)
{
    char subsi_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO* subsi = (SYMBOL_INFO*)subsi_buf;
    IMAGEHLP_LINE64 li;
    DWORD j, ldsp, inline_ctx, frmidx;
    DWORD64 ildsp;

    ib->depth = SymAddrIncludeInlineTrace(dbg->process, addr);

    if (!SymQueryInlineTrace(dbg->process, si->Address, 0, si->Address + si->Size, addr, &inline_ctx, &frmidx))
        inline_ctx = 0;

    li.SizeOfStruct = sizeof(li);
    subsi->SizeOfStruct = sizeof(*subsi);
    subsi->MaxNameLen = 200;
#ifdef __WINESRC__
    subsi->MaxNameLen++;
#endif
    ib->elements = malloc((ib->depth + 1) * sizeof(ib->elements[0]));
    for (j = 0; j <= ib->depth; j++)
    {
        if (SymFromInlineContext(dbg->process, addr, inline_ctx + j, &ildsp, subsi))
        {
            /* Native DbgHelp doesn't always terminate the string, sigh */
            subsi->Name[si->MaxNameLen] = '\0';
            ib->elements[j].function = string_cache_insert(&mycache, subsi->Name);
            ib->elements[j].inlinectx_displacement = (unsigned)ildsp;
            if (with_lines && SymGetLineFromInlineContext(dbg->process, addr, inline_ctx + j, 0, &ldsp, &li))
            {
                ib->elements[j].file = string_cache_insert(&mycache, li.FileName);
                ib->elements[j].line = li.LineNumber;
                ib->elements[j].line_displacement = ldsp;
            }
            else
            {
                ib->elements[j].file = NULL;
            }
        }
        else
        {
            ib->elements[j].function = NULL;
            ib->elements[j].file = NULL;
        }
    }
}

static BOOL is_same_inline_block(const struct inline_block* ib1, const struct inline_block* ib2, unsigned delta)
{
    DWORD j;

    if (ib1->depth != ib2->depth) return FALSE;
    for (j = 0; j <= ib1->depth; j++)
    {
        if (!ib1->elements[j].function && !ib2->elements[j].function) continue;
        if (!ib1->elements[j].function || !ib2->elements[j].function) return FALSE;
        if (strcmp(ib1->elements[j].function, ib2->elements[j].function)) return FALSE;
        if (ib1->elements[j].inlinectx_displacement + delta != ib2->elements[j].inlinectx_displacement) return FALSE;
        if (!ib1->elements[j].file && !ib2->elements[j].file) continue;
        if (!ib1->elements[j].file || !ib2->elements[j].file) return FALSE;
        if (strcmp(ib1->elements[j].file, ib2->elements[j].file)) return FALSE;
        if (ib1->elements[j].line != ib2->elements[j].line) return FALSE;
        if (ib1->elements[j].line_displacement + delta != ib2->elements[j].line_displacement) return FALSE;
    }
    return TRUE;
}

static void dispose_inline_block(struct inline_block* ib)
{
    free(ib->elements);
}

static void print_inline_block(struct inline_block* ib, ULONG_PTR from, ULONG_PTR range)
{
    BOOL first_printed = FALSE;
    DWORD j;

    for (j = ib->depth; j <= ib->depth; j--)
    {
        if (!first_printed)
        {
            printf("\t%Ix:\t#%Ix\t", from, range);
            first_printed = TRUE;
        }
        else
            printf("\t\t\t");
        if (ib->elements[j].function)
        {
            printf("%s%-30s", (j == ib->depth) ? "+  " : (j ? "|- " : "\\- "), ib->elements[j].function);
            printf("+%x", ib->elements[j].inlinectx_displacement);
            if (ib->elements[j].file)
            {
                printf("\t%s:%u+%#x", ib->elements[j].file,
                       ib->elements[j].line, ib->elements[j].line_displacement);
            }
        }
        printf("\n");
    }
}

static void map_show_element(const struct debuggee* dbg, DWORD64 addr, const SYMBOL_INFO* siref, enum map_modes mode)
{
    if (siref->Index == ~0u)
    {
        printf("%I64x-%I64x <<EMPTY>>\n", siref->Address - dbg->base, addr - dbg->base);
        return;
    }
    printf("%I64x-%I64x %s %s flags=%lx size=%lx\n",
           siref->Address - dbg->base, addr - dbg->base,
           siref->Name, tag2str(siref->Tag), siref->Flags, siref->Size);
    if (siref->Tag == SymTagFunction && (mode & MAP_WITH_INLINED))
    {
        struct inline_block ibcurr, ibnext;
        DWORD i, from_offset;

        from_offset = 0;
        grab_inline_block(dbg, siref, siref->Address, &ibcurr, mode & MAP_WITH_LINES);
        for (i = 1; i < siref->Size; i++)
        {
            grab_inline_block(dbg, siref, siref->Address + i, &ibnext, mode & MAP_WITH_LINES);
            if (!is_same_inline_block(&ibcurr, &ibnext, i - from_offset))
            {
                print_inline_block(&ibcurr, from_offset, i - from_offset);
                from_offset = i;
                dispose_inline_block(&ibcurr);
                ibcurr = ibnext;
            }
        }
        if (siref->Size)
        {
            print_inline_block(&ibcurr, from_offset, i - from_offset);
            dispose_inline_block(&ibcurr);
        }
    }
}

// missing from dbghelp.h in Win11 (until it's fixed)
#define SYMFLAG_REGREL_ALIASINDIR  0x00800000
#define SYMFLAG_FIXUP_ARM64X       0x01000000
#define SYMFLAG_GLOBAL             0x02000000

static BOOL sym_from_addr(const struct debuggee* dbg, ULONG64 addr, SYMBOL_INFO* si)
{
    DWORD64 disp;

    if (!SymFromAddr(dbg->process, addr, &disp, si)) return FALSE;
    /* if no size set, try to get it from type */
    if (!si->Size && !SymGetTypeInfo(dbg->process, dbg->base, si->TypeIndex, TI_GET_LENGTH, &si->Size))
        return FALSE;
    /* Native DbgHelp doesn't always terminate the string, sigh */
    si->Name[si->MaxNameLen] = '\0';
    /* Clear some flags not correctly handled in Builtin yet */
    si->Flags &= ~(SYMFLAG_THUNK /* builtin shouldn't return it */ |
                   SYMFLAG_FUNC_NO_RETURN /* builtin should return it when needed */ |
                   SYMFLAG_GLOBAL /* returned by native */
        );
    /* Native DbgHelp returns symbol with highest address below 'addr'
     * => ensure we're inside the symbol
     * FIXME in builtin
     */
    return disp < si->Size;
}

static void do_map_by_address(const struct debuggee* dbg, enum map_modes mode, DWORD64 from, DWORD64 to)
{
    char siref_buf[sizeof(SYMBOL_INFO) + 200];
    SYMBOL_INFO *siref = (SYMBOL_INFO *)siref_buf;
    SYMBOL_INFO sifast;

    siref->SizeOfStruct = sizeof(SYMBOL_INFO);
    siref->MaxNameLen = 200;
#ifdef __WINESRC__
    siref->MaxNameLen++;
#endif
    siref->Index = ~0u;
    siref->Address = from;

    sifast.SizeOfStruct = sizeof(SYMBOL_INFO);
    sifast.MaxNameLen = 0;

    /* note: the following code doesn't take into account overlapping symbols
     * (they do exist: for example aliases)
     */
    while (from < to)
    {
        if (sym_from_addr(dbg, from, &sifast))
        {
            /* still in same object? */
            if (sifast.Index == siref->Index) continue;
            /* close current object */
            map_show_element(dbg, from, siref, mode);
            /* start new segment; record start of func */
            if (!sym_from_addr(dbg, from, siref)) printf("badbadbad\n");
            from += siref->Size ? siref->Size : 1;
        }
        else
        {
            if (siref->Index != ~0u) /* entering empty zone */
            {
                /* close current segment fo function */
                map_show_element(dbg, from, siref, mode);
                /* start new segment: empty */
                siref->Index = ~0u;
                siref->Address = from;
            } /* else: still in empty zone */
            from++;
        }
    }
    /* close last object */
    map_show_element(dbg, from, siref, mode);
}

struct map_user_cb
{
    const struct debuggee*      dbg;
    enum map_modes              mode;
    tagmask_t                   mask;
};

static BOOL CALLBACK map_by_addr_enum_cb(PSYMBOL_INFO sym, ULONG size, void* usr)
{
    struct map_user_cb* muc = usr;
    do_map_by_address(muc->dbg, muc->mode, sym->Address, sym->Address + size);
    return TRUE;
}

const struct debuggee* my_qsort_dbg;
static int sym_addr_compare(const void* p1, const void* p2)
{
    BOOL ret1, ret2;
    DWORD64 addr1, addr2;

    ret1 = SymGetTypeInfo(my_qsort_dbg->process, my_qsort_dbg->base, *(DWORD*)p1, TI_GET_ADDRESS, &addr1);
    ret2 = SymGetTypeInfo(my_qsort_dbg->process, my_qsort_dbg->base, *(DWORD*)p2, TI_GET_ADDRESS, &addr2);
    if (!ret1 && !ret2) return 0;
    if (!ret1) return -1;
    if (!ret2) return +1;
    return (addr1 == addr2) ? 0 : (addr1 < addr2) ? -1 : +1;
}

static void do_map_hierarchy(const struct debuggee* dbg, tagmask_t tag_recurse_mask, DWORD id, unsigned int depth)
{
    unsigned i;
    DWORD tag, typid;
    DWORD64 addr, length;
    WCHAR* name;
    char tmp[64];
    size_t len;
    size_t first_row = 32;
    TI_FINDCHILDREN_PARAMS* tfp;

    if (!SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_SYMTAG, &tag) || !(tag_recurse_mask & tag2mask(tag)))
        return;

    snprintf(tmp, sizeof(tmp), "%*s", 2 * depth, "");
    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_ADDRESS, &addr))
    {
        /* Don't expect success on TI_GET_LENGTH for global variables (native fails, unlike builtin)
         * So fallback to type's size if direct request fails.
         */
        if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_LENGTH, &length) ||
            (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_TYPEID, &typid) &&
             SymGetTypeInfo(dbg->process, dbg->base, typid, TI_GET_LENGTH, &length)))
        {
            if (addr >= dbg->base && addr < dbg->base + dbg->size)
                snprintf(tmp + strlen(tmp), sizeof(tmp), "[%I64x-%I64x]", addr - dbg->base, addr + length - dbg->base);
            else
                snprintf(tmp + strlen(tmp), sizeof(tmp), "[%I64x-%I64x]abs", addr, addr + length);
        }
        else
        {
            if (addr >= dbg->base && addr < dbg->base + dbg->size)
                snprintf(tmp + strlen(tmp), sizeof(tmp), "[%I64x]", addr - dbg->base);
            else
                snprintf(tmp + strlen(tmp), sizeof(tmp), "[%I64x]abs", addr);
        }
    }
    first_row += 2 * depth;
    if ((len = strlen(tmp)) < first_row)
    {
        memset(&tmp[len], ' ', first_row - len);
        tmp[first_row] = '\0';
    }
    printf("%s %s ", tmp, tag2str(tag));

    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_SYMNAME, &name))
    {
        printf("%ls ", name);
        LocalFree(name);
    }
    printf("\n");

    if ((tfp = retrieve_children(dbg, id)) != NULL)
    {
        my_qsort_dbg = dbg;
        qsort(tfp->ChildId, tfp->Count, sizeof(tfp->ChildId[0]), sym_addr_compare);
        for (i = 0; i < tfp->Count; i++)
            do_map_hierarchy(dbg, tag_recurse_mask, tfp->ChildId[i], depth + 1);
        free(tfp);
    }
}

static BOOL CALLBACK map_hierarchy_enum_cb(PSYMBOL_INFO sym, ULONG size, void* usr)
{
    struct map_user_cb* muc = usr;
    UNREFERENCED_PARAMETER(size);

    do_map_hierarchy(muc->dbg, muc->mask, sym->Index, 0);
    return TRUE;
}

void do_map(const struct debuggee* dbg, enum map_modes mode, const char* filter)
{
    string_cache_init(&mycache);

    if (mode & MAP_HIERARCHY)
    {
        tagmask_t tag_recurse_mask = tag2mask(SymTagExe) | tag2mask(SymTagCompiland) |
            tag2mask(SymTagThunk) | tag2mask(SymTagFunction) | tag2mask(SymTagBlock) |
            tag2mask(SymTagFuncDebugStart) | tag2mask(SymTagFuncDebugEnd);
        if (mode & MAP_WITH_INLINED) tag_recurse_mask |= tag2mask(SymTagInlineSite);

        if (mode & MAP_WITH_LINES) printf("Lines not supported yet\n");

        if (filter)
        {
            struct map_user_cb muc = {dbg, mode, tag_recurse_mask};
            SymEnumSymbols(dbg->process, dbg->base, filter, map_hierarchy_enum_cb, &muc);
        }
        else
        {
            DWORD id = get_top_symbol(dbg);
            if (id)
                do_map_hierarchy(dbg, tag_recurse_mask, id, 0);
            else
                printf("Couldn't find module's %s top symbol\n", dbg->module_name);
        }
    }
    else
    {
        if (filter)
        {
            struct map_user_cb muc = {dbg, mode, 0};
            SymEnumSymbols(dbg->process, dbg->base, filter, map_by_addr_enum_cb, &muc);
        }
        else
            do_map_by_address(dbg, mode, dbg->base, dbg->base + dbg->size);
    }
}
