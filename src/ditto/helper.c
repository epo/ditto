/*
 * This file is part of Wine DITTO (Wine DebugInfo Test Tool)
 *
 * Helper for extracting information out of DbgHelp.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define _FORCENAMELESSUNION
#include "ditto.h"
#include "test.h"
#include <oaidl.h>

#if defined(__GNUC__)
static inline char* mysprintf(const char* fmt, ...) __attribute__ ((format(printf, 1, 2)));
#endif
static inline char* mysprintf(const char* fmt, ...)
{
    static char buf[4096];
    static char* ptr = buf;
    static char errorbuf[] = "<<error>>";
    va_list args;
    int len = (int)(buf + sizeof(buf) - ptr), aclen;
    char* ret = ptr;

    va_start(args, fmt);
    aclen = vsnprintf(ptr, len, fmt, args);
    va_end(args);
    if (aclen < 0) return errorbuf;
    if (aclen >= len) /* not enough space, wrap around */
    {
        aclen = max(aclen, sizeof(buf) / 4);
        ptr = buf;
        va_start(args, fmt);
        aclen = vsnprintf(ptr, aclen + 1, fmt, args);
        va_end(args);
        if (aclen < 0) return errorbuf;
    }
    ret = ptr;
    ptr += aclen + 1;
    return ret;
}

void split_path(const char* path, struct split_path* sp)
{
    const char* ff;

    if ((ff = strrchr(path, '/')) == NULL)
        ff = path;
    else
        ++ff;
    if ((sp->filename = strrchr(ff, '\\')) == NULL)
        sp->filename = ff;
    else
        ++sp->filename;

    sp->ext = strchr(sp->filename, '.');
    if (!sp->ext) sp->ext = sp->filename + strlen(sp->filename);

    sp->dir = path;
    sp->dir_len = (int)(sp->filename - path);
    sp->filename = sp->filename;
    sp->filename_len = (int)(sp->ext - sp->filename);
    sp->ext = sp->ext;
    sp->ext_len = (unsigned)strlen(sp->ext);
}

void string_cache_init(struct string_cache* cache)
{
    cache->cache = NULL;
    cache->allocated = 0;
}

void string_cache_dispose(struct string_cache* cache)
{
    unsigned i;

    for (i = 0; i < cache->allocated; i++)
        free((void*)cache->cache[i]);
    free((void*)cache->cache);
}

static int __cdecl string_cache_compare(const void *p1, const void *p2)
{
    return strcmp(*(const char**)p1, *(const char**)p2);
}

const char* string_cache_insert(struct string_cache* cache, const char* s)
{
    const char** new;
    const char* result;

    if (cache->cache)
    {
        const char** ret = bsearch(&s, cache->cache, cache->allocated, sizeof(cache->cache[0]), string_cache_compare);
        if (ret) return *ret;
    }

    new = realloc((void*)cache->cache, (cache->allocated + 1) * sizeof(cache->cache[0]));
    if (!new) return NULL;

    cache->cache = new;
    result = cache->cache[cache->allocated++] = strdup(s);
    qsort((void*)cache->cache, cache->allocated, sizeof(cache->cache[0]), string_cache_compare);

    return result;
}


/****************************
 * Dumping to strings       *
 ****************************/

const char* tag2str(enum SymTagEnum tag)
{
    switch (tag)
    {
    case SymTagNull:                    return "SymTagNull";
    case SymTagExe:                     return "SymTagExe";
    case SymTagCompiland:               return "SymTagCompiland";
    case SymTagCompilandDetails:        return "SymTagCompilandDetails";
    case SymTagCompilandEnv:            return "SymTagCompilandEnv";
    case SymTagFunction:                return "SymTagFunction";
    case SymTagBlock:                   return "SymTagBlock";
    case SymTagData:                    return "SymTagData";
    case SymTagAnnotation:              return "SymTagAnnotation";
    case SymTagLabel:                   return "SymTagLabel";
    case SymTagPublicSymbol:            return "SymTagPublicSymbol";
    case SymTagUDT:                     return "SymTagUDT";
    case SymTagEnum:                    return "SymTagEnum";
    case SymTagFunctionType:            return "SymTagFunctionType";
    case SymTagPointerType:             return "SymTagPointerType";
    case SymTagArrayType:               return "SymTagArrayType";
    case SymTagBaseType:                return "SymTagBaseType";
    case SymTagTypedef:                 return "SymTagTypedef";
    case SymTagBaseClass:               return "SymTagBaseClass";
    case SymTagFriend:                  return "SymTagFriend";
    case SymTagFunctionArgType:         return "SymTagFunctionArgType";
    case SymTagFuncDebugStart:          return "SymTagFuncDebugStart";
    case SymTagFuncDebugEnd:            return "SymTagFuncDebugEnd";
    case SymTagUsingNamespace:          return "SymTagUsingNamespace";
    case SymTagVTableShape:             return "SymTagVTableShape";
    case SymTagVTable:                  return "SymTagVTable";
    case SymTagCustom:                  return "SymTagCustom";
    case SymTagThunk:                   return "SymTagThunk";
    case SymTagCustomType:              return "SymTagCustomType";
    case SymTagManagedType:             return "SymTagManagedType";
    case SymTagDimension:               return "SymTagDimension";
    case SymTagCallSite:                return "SymTagCallSite";
    case SymTagInlineSite:              return "SymTagInlineSite";
    case SymTagBaseInterface:           return "SymTagBaseInterface";
    case SymTagVectorType:              return "SymTagVectorType";
    case SymTagMatrixType:              return "SymTagMatrixType";
    case SymTagHLSLType:                return "SymTagHLSLType";
    case SymTagCaller:                  return "SymTagCaller";
    case SymTagCallee:                  return "SymTagCallee";
    case SymTagExport:                  return "SymTagExport";
    case SymTagHeapAllocationSite:      return "SymTagHeapAllocationSite";
    case SymTagCoffGroup:               return "SymTagCoffGroup";
    case SymTagInlinee:                 return "SymTagInlinee";
    default:
        return mysprintf("SymTag<%u>", tag);
    }
}

const char* ti2str(IMAGEHLP_SYMBOL_TYPE_INFO ti)
{
    switch (ti)
    {
    case TI_GET_SYMTAG:                         return "TI_GET_SYMTAG";
    case TI_GET_SYMNAME:                        return "TI_GET_SYMNAME";
    case TI_GET_LENGTH:                         return "TI_GET_LENGTH";
    case TI_GET_TYPE:                           return "TI_GET_TYPE";
    case TI_GET_TYPEID:                         return "TI_GET_TYPEID";
    case TI_GET_BASETYPE:                       return "TI_GET_BASETYPE";
    case TI_GET_ARRAYINDEXTYPEID:               return "TI_GET_ARRAYINDEXTYPEID";
    case TI_FINDCHILDREN:                       return "TI_FINDCHILDREN";
    case TI_GET_DATAKIND:                       return "TI_GET_DATAKIND";
    case TI_GET_ADDRESSOFFSET:                  return "TI_GET_ADDRESSOFFSET";
    case TI_GET_OFFSET:                         return "TI_GET_OFFSET";
    case TI_GET_VALUE:                          return "TI_GET_VALUE";
    case TI_GET_COUNT:                          return "TI_GET_COUNT";
    case TI_GET_CHILDRENCOUNT:                  return "TI_GET_CHILDRENCOUNT";
    case TI_GET_BITPOSITION:                    return "TI_GET_BITPOSITION";
    case TI_GET_VIRTUALBASECLASS:               return "TI_GET_VIRTUALBASECLASS";
    case TI_GET_VIRTUALTABLESHAPEID:            return "TI_GET_VIRTUALTABLESHAPEID";
    case TI_GET_VIRTUALBASEPOINTEROFFSET:       return "TI_GET_VIRTUALBASEPOINTEROFFSET";
    case TI_GET_CLASSPARENTID:                  return "TI_GET_CLASSPARENTID";
    case TI_GET_NESTED:                         return "TI_GET_NESTED";
    case TI_GET_SYMINDEX:                       return "TI_GET_SYMINDEX";
    case TI_GET_LEXICALPARENT:                  return "TI_GET_LEXICALPARENT";
    case TI_GET_ADDRESS:                        return "TI_GET_ADDRESS";
    case TI_GET_THISADJUST:                     return "TI_GET_THISADJUST";
    case TI_GET_UDTKIND:                        return "TI_GET_UDTKIND";
    case TI_IS_EQUIV_TO:                        return "TI_IS_EQUIV_TO";
    case TI_GET_CALLING_CONVENTION:             return "TI_GET_CALLING_CONVENTION";
    case TI_IS_CLOSE_EQUIV_TO:                  return "TI_IS_CLOSE_EQUIV_TO";
    case TI_GTIEX_REQS_VALID:                   return "TI_GTIEX_REQS_VALID";
    case TI_GET_VIRTUALBASEOFFSET:              return "TI_GET_VIRTUALBASEOFFSET";
    case TI_GET_VIRTUALBASEDISPINDEX:           return "TI_GET_VIRTUALBASEDISPINDEX";
    case TI_GET_IS_REFERENCE:                   return "TI_GET_IS_REFERENCE";
    case TI_GET_INDIRECTVIRTUALBASECLASS:       return "TI_GET_INDIRECTVIRTUALBASECLASS";
    case TI_GET_VIRTUALBASETABLETYPE:           return "TI_GET_VIRTUALBASETABLETYPE";
#if API_VERSION_NUMBER > 12
    case TI_GET_OBJECTPOINTERTYPE:              return "TI_GET_OBJECTPOINTERTYPE";
#endif
    case IMAGEHLP_SYMBOL_TYPE_INFO_MAX:         return "IMAGEHLP_SYMBOL_TYPE_INFO_MAX";
    default:
        return mysprintf("TI_<%u>", ti);
    }
}

const char* basetype2str(DWORD bt)
{
    switch (bt)
    {
    case btVoid:  return "btVoid";
    case btChar:  return "btChar";
    case btWChar: return "btWChar";
    case btInt:   return "btInt";
    case btUInt:  return "btUInt";
    case btFloat: return "btFloat";
    case btBCD:   return "btBCD";
    case btBool:  return "btBool";
    case btLong:  return "btLong";
    case btULong: return "btULong";

#if 0
    case btCurrency:
    case btDate:
    case btVariant:
    case btComplex:
    case btBit:
    case btBSTR:
#endif
    case btHresult: return "btHresult";
    case btChar16:  return "btChar16_t";
    case btChar32:  return "btChar32_t";
    case btChar8:   return "btChar8_t";
    default:
        return mysprintf("basetype<%lu>", bt);
    }
}

static const char* udtkind2str(DWORD knd)
{
    switch (knd)
    {
    case UdtStruct: return "struct";
    case UdtClass: return "class";
    case UdtUnion: return "union";
    default:
        return mysprintf("udtkind<%lu>", knd);
    }
}

static const char* cc2str(DWORD cc)
{
    switch (cc)
    {
    case CV_CALL_NEAR_C:        return "NEAR CDECL";
    case CV_CALL_FAR_C:         return "FAR CDECL";
    case CV_CALL_NEAR_PASCAL:   return "NEAR PASCAL";
    case CV_CALL_FAR_PASCAL:    return "FAR PASCAL";
    case CV_CALL_NEAR_FAST:     return "NEAR FASTCALL";
    case CV_CALL_FAR_FAST:      return "FAR FASTCALL";
    case CV_CALL_SKIPPED:       return "SKIPPED";
    case CV_CALL_NEAR_STD:      return "STDCALL";
    case CV_CALL_FAR_STD:       return "FAR STDCALL";
    case CV_CALL_NEAR_SYS:      return "NEAR SYS";
    case CV_CALL_FAR_SYS:       return "FAR SYS";
    case CV_CALL_THISCALL:      return "THISCALL";

    case CV_CALL_MIPSCALL:
    case CV_CALL_GENERIC:
    case CV_CALL_ALPHACALL:
    case CV_CALL_PPCCALL:
    case CV_CALL_SHCALL:
    case CV_CALL_ARMCALL:
    case CV_CALL_AM33CALL:
    case CV_CALL_TRICALL:
    case CV_CALL_SH5CALL:
    case CV_CALL_M32RCALL:
    default:
        return mysprintf("callconv<<%lu>>", cc);
    }
}

const char* datakind2str(DWORD dk)
{
    switch (dk)
    {
    case DataIsUnknown:       return "DataIsUnknown";
    case DataIsLocal:         return "DataIsLocal";
    case DataIsStaticLocal:   return "DataIsStaticLocal";
    case DataIsParam:         return "DataIsParam";
    case DataIsObjectPtr:     return "DataIsObjectPtr";
    case DataIsFileStatic:    return "DataIsFileStatic";
    case DataIsGlobal:        return "DataIsGlobal";
    case DataIsMember:        return "DataIsMember";
    case DataIsStaticMember:  return "DataIsStaticMember";
    case DataIsConstant:      return "DataIsConstant";
    default:
        return mysprintf("datakind<<%lu>>", dk);
    }
};

static const char* variant2str(const VARIANT *v)
{
    switch (V_VT(v))
    {
    case VT_I1:   return mysprintf("%dS",         (INT)V_I1(v));
    case VT_I2:   return mysprintf("%dS",         (UINT)V_I2(v));
    case VT_I4:   return mysprintf("%dS",         (UINT)V_I4(v));
    case VT_I8:   return mysprintf("%I64dS",      V_I8(v));
    case VT_INT:  return mysprintf("%dS",         V_INT(v));
    case VT_UI1:  return mysprintf("%uU",         (UINT)V_UI1(v));
    case VT_UI2:  return mysprintf("%uU",         (UINT)V_UI2(v));
    case VT_UI4:  return mysprintf("%uU",         (UINT)V_UI4(v));
    case VT_UI8:  return mysprintf("%I64u",       V_UI8(v));
    case VT_UINT: return mysprintf("%uU",         V_UINT(v));
    case VT_BSTR: return mysprintf("\"%ls\"",     V_BSTR(v));
    default:      return mysprintf("variant<%u>", V_VT(v));
    }
}

const char* addr2str(const struct debuggee* dbg, DWORD64 addr)
{
    if (addr >= dbg->base && addr < dbg->base + dbg->size)
    {
        return mysprintf("%s[+%I64x]", dbg->module_name, addr - dbg->base);
    }
    return mysprintf("%#I64x", addr);
}

/****************************************
 * understanding SymGettypeinfo details *
 ****************************************/

/* When looking at a typedef (like DWORD), compilers & debug formats differ:
 * - some use the type of the typedef
 * - some others use the target of the typedef (especially, when it's a well defined
 *   type for the debug format)
 * Cope with this by removing all typedef:s from the debug info to get to the
 * target type.
 */
DWORD untypedef(const struct debuggee* dbg, DWORD type)
{
    DWORD tag;
    BOOL ret = TRUE;

    while (ret &&
           SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_SYMTAG, &tag) &&
           tag == SymTagTypedef)
    {
        ret = SymGetTypeInfo(dbg->process, dbg->base, type, TI_GET_TYPE, &type);
        ok(ret, "SymGetTypeInfo failed: %lu\n", GetLastError());
    }
    return type;
}

TI_FINDCHILDREN_PARAMS* retrieve_children(const struct debuggee* dbg, DWORD id)
{
    DWORD count;

    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_CHILDRENCOUNT, &count))
    {
        TI_FINDCHILDREN_PARAMS* tfp;

        tfp = malloc(sizeof(*tfp) + count * sizeof(tfp->ChildId[0]));
        if (tfp)
        {
            tfp->Count = count;
            tfp->Start = 0;
            if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_FINDCHILDREN, tfp))
                return tfp;
            free(tfp);
        }
    }
    return NULL;
}

struct top_symbol_usr
{
    const struct debuggee* dbg;
    DWORD id;
};

static BOOL CALLBACK top_symbol_enum_cb(PSYMBOL_INFO sym, ULONG size, void* usr)
{
    struct top_symbol_usr* tsu = usr;
    DWORD tag, id = sym->Index;
    UNREFERENCED_PARAMETER(size);

    do
    {
        if (SymGetTypeInfo(tsu->dbg->process, tsu->dbg->base, id, TI_GET_SYMTAG, &tag) && tag == SymTagExe)
        {
            tsu->id = id;
            return FALSE; /* stop enum */
        }
    } while (SymGetTypeInfo(tsu->dbg->process, tsu->dbg->base, id, TI_GET_LEXICALPARENT, &id));

    return TRUE;
}

DWORD get_top_symbol(const struct debuggee* dbg)
{
    struct top_symbol_usr tsu = {dbg, 0};
    SymEnumSymbols(dbg->process, dbg->base, NULL, top_symbol_enum_cb, &tsu);
    return tsu.id;
}

const struct ti_info ti_info[IMAGEHLP_SYMBOL_TYPE_INFO_MAX] =
{
    {sizeof(DWORD),   0, "SYMTAG"},                   //  0 TI_GET_SYMTAG
    {sizeof(WCHAR*),  0, "SYMNAME"},                  //  1 TI_GET_SYMNAME
    {sizeof(ULONG64), 0, "LENGTH"},                   //  2 TI_GET_LENGTH
    {sizeof(DWORD),   1, "TYPE"},                     //    TI_GET_TYPE
    {sizeof(DWORD),   1, "TYPEID"},                   //    TI_GET_TYPEID
    {sizeof(DWORD),   0, "BASETYPE"},                 //  5 TI_GET_BASETYPE
    {sizeof(DWORD),   1, "ARRAYINDEXTYPEID"},         //    TI_GET_ARRAYINDEXTYPEID
    {0,               0, "FINDCHILDREN"},             //    TI_FINDCHILDREN
    {sizeof(DWORD),   0, "DATAKIND"},                 //    TI_GET_DATAKIND
    {sizeof(DWORD),   0, "ADDRESSOFFSET"},            //    TI_GET_ADDRESSOFFSET
    {sizeof(DWORD),   0, "OFFSET"},                   // 10 TI_GET_OFFSET
    {sizeof(VARIANT), 0, "VALUE"},                    //    TI_GET_VALUE
    {sizeof(DWORD),   0, "COUNT"},                    //    TI_GET_COUNT
    {sizeof(DWORD),   0, "CHILDRENCOUNT"},            //    TI_GET_CHILDRENCOUNT
    {sizeof(DWORD),   0, "BITPOSITION"},              //    TI_GET_BITPOSITION
    {sizeof(BOOL),    0, "VIRTUALBASECLASS"},         // 15 TI_GET_VIRTUALBASECLASS
    {sizeof(DWORD),   1, "VIRTUALTABLESHAPEID"},      //    TI_GET_VIRTUALTABLESHAPEID
    {sizeof(DWORD),   0, "VIRTUALBASEPOINTEROFFSET"}, //    TI_GET_VIRTUALBASEPOINTEROFFSET
    {sizeof(DWORD),   1, "CLASSPARENTID"},            //    TI_GET_CLASSPARENTID
    {sizeof(DWORD),   0, "NESTED"},                   //    TI_GET_NESTED
    {sizeof(DWORD),   1, "SYMINDEX"},                 // 20 TI_GET_SYMINDEX
    {sizeof(DWORD),   1, "LEXICALPARENT"},            //    TI_GET_LEXICALPARENT
    {sizeof(ULONG64), 0, "ADDRESS"},                  //    TI_GET_ADDRESS
    {sizeof(DWORD),   0, "THISADJUST"},               //    TI_GET_THISADJUST
    {sizeof(DWORD),   0, "UDTKIND"},                  //    TI_GET_UDTKIND
    {0,               0, "IS_EQUIV_TO"},              // 25 TI_IS_EQUIV_TO
    {sizeof(DWORD),   0, "CALLING_CONVENTION"},       //    TI_GET_CALLING_CONVENTION
    {0,               0, "IS_CLOSE_EQUIV_TO"},        //    TI_IS_CLOSE_EQUIV_TO
    {0,               0, "GTIEX_REQS_VALID"},         //    TI_GTIEX_REQS_VALID
    {sizeof(DWORD),   0, "VIRTUALBASEOFFSET"},        //    TI_GET_VIRTUALBASEOFFSET
    {sizeof(DWORD),   0, "VIRTUALBASEDISPINDEX"},     // 30 TI_GET_VIRTUALBASEDISPINDEX
    /* FIXME: this generates crashes in native dbghelp... disable for now */
    {0/*sizeof(BOOL)*/,    0, "IS_REFERENCE"},             // 31 TI_GET_IS_REFERENCE
    {sizeof(BOOL),    0, "INDIRECTVIRTUALBASECLASS"}, // 32 TI_GET_INDIRECTVIRTUALBASECLASS
    {sizeof(DWORD),   1, "VIRTUALBASETABLETYPE"},     // 33 TI_GET_VIRTUALBASETABLETYPE
#if API_VERSION_NUMBER > 12
    {sizeof(DWORD),   1, "OBJECTPOINTERTYPE"},        // 34 TI_GET_OBJECTPOINTERTYPE
#endif
};

const char* id2str(const struct vacuum* v, DWORD id)
{
    DWORD tag;
    WCHAR* name;
    WCHAR* pname = NULL;
    BOOL with_tag;
    WCHAR und[128]; /* FIXME could make it dynamic */
    char* ret;

    with_tag = SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMTAG, &tag);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMNAME, &name))
    {
        if (UnDecorateSymbolNameW(name, und, ARRAYSIZE(und), 0))
            pname = und;
        else
            pname = name;
    }
    else name = NULL;
    if (with_tag && pname)
        ret = mysprintf("#%lx %s %ls", id, tag2str(tag), pname);
    else if (pname)
        ret = mysprintf("#%lx %ls", id, pname);
    else if (with_tag)
        ret = mysprintf("#%lx %s", id, tag2str(tag));
    else
        ret = mysprintf("#%lx", id);

    HeapFree(GetProcessHeap(), 0, name);
    return ret;
}

BOOL get_symbol_flags(const struct debuggee* dbg, DWORD64 base, DWORD id, DWORD* flags)
{
    SYMBOL_INFO si;

    si.SizeOfStruct = sizeof(si);
    si.MaxNameLen = 0;
    if (!SymFromIndex(dbg->process, base, id, &si)) return FALSE;
    *flags = si.Flags;
    return TRUE;
}

const char* symbolflags2str(DWORD flags)
{
    if (!flags) return "<<>>";
#define X(f, s) ((flags & (f)) ? ";" s : "")
    return mysprintf("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
                     X(SYMFLAG_VALUEPRESENT,            "valuepresent"),
                     X(SYMFLAG_REGISTER,                "register"),
                     X(SYMFLAG_REGREL,                  "regrel"),
                     X(SYMFLAG_FRAMEREL,                "framerel"),
                     X(SYMFLAG_PARAMETER,               "parameter"),
                     X(SYMFLAG_LOCAL,                   "local"),
                     X(SYMFLAG_CONSTANT,                "constant"),
                     X(SYMFLAG_EXPORT,                  "export"),
                     X(SYMFLAG_FORWARDER,               "forwarder"),
                     X(SYMFLAG_FUNCTION,                "function"),
                     X(SYMFLAG_VIRTUAL,                 "virtual"),
                     X(SYMFLAG_THUNK,                   "thunk"),
                     X(SYMFLAG_TLSREL,                  "tlsrel"),
                     X(SYMFLAG_SLOT,                    "slot"),
                     X(SYMFLAG_ILREL,                   "ilrel"),
                     X(SYMFLAG_METADATA,                "metadata"),
                     X(SYMFLAG_CLR_TOKEN,               "clr_token"),
                     X(SYMFLAG_NULL,                    "null"),
                     X(SYMFLAG_FUNC_NO_RETURN,          "func_no_return"),
                     X(SYMFLAG_SYNTHETIC_ZEROBASE,      "synthetic_zerobase"),
                     X(SYMFLAG_PUBLIC_CODE,             "public_code" )) + 1;
#undef X
}

void dump_symbol(const struct vacuum* v, DWORD id)
{
    DWORD info, info2;
    DWORD64 info64;
    VARIANT variant;
    BOOL b;
    unsigned j;

    printf("%s\n", id2str(v, id));

    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_LENGTH, &info64))
        printf("\tlength\t\t\t%llu %#I64x\n", info64, info64);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_TYPE, &info))
        printf("\ttype\t\t\t#%lx\n", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_TYPEID, &info2))
        printf("\ttypeid\t\t\t#%lx%s\n", info2, info == info2 ? "" : " ~~~Different type/typeID");
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_BASETYPE, &info))
        printf("\tbasetype\t\t%lu [%s]\n", info, basetype2str(info));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ARRAYINDEXTYPEID, &info))
        printf("\tarrayindextypeid\t#%lx\n", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ADDRESSOFFSET, &info))
        printf("\taddressoffset\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_OFFSET, &info))
        printf("\toffset\t\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VALUE, &variant))
        printf("\tvalue\t\t\t%s\n", variant2str(&variant));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_COUNT, &info))
        printf("\tcount\t\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_BITPOSITION, &info))
        printf("\tbitposition\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_DATAKIND, &info))
        printf("\tdatakind\t\t%lu [%s]\n", info, datakind2str(info));
    if (get_symbol_flags(v->dbg, v->base, id, &info))
        printf("\tsymbol flags\t\t%lx [%s]\n", info, symbolflags2str(info));

    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALBASECLASS, &b))
        printf("\tvirtual base class\t%s\n", b ? "True" : "False");
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALTABLESHAPEID, &info))
        printf("\tvirtual table shape ID\t#%lx\n", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALBASEPOINTEROFFSET, &info))
        printf("\tvirtual base pointer offset\t%lu %#lx\n", info, info);

    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_CLASSPARENTID, &info))
        printf("\tclassparentid\t\t#%lx\n", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_NESTED, &info))
        printf("\tnested\t\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMINDEX, &info))
        printf("\tsymindex\t\t#%lx%s\n", info, info == id ? "" : " ~~~ different symindex");
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_LEXICALPARENT, &info))
        printf("\tlexicalparent\t\t#%lx\n", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ADDRESS, &info64))
        printf("\taddress\t\t\t%I64x\n", info64);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_THISADJUST, &info))
        printf("\tthis adjust\t\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_UDTKIND, &info))
        printf("\tudtkind\t\t\t%lu %s\n", info, udtkind2str(info));
//    TI_IS_EQUIV_TO,
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_CALLING_CONVENTION, &info))
        printf("\tcalling convention\t%lu %#lx %s\n", info, info, cc2str(info));
//    TI_IS_CLOSE_EQUIV_TO,
//    TI_GTIEX_REQS_VALID,
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALBASEOFFSET, &info))
        printf("\tvirtual base offset\t%lu %#lx\n", info, info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALBASEDISPINDEX, &info))
        printf("\tvirtual base disp index\t%lu %#lx\n", info, info);
    #if 0
    // generates very strange crashes... I don't get it
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_IS_REFERENCE, &b))
        printf("\tis reference\t\t%s", b ? "True" : "False");
    #endif
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_INDIRECTVIRTUALBASECLASS, &b))
        printf("\tindirect virtual base class\t%s\n",  b ? "True" : "False");
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VIRTUALBASETABLETYPE, &info))
        printf("\tvirtual base table typeid\t#%lx\n", info);

    /* dump edge info */
    for (j = 0; j < v->num_edges; j++)
        if (v->edges[j].to == id)
        {
            if (v->edges[j].what == ~0u)
                printf("\tchild of %s\n", id2str(v, v->edges[j].from));
            else
                printf("\tedge %s from %s\n", ti2str(v->edges[j].what), id2str(v, v->edges[j].from));
        }
}

static unsigned dump_hash(unsigned hash, const void* ptr, size_t size)
{
    size_t i;
    for (i = 0; i < size; i++)
    {
        hash += ((const unsigned char*)ptr)[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}

static unsigned get_compact_stable_id(const struct debuggee* dbg, DWORD id)
{
    DWORD64 info64;
    DWORD info;
    LPWSTR wname;

    struct fold
    {
        DWORD64 lengthoffset;
        unsigned basetype : 6,
                 count : 7,
                 udtKind : 2;
        unsigned hash;
    } f;
    memset(&f, 0, sizeof(f));
    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_LENGTH, &info64)) f.lengthoffset ^= info64;
    if (!SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_BASETYPE, &info)) info = ~0u;
    f.basetype = info;
    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_OFFSET, &info)) f.lengthoffset ^= info;
    if (!SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_COUNT, &info)) info = 0;
    f.count = info;
    if (!SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_UDTKIND, &info)) info = 3;
    f.udtKind = info;
    if (SymGetTypeInfo(dbg->process, dbg->base, id, TI_GET_SYMNAME, &wname))
    {
        f.hash = dump_hash(0, wname, wcslen(wname) * sizeof(WCHAR));
        HeapFree(GetProcessHeap(), 0, wname);
    }
    else f.hash = 0;
    return dump_hash(0, &f, sizeof(f)) % 4093; /* prime, result will fit into 3 hex numbers */
}

static const char* compact_id2str(const struct vacuum* v, DWORD id)
{
    DWORD tag, cid;
    WCHAR* name;
    WCHAR* pname = NULL;
    BOOL with_tag;
    WCHAR und[128]; /* FIXME could make it dynamic */
    char* ret;

    cid = get_compact_stable_id(v->dbg, id);
    with_tag = SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMTAG, &tag);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_SYMNAME, &name))
    {
        if (UnDecorateSymbolNameW(name, und, ARRAYSIZE(und), 0))
            pname = und;
        else
            pname = name;
    }
    else name = NULL;
    if (with_tag && pname)
        ret = mysprintf("%s %ls{%lx}", tag2str(tag), pname, cid);
    else if (pname)
        ret = mysprintf("%ls{%lx}", pname, cid);
    else if (with_tag)
        ret = mysprintf("%s{%lx}", tag2str(tag), cid);
    else
        ret = mysprintf("{%lx}", cid);

    HeapFree(GetProcessHeap(), 0, name);
    return ret;

}

void dump_symbol_compact(const struct vacuum* v, DWORD id)
{
    DWORD info, info2;
    DWORD64 info64;
    VARIANT variant;

    printf("%s", compact_id2str(v, id));

    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_LENGTH, &info64))
        printf(" length=%#I64x", info64);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_TYPE, &info))
    {
        if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_TYPEID, &info2) && info == info2)
            printf(" type(-id)=%s", compact_id2str(v, info));
        else
            printf(" type=%s type-id=%s", compact_id2str(v, info), compact_id2str(v, info2));
    }
    else if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_TYPEID, &info2))
            printf(" type-id=%s", compact_id2str(v, info2));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_BASETYPE, &info))
        printf(" basetype=%s", basetype2str(info));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ARRAYINDEXTYPEID, &info))
        printf(" array-index-type-id=%s", compact_id2str(v, info));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ADDRESSOFFSET, &info))
        printf(" address-offset=%#lx", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_OFFSET, &info))
        printf(" offset=%#lx", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_VALUE, &variant))
        printf(" value=%s", variant2str(&variant));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_COUNT, &info))
        printf(" count=%#lx", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_BITPOSITION, &info))
        printf(" bit-position=%#lx", info);
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_LEXICALPARENT, &info))
        printf(" lexical-parent=%s", compact_id2str(v, info));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_ADDRESS, &info64))
        printf(" address=%s", addr2str(v->dbg, info64));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_UDTKIND, &info))
        printf(" udtkind=%s", udtkind2str(info));
    if (SymGetTypeInfo(v->dbg->process, v->base, id, TI_GET_CALLING_CONVENTION, &info))
        printf(" calling-convention=%s", cc2str(info));
    /* lots of TI_ C++ related skipped...
     *   TI_GET_VIRTUALBASECLASS, TI_GET_VIRTUALTABLESHAPEID, TI_GET_VIRTUALBASEPOINTEROFFSET,
     *   TI_GET_CLASSPARENTID, TI_GET_NESTED, TI_THIS_ADJUST, TI_GET_VIRTUALBASEOFFSET,
     *   TI_GET_VIRTUALBASEDISPINDEX, TI_GET_IS_REFERENCE, TI_GET_INDIRECTVIRTUALBASECLASS,
     *   TI_GET_VIRTUALBASETABLETYPE,
     */

    printf("\n");
}

/*************************************
 * Vacuum helpers (retrieve all ids) *
 *************************************/

static void push_id(struct vacuum* v, DWORD id)
{
    unsigned i;
    for (i = 0; i < v->num_ids; ++i)
        if (v->ids[i] == id) return;
    if (v->num_ids == v->alloc_ids)
    {
        DWORD* new;
        unsigned na = v->alloc_ids ? v->alloc_ids * 2 : 64;
        new = realloc(v->ids, na * sizeof(new[0]));
        if (!new) return;
        v->ids = new;
        v->alloc_ids = na;
    }
    v->ids[v->num_ids++] = id;
}

static void push_edge(struct vacuum* v, DWORD from, DWORD to, DWORD what)
{
    if (v->num_edges == v->alloc_edges)
    {
        struct edge* new;
        unsigned na = v->alloc_edges ? v->alloc_edges * 2 : 64;
        new = realloc(v->edges, na * sizeof(new[0]));
        if (!new) return;
        v->edges = new;
        v->alloc_edges = na;
    }
    v->edges[v->num_edges].from = from;
    v->edges[v->num_edges].to = to;
    v->edges[v->num_edges].what = what;
    v->num_edges++;
}

static BOOL CALLBACK vacuum_symbol_cb(PSYMBOL_INFOW pSymInfo, ULONG SymbolSize, PVOID user)
{
    UNREFERENCED_PARAMETER(SymbolSize);
    push_id((struct vacuum*)user, pSymInfo->Index);
    return TRUE;
}

static BOOL insert_edges(struct vacuum* v, DWORD id)
{
    TI_FINDCHILDREN_PARAMS* tfp;
    DWORD otherid;
    unsigned ti;

    for (ti = 0; ti < ARRAYSIZE(ti_info); ++ti)
    {
        if (ti_info[ti].recurse &&
            SymGetTypeInfo(v->dbg->process, v->base, id, ti, &otherid))
        {
            push_id(v, otherid);
            push_edge(v, id, otherid, ti);
        }
    }
    tfp = retrieve_children(v->dbg, id);
    if (tfp)
    {
        unsigned i;
        for (i = 0; i < tfp->Count; ++i)
        {
            /* Native uses SymTagCustom for other needs than SymAddSymbol
             * And, native doesn't expose SymTagCustom:s from current vacuum implem.
             * So, until all of this gets fixed, don't gather SymTagCustom symbols
             */
            if (tfp->ChildId[i] >= 0x80000000) continue;
            push_id(v, tfp->ChildId[i]);
            push_edge(v, id, tfp->ChildId[i], ~0u);
        }
        free(tfp);
    }
    return TRUE;
}

BOOL create_vacuum(const struct debuggee* dbg, ULONG_PTR base, struct vacuum* v)
{
    unsigned pos;

    v->dbg = dbg;
    v->base = base;
    v->ids = NULL;
    v->num_ids = v->alloc_ids = 0;
    v->edges = NULL;
    v->num_edges = v->alloc_edges = 0;

    SymEnumSymbolsW(dbg->process, base, NULL, vacuum_symbol_cb, v);
    SymEnumTypesW(dbg->process, base, vacuum_symbol_cb, v);

    for (pos = 0; pos < v->num_ids; ++pos)
    {
        insert_edges(v, v->ids[pos]);
    }
    return v->num_ids != 0;
}

void dispose_vacuum(struct vacuum* v)
{
    free(v->ids);
    free(v->edges);
}
