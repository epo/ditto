/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Entry point for managing command line and driving internals.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ditto.h"
#include "test.h"

static void NORETURN usage(const char* error, const char* pmt)
{
    if (error) fprintf(stderr, "Error %s", error);
    if (pmt) fprintf(stderr, ": %s", pmt);
    fprintf(stderr, "\n");
    fprintf(stderr, "<ditto> mode [options] foo.dll\n");
    fprintf(stderr, "mode can be among:\n");
    fprintf(stderr, "   test                          run the various tests\n");
    fprintf(stderr, "      --subprocess X             run with external process X (otherwise internal)\n");
    fprintf(stderr, "      --filter tst1[;tst2...]    run only the tests named tst1, tst2...\n");
    fprintf(stderr, "   dump                          dump all information out of DLL\n");
    fprintf(stderr, "      --filter tag1[;tag2...]    dump only the listed SymTag:s\n");
    fprintf(stderr, "      --compact                  one line per symbol, not printing non stable values\n");
    fprintf(stderr, "   matrix                        report the SymTag vs TI_ matrix\n");
    fprintf(stderr, "   edge                          report the SymTag vs TI_ matrix\n");
    fprintf(stderr, "      --dot                      use a .dot format\n");
    fprintf(stderr, "      --kind X                   use X relationship (all by default)\n");
    fprintf(stderr, "                                 possible values: lexical, child, symindex, type, typeid, class\n");
    fprintf(stderr, "   stats                         statistics about debug information\n");
    fprintf(stderr, "   map                           report detailed information\n");
    fprintf(stderr, "      --hierarchy                choose hierarchical exploration (default by address)\n");
    fprintf(stderr, "      --filter=XX                explore symbols named XX (default is all symbols in module)\n");
    fprintf(stderr, "      --with-lines               show line number information (default none)\n");
    fprintf(stderr, "      --with-inlined             show inlined functions inside the top-level functions (default none)\n");
    exit(1);
}

enum modes {MODE_JUSTLOAD, MODE_TEST, MODE_DUMP, MODE_MATRIX, MODE_EDGE, MODE_MAP, MODE_STATISTICS};

static void wait_thread(const struct debuggee* dbg)
{
    DWORD count;
    /* wait for the thread to suspend itself */
    do
    {
        Sleep(50);
        count = SuspendThread(dbg->thread);
        ResumeThread(dbg->thread);
    }
    while (!count);
}

static BOOL init_internal_process(struct debuggee* dbg, BOOL with_thread)
{
    HMODULE module = LoadLibraryA(dbg->dll_name);
    LPTHREAD_START_ROUTINE ptr;

    if (!module)
    {
        fprintf(stderr, "Couldn't load library %s\n", dbg->dll_name);
        return FALSE;
    }
    dbg->process = GetCurrentProcess();
    dbg->machine = IMAGE_FILE_MACHINE_UNKNOWN;
    dbg->base = dbg->size = 0;
    if (with_thread)
    {
        ptr = (LPTHREAD_START_ROUTINE)GetProcAddress(module, "stack_walk_thread");
        if (ptr == NULL)
        {
            fprintf(stderr, "Couldn't find stack_walk_thread\n");
            return FALSE;
        }
        dbg->thread = CreateThread(NULL, 0, ptr, NULL, 0, NULL);
        if (dbg->thread == NULL)
        {
            fprintf(stderr, "Couldn't start thread %p\n", ptr);
            return FALSE;
        }
        wait_thread(dbg);
    }
    else
        dbg->thread = NULL;
    return TRUE;
}

static BOOL init_external_process(struct debuggee* dbg, const char* subpcs)
{
    STARTUPINFOA siA;
    PROCESS_INFORMATION pi;
    char cmdline[MAX_PATH];
    HANDLE hJob;
    JOBOBJECT_EXTENDED_LIMIT_INFORMATION info;

    memset(&siA, 0, sizeof(siA));
    siA.cb = sizeof(siA);
    memset(&pi, 0, sizeof(pi));

    snprintf(cmdline, sizeof(cmdline), "%s justload %s", subpcs, dbg->dll_name);

    if (!CreateProcessA(NULL, cmdline, NULL, NULL, FALSE,
                        NORMAL_PRIORITY_CLASS|DETACHED_PROCESS, NULL, NULL, &siA, &pi))
    {
        fprintf(stderr, "Couldn't start subprocess %s\n", subpcs);
        return FALSE;
    }

    /* set current process and subprocess into the same job, so that subprocess will
     * be terminated if this process dies
     */
    hJob = CreateJobObjectA(NULL, NULL);
    memset(&info, 0, sizeof(info));
    info.BasicLimitInformation.LimitFlags =
        JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE |
        JOB_OBJECT_LIMIT_SILENT_BREAKAWAY_OK;
    SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &info, sizeof(info));
    AssignProcessToJobObject(hJob, pi.hProcess);

    dbg->process = pi.hProcess;
    dbg->thread = pi.hThread;
    dbg->machine = IMAGE_FILE_MACHINE_UNKNOWN;
    dbg->base = dbg->size = 0;
    wait_thread(dbg);

    return TRUE;
}

static BOOL init_debuggee(struct debuggee* dbg, const char* subpcs)
{
    if (dbg->kind & MODULE_LOADLIB)
        return (subpcs) ? init_external_process(dbg, subpcs) : init_internal_process(dbg, TRUE);
    dbg->process = GetCurrentProcess();
    return TRUE;
}

static void terminate_process(const struct debuggee* dbg)
{
    ResumeThread(dbg->thread);
}

/* wine's dbghelp is currently broken:
 * when loading a module with only dwarf4 debug info, it reports that
 * debug info is present (as it has found the relevant ELF/PE sections)
 * but fails to handle the case where all CUs are in the wrong format.
 * So we have to detect this on our own (until wine's dbghelp is fixed)
 */
static BOOL has_debug_info(const IMAGEHLP_MODULE64* im)
{
    return im->SymType == SymPdb || im->NumSyms > 4;
}

static BOOL CALLBACK find_debuggee_module_cb(PCSTR name, DWORD64 base, PVOID in_user)
{
    struct debuggee* dbg = in_user;
    IMAGEHLP_MODULE64 im;

    /* note: the im.ModuleName is truncated to 32 characters (including '\0'),
     * while parameter 'name' is truncated to 64...
     */
    memset(&im, 0, sizeof(im));
    im.SizeOfStruct = sizeof(im);
    if (SymGetModuleInfo64(dbg->process, base, &im) && has_debug_info(&im))
    {
        /* lookup external exec path as module name */
        struct split_path sp;

        split_path(dbg->dll_name, &sp);
        if ((!sp.ext_len || !strcasecmp(sp.ext, ".dll") || !strcasecmp(sp.ext, ".exe")) &&
            !strncasecmp(sp.filename, name, sp.filename_len))
        {
            dbg->base        = im.BaseOfImage;
            dbg->size        = im.ImageSize;
            dbg->machine     = im.MachineType;
            dbg->module_name = strdup(name);

            return FALSE;
        }
    }

    return TRUE;
}

static BOOL start_debug_info(struct debuggee* dbg, enum modes mode)
{
    /* FIXME: native (on W10) has LOAD_LINES set, while wine doesn't
     * always setting it is a simple workaround
     */
    DWORD opt = SymGetOptions() | SYMOPT_INCLUDE_32BIT_MODULES | SYMOPT_LOAD_LINES;
    if (mode == MODE_MAP) opt |= SYMOPT_NO_PUBLICS;
    SymSetOptions(opt);

    if (!SymInitialize(dbg->process, NULL, dbg->kind & MODULE_LOADLIB))
    {
        fprintf(stderr, "Cannot init dbghelp\n");
        return FALSE;
    }
    SymSetSearchPath(dbg->process, "srv*z:\\ditto\\symstore*https://msdl.microsoft.com/download/symbols");
    if (!(dbg->kind & MODULE_LOADLIB) && !SymLoadModuleEx(dbg->process, NULL, dbg->dll_name, NULL, 0, 0, NULL, 0))
    {
        fprintf(stderr, "Couldn't init module2 %s\n", dbg->dll_name);
        SymCleanup(dbg->process);
        return FALSE;
    }
    if (!SymEnumerateModules64(dbg->process, find_debuggee_module_cb, dbg) || dbg->base == 0)
    {
        fprintf(stderr, "Couldn't init module %s\n", dbg->dll_name);
        SymCleanup(dbg->process);
        return FALSE;
    }

    return TRUE;
}

static void stop_debug_info(struct debuggee* dbg)
{
    SymCleanup(dbg->process);
}

static BOOL can_run(const char* filter, const char* name)
{
    if (filter)
    {
        const char* where = strstr(filter, name);
        if (where && (where == filter || where[-1] == ';'))
        {
            char ch = where[strlen(name)];
            return ch == '\0' || ch == ';';
        }
        return FALSE;
    }
    return TRUE;
}

static void do_tests(struct debuggee* dbg, const char* filter)
{
    if (can_run(filter, "dbginfo"))   debuggee_tests(dbg);
    if (can_run(filter, "coherence")) coherence_tests(dbg);
}

int main(int argc, char** argv)
{
    int i;
    struct debuggee dbg;
    enum modes mode;
    const char* subpcs = NULL;
    const char* filter = NULL;
    BOOL with_dot = FALSE, compact = FALSE;
    enum map_modes map = MAP_BY_ADDRESS;

    if (argc < 2) usage("no mode specified", NULL);
    if (!strcmp(argv[1], "test")) mode = MODE_TEST;
    else if (!strcmp(argv[1], "justload")) mode = MODE_JUSTLOAD;
    else if (!strcmp(argv[1], "dump")) mode = MODE_DUMP;
    else if (!strcmp(argv[1], "matrix")) mode = MODE_MATRIX;
    else if (!strcmp(argv[1], "edge")) mode = MODE_EDGE;
    else if (!strcmp(argv[1], "map")) mode = MODE_MAP;
    else if (!strcmp(argv[1], "stats")) mode = MODE_STATISTICS;
    else if (!strcmp(argv[1], "--help")) usage(NULL, NULL);
    else usage("Unknown mode", argv[1]);

    dbg.dll_name = NULL;
    for (i = 2; i < argc; ++i)
    {
        if (!strcmp(argv[i], "--help")) usage(NULL, NULL);
        else if (mode == MODE_TEST && !strcmp(argv[i], "--subprocess") && i + 1 < argc) subpcs = argv[++i];
        else if (mode == MODE_TEST && !strcmp(argv[i], "--filter") && i + 1 < argc)     filter = argv[++i];
        else if (mode == MODE_DUMP && !strcmp(argv[i], "--filter") && i + 1 < argc)     filter = argv[++i];
        else if (mode == MODE_DUMP && !strcmp(argv[i], "--compact"))                    compact = TRUE;
        else if (mode == MODE_EDGE && !strcmp(argv[i], "--dot"))                        with_dot = TRUE;
        else if (mode == MODE_EDGE && !strcmp(argv[i], "--kind") && i + 1 < argc)       filter = argv[++i];
        else if (mode == MODE_MAP  && !strcmp(argv[i], "--hierarchy"))                  map |= MAP_HIERARCHY;
        else if (mode == MODE_MAP  && !strcmp(argv[i], "--with-lines"))                 map |= MAP_WITH_LINES;
        else if (mode == MODE_MAP  && !strcmp(argv[i], "--with-inlined"))               map |= MAP_WITH_INLINED;
        else if (mode == MODE_MAP  && !strcmp(argv[i], "--filter") && i + 1 < argc)     filter = argv[++i];
        else if (argv[i][0] == '-')                                                     usage("unknown option", argv[i]);
        else if (!dbg.dll_name)                                                         dbg.dll_name = argv[i];
        else usage("second dll specified", argv[i]);
    }
    if (!dbg.dll_name) usage("Must specify a DLL", NULL);

    if (mode == MODE_JUSTLOAD)
    {
        HMODULE module = LoadLibraryA(dbg.dll_name);
        LPTHREAD_START_ROUTINE ptr;
        if (module && (ptr = (LPTHREAD_START_ROUTINE)GetProcAddress(module, "stack_walk_thread")) != NULL)
            (ptr)(NULL);
    }
    else
    {
        dbg.kind = (mode == MODE_TEST) ? MODULE_LOADLIB : 0;
        if (mode == MODE_TEST && subpcs) dbg.kind |= EXTERNAL_PROCESS;

        if (init_debuggee(&dbg, subpcs))
        {
            if (start_debug_info(&dbg, mode))
            {
                switch (mode)
                {
                case MODE_TEST:       do_tests(&dbg, filter);           break;
                case MODE_DUMP:       do_dump(&dbg, filter, compact);   break;
                case MODE_MATRIX:     do_matrix(&dbg);                  break;
                case MODE_EDGE:       do_edge(&dbg, filter, with_dot);  break;
                case MODE_MAP:        do_map(&dbg, map, filter);        break;
                case MODE_STATISTICS: do_statistics(&dbg);              break;
                case MODE_JUSTLOAD:                                     break;
                }
                stop_debug_info(&dbg);
            }
            terminate_process(&dbg);
        }
    }
    return 0;
}
