/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * Wine-like test framework, so that tests could be integrated into Wine.
 * (imported from Wine tree)
 *
 * Copyright (C) 2002 Alexandre Julliard
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <windows.h>

static unsigned ok_count;
static unsigned ok_count_failed;
static unsigned ok_count_todo;
static const char* ok_file;
static unsigned ok_line;
static char* ok_context[4];
static unsigned ok_context_index;
static int ok_intodo;
static int ok_todocond;
static unsigned ok_running_wine;

static unsigned winetest_color;
static const char color_reset[] = "\x1b[0m";
static const char color_dark_red[] = "\x1b[31m";
static const char color_yellow[] = "\x1b[33m";
static const char color_bright_red[] = "\x1b[1;91m";
static inline const char* color_of(const char* s)
{
    return winetest_color ? s : "";
}

#if defined(__GNUC__)
static inline void __ok(int c, const char* msg, ...) __attribute__ ((format(printf, 2, 3)));
#endif
static inline void __ok(int c, const char* msg, ...)
{
    unsigned intodo = ok_intodo && ok_todocond;
    if (!c || intodo)
    {
        va_list args;
        if (intodo)
        {
            if (c)
                printf("%sTest succeeded inside todo:%s\n\t", color_of(color_dark_red), color_of(color_reset));
            else
                printf("%sTest marked todo:%s ", color_of(color_yellow), color_of(color_reset));
        }
        else
            printf("%sTest failed:%s ", color_of(color_bright_red), color_of(color_reset));
        if (ok_context_index)
        {
            unsigned i;
            for (i = 0; i < ok_context_index; ++i)
                printf("%s:", ok_context[i]);
            printf(" ");
        }
        printf("%s:%u:", ok_file, ok_line);
        va_start(args, msg);
        vprintf(msg, args);
        va_end(args);
        if (!c && !intodo) ok_count_failed++; else ok_count_todo++;
    }
    ok_count++;
}
#define ok              ok_(__FILE__, __LINE__)
#define ok_(a, b)       ok_file=(a),ok_line=(b),__ok

static inline const char* debugstr_a(const char* s) {return s;}

#if defined(__GNUC__)
static inline void winetest_push_context(const char* msg, ...) __attribute__ ((format(printf, 1, 2)));
#endif
static inline void winetest_push_context(const char* msg, ...)
{
    va_list args;

    assert(ok_context_index < sizeof(ok_context) / sizeof(ok_context[0]));
    ok_context[ok_context_index] = malloc(1024);

    va_start(args, msg);
    vsnprintf(ok_context[ok_context_index], 1024, msg, args);
    va_end(args);
    ok_context_index++;
}

static inline void winetest_pop_context(void)
{
    assert(ok_context_index);
    free(ok_context[--ok_context_index]);
}

#define todo_for(x) for (ok_todocond = (x),ok_intodo++;ok_intodo;ok_intodo--)
#define todo_wine todo_for(ok_running_wine)
#define todo_wine_if(x) todo_for(ok_running_wine && (x))

static inline void winetest_init(void)
{
    HMODULE module = GetModuleHandleA("ntdll.dll");
    if (module && GetProcAddress(module, "wine_server_call") != NULL)
        ok_running_wine = 1;
    /* sigh... */
#ifdef _MSC_VER
#pragma warning( suppress : 4996 )
    winetest_color = getenv("DITTO_TEST_COLOR") || _isatty( _fileno( stdout ) );
#else
    winetest_color = getenv("DITTO_TEST_COLOR") || _isatty( _fileno( stdout ) );
#endif
    /* enable ANSI support for Windows console */
    if (winetest_color)
    {
        HANDLE hOutput = (HANDLE)_get_osfhandle( _fileno( stdout ) );
        DWORD mode;
        if (GetConsoleMode( hOutput, &mode ) &&
            !SetConsoleMode( hOutput, mode | ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING ))
            winetest_color = 0;
    }
}

static inline void winetest_report(const char* testname)
{
    printf("Test %s: executed %u", testname, ok_count);
    if (ok_count_failed) printf(", %sfailed %u%s", color_of(color_bright_red), ok_count_failed, color_of(color_reset));
    if (ok_count_todo) printf(", %stodo %u%s", color_of(color_yellow), ok_count_todo, color_of(color_reset));
    printf("\n");
}

#define CONCAT2(x, y) x##y
#define CONCAT(x, y)  CONCAT2(x, y)

#define START_TEST(X) \
    static void CONCAT(ditto_implement_, X)(const struct debuggee*);                    \
    void CONCAT(X, _tests)(const struct debuggee* dbg)                                  \
    {                                                                                   \
        winetest_init();                                                                \
        winetest_push_context("%s<%s>",                                                 \
                              dbg->module_name,                                         \
                              dbg->kind & EXTERNAL_PROCESS ? "external" : "internal");  \
        CONCAT(ditto_implement_, X)(dbg);                                               \
        winetest_pop_context();                                                         \
        winetest_report(#X);                                                            \
    }                                                                                   \
    static void CONCAT(ditto_implement_, X)(const struct debuggee* dbg)
