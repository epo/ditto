/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * WineDbg test program.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern int wdtp_expr(int argc, const char** argv);
extern int wdtp_xpoint(int argc, const char** argv);

int main(int argc, const char** argv)
{
    if (argc < 2) return -1;
    if (!strcmp(argv[1], "expr"))   return wdtp_expr(argc - 1, argv + 1);
    if (!strcmp(argv[1], "xpoint")) return wdtp_xpoint(argc - 1, argv + 1);
    printf("--< Unsupported test '%s' >--\n", argv[1]);
    return -1;
}
