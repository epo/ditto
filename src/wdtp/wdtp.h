/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * WineDbg test program.
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

static int wdtp_dummy_counter;
/* the WDTP_INSN_BARRIER is a macro to prevent the compiler to reorder instructions
 * (at least memory reads and writes) before and after the barrier
 * it's also supposed to be breakable (ie to contain code that's not optimized out)
 */
#ifdef __GNUC__
#  define WDTP_INSN_BARRIER(...) { asm volatile ("" ::: "memory"); wdtp_dummy_counter++; asm volatile ("" ::: "memory"); }
#  define WDTP_DONT_INLINE __attribute__ ((noinline))
#elif defined(_MSC_VER)
#  if _MSC_VER < 1400
     extern void _ReadWriteBarrier();
#  else
#    include <intrin.h>
#  endif
#  pragma intrinsic(_ReadWriteBarrier)
#  define WDTP_INSN_BARRIER(...) { _ReadWriteBarrier(); wdtp_dummy_counter++; _ReadWriteBarrier(); }
#  define WDTP_DONT_INLINE __declspec(noinline)
#else
#  error Undefined WDTP_INSN_BARRIER, WDTP_DONT_INLINE macros
#endif
