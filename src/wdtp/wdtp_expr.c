/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * WineDbg test program (sample tests for expressions)
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */


#include <stdlib.h>
#include <windows.h>
#include "wdtp.h"

/* FIXME: strangely enough, msvc wrongly handles types
 * where struct foo exists and foo is a typedef to another struct
 */

int             myint0;
int             myint1;
static int      myint2;
enum toto_enum {zero, one, two, three, four};

struct toto
{
    int toto_a;
    int toto_b;
    unsigned bf1 : 10, bf2 : 6;
    int bf3 : 13, bf4 : 3;
    float       ff;
    double      fd;
    struct {
        int a;
        int b;
    } s;
    union {
        int i;
        unsigned u;
    } u;
    enum toto_enum x;
};

struct titi
{
    void*               pA;
    struct titi*        pB;
    const char*         pC;
    const WCHAR*        pD;
} titi = {(void*)(DWORD_PTR)0x87654321, (struct titi*)(DWORD_PTR)0x43218765, "foo", L"barbar"};

static long long sll;
static unsigned long long ull;
enum toto_enum wdtp_test_expr_te = three;

struct tata
{
    int                 i;
    short               s;
    char                c;
} mytata[] = {{12,2,'a'},{13,3,'b'},{14,4,'c'}}, *pmytata = mytata;

static int g(int a)
{
    sll = -((long long)1234567 * 100000 + 99000);
    ull = -sll;
    return a;
}

/* we need a separate global function so that the toto struct isn't optimized out */
int wdtp_test_expr_part(struct toto* t, int argc)
{
    t->toto_a += 1 + argc;
    t->toto_b += 2 + (argc << 3);
    WDTP_INSN_BARRIER(PART1);
    t->toto_a <<= t->bf1;
    WDTP_INSN_BARRIER();
    t->toto_a *= 2;
    t->toto_b += t->toto_a + ((t->x == two) ? 2 : 1);
    return 0;
}

int wdtp_expr(int argc, const char** ptr)
{
    struct toto t = {0, 0, 12, 63, -34, -4, 1.23f, 4.56e-2, {0x5A5A5A5A, 0xA5A5A5A5}, {0xAAAAAAAA}, one};
    static int sqrt2 = 14142;

    WDTP_INSN_BARRIER(EXPR);
    myint0 = -4;
    myint1 = g(argc);
    myint2 = 3 * t.bf1;
    wdtp_test_expr_part(&t, argc - 1);

    return t.toto_a + t.toto_b + ptr[0][0] + sqrt2++;
}

/* checking basic types (ensuring types are kept in debug info) */
_Bool my_bt_Bool;
char my_bt_char;
WCHAR my_bt_WCHAR;
signed char my_bt_signed_char;
unsigned char my_bt_unsigned_char;
signed short int my_bt_signed_short;
unsigned short int my_bt_unsigned_short;
signed int my_bt_signed_int;
unsigned int my_bt_unsigned_int;
signed long my_bt_signed_long;
unsigned long my_bt_unsigned_long;
signed long long my_bt_signed_longlong;
unsigned long long my_bt_unsigned_longlong;
#if defined(_WIN64) && !defined(_MSC_VER)
signed __int128 my_bt_signed___int128;
unsigned __int128 my_bt_unsigned___int128;
#endif
