/*
 * This file is part of Wine Ditto (Wine Debug Info Test Tool)
 *
 * WineDbg test program (break and watchpoints).
 *
 * Copyright (C) 2021-2022 Eric Pouech
 *               2023-2024 Eric Pouech for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include "wdtp.h"

static int xpoint_int = 3;

int wdtp_test_xpoint_write(int* ptr)
{
    *ptr = 3;
    WDTP_INSN_BARRIER(XPOINT2);
    xpoint_int += *ptr;
    WDTP_INSN_BARRIER(XPOINT3);
    return *ptr ^ xpoint_int;
}

/* a bit intricated, but done so that 'b func' matches the marker */
int wdtp_test_xpoint_g(int i) { WDTP_INSN_BARRIER(XPOINT4);
    do
    {
    volatile int ret;
    ret = 12 + i;
    WDTP_INSN_BARRIER(XPOINT5);
    return ret;
    } while (0);
}

int wdtp_test_xpoint_cond(void)
{
    int i;
    int ret = 0;

    for (i = 0; i < 10; i++)
        ret += wdtp_test_xpoint_g(i);
    return ret;
}

int wdtp_xpoint(int argc, const char** argv)
{
    (void)argv;

    WDTP_INSN_BARRIER(XPOINT);
    wdtp_test_xpoint_write(&argc);
    wdtp_test_xpoint_cond();
    return 0;
}
